package de.stefan1200.jts3servermod.functions;

import de.stefan1200.jts3servermod.interfaces.*;
import de.stefan1200.jts3serverquery.*;
import de.stefan1200.util.*;
import de.stefan1200.jts3servermod.*;
import java.util.*;

public class InactiveChannelCheck implements LoadConfiguration, HandleBotEvents
{
    private String configPrefix;
    private JTS3ServerMod_Interface modClass;
    private JTS3ServerQuery queryLib;
    private boolean pluginEnabled;
    private long emptyDeleteTime;
    private boolean ignorePermanent;
    private boolean ignoreSemiPermanent;
    private Vector<Integer> ignoreChannelList;
    private Vector<Integer> parentChannelList;
    private boolean parentChannelList_ignore;
    private FunctionExceptionLog fel;
    private long lastCheck;
    
    public InactiveChannelCheck() {
        this.configPrefix = "";
        this.modClass = null;
        this.queryLib = null;
        this.pluginEnabled = false;
        this.emptyDeleteTime = -1L;
        this.ignorePermanent = true;
        this.ignoreSemiPermanent = false;
        this.ignoreChannelList = new Vector<Integer>();
        this.parentChannelList = new Vector<Integer>();
        this.parentChannelList_ignore = true;
        this.fel = new FunctionExceptionLog();
        this.lastCheck = 0L;
    }
    
    public void initClass(final JTS3ServerMod_Interface modClass, final JTS3ServerQuery queryLib, final String prefix) {
        this.modClass = modClass;
        this.queryLib = queryLib;
        this.configPrefix = prefix.trim();
    }
    
    public void handleOnBotConnect() {
        if (!this.pluginEnabled) {
            return;
        }
        final StringBuffer sbList = new StringBuffer();
        if (!this.ignorePermanent) {
            sbList.append("permanent");
        }
        if (!this.ignoreSemiPermanent) {
            if (sbList.length() > 0) {
                sbList.append(" and ");
            }
            sbList.append("semi-permanent");
        }
        final String msg = "Delete " + sbList.toString() + " channels if empty for at least " + Long.toString(this.emptyDeleteTime / 60L / 60L) + " hours!";
        this.modClass.addLogEntry(this.configPrefix, (byte)1, msg, true);
    }
    
    public void handleAfterCacheUpdate() {
        if (!this.pluginEnabled) {
            return;
        }
        if (System.currentTimeMillis() - this.lastCheck < 600000L) {
            return;
        }
        int channelID = 0;
        int emptySeconds = 0;
        for (final HashMap<String, String> channel : this.modClass.getChannelList()) {
            if (channel.get("channel_flag_default").equals("1")) {
                continue;
            }
            if (this.ignorePermanent && channel.get("channel_flag_permanent").equals("1")) {
                continue;
            }
            if (this.ignoreSemiPermanent && channel.get("channel_flag_semi_permanent").equals("1")) {
                continue;
            }
            if (channel.get("channel_flag_permanent").equals("0") && channel.get("channel_flag_semi_permanent").equals("0")) {
                continue;
            }
            channelID = Integer.parseInt(channel.get("cid"));
            if (this.modClass.isIDListed(channelID, this.ignoreChannelList)) {
                continue;
            }
            final boolean result = this.modClass.isIDListed(Integer.parseInt(channel.get("pid")), this.parentChannelList);
            if (this.parentChannelList_ignore) {
                if (result) {
                    continue;
                }
            }
            else if (!result) {
                continue;
            }
            emptySeconds = Integer.parseInt(channel.get("seconds_empty"));
            if (emptySeconds > 31536000) {
                this.modClass.addLogEntry(this.configPrefix, (byte)2, "Got bad values for channel \"" + channel.get("channel_name") + "\" (id: " + Integer.toString(channelID) + ") from TS3 server, skipping channel!", false);
            }
            else {
                if (emptySeconds <= this.emptyDeleteTime) {
                    continue;
                }
                try {
                    this.queryLib.deleteChannel(channelID, false);
                    this.modClass.addLogEntry(this.configPrefix, (byte)1, "Channel \"" + channel.get("channel_name") + "\" (id: " + Integer.toString(channelID) + ") was empty for more than " + Long.toString(emptySeconds / 60 / 60) + " hours. Channel was deleted!", false);
                    this.fel.clearException(channelID);
                }
                catch (TS3ServerQueryException sqe) {
                    if (this.fel.existsException(sqe, channelID)) {
                        continue;
                    }
                    this.fel.addException(sqe, channelID);
                    this.modClass.addLogEntry(this.configPrefix, (byte)3, "Channel \"" + channel.get("channel_name") + "\" (id: " + Integer.toString(channelID) + ") was empty for more than " + Long.toString(emptySeconds / 60 / 60) + " hours, but an error occurred while deleting channel!", false);
                    this.modClass.addLogEntry(this.configPrefix, sqe, false);
                }
                catch (Exception e) {
                    this.modClass.addLogEntry(this.configPrefix, (byte)3, "Channel \"" + channel.get("channel_name") + "\" (id: " + Integer.toString(channelID) + ") was empty for more than " + Long.toString(emptySeconds / 60 / 60) + " hours, but an error occurred while deleting channel!", false);
                    this.modClass.addLogEntry(this.configPrefix, e, false);
                }
            }
        }
        this.lastCheck = System.currentTimeMillis();
    }
    
    public void activate() {
    }
    
    public void disable() {
    }
    
    public void unload() {
        this.ignoreChannelList = null;
        this.parentChannelList = null;
    }
    
    public boolean multipleInstances() {
        return true;
    }
    
    public void initConfig(final ArrangedPropertiesWriter config) {
        config.addKey(String.valueOf(this.configPrefix) + "_emptydeletetime", "After how many hours an empty channel should be deleted? Possible values between 1 and 2200 hours.", "168");
        config.addKey(String.valueOf(this.configPrefix) + "_ignore_permanent", "Never delete permanent channels? Set yes or no here!", "yes");
        config.addKey(String.valueOf(this.configPrefix) + "_ignore_semipermanent", "Never delete semi permanent channels? Set yes or no here!", "no");
        config.addKey(String.valueOf(this.configPrefix) + "_ignore_channels", "A comma separated list (without spaces) of channel ids you like to ignore. This channels don't get deleted!");
        config.addKey(String.valueOf(this.configPrefix) + "_parentchannel_list", "A comma separated list (without spaces) of parent channel ids.\nDepends on the given mode, all sub-channels of this channels can be ignored or only sub-channels of this channels will be checked!\nIf no parent channels should be ignored, set no channels here and select the channel list mode ignore!");
        config.addKey(String.valueOf(this.configPrefix) + "_parentchannel_list_mode", "Select one of the two modes for the parent channel list.\nignore = All sub-channels of the selected channels will be ignored.\nonly = Only sub-channels of the selected channels will be checked.", "ignore");
    }
    
    public boolean loadConfig(final ArrangedPropertiesWriter config, final boolean slowMode) throws BotConfigurationException, NumberFormatException {
        String lastNumberValue = "";
        String temp = null;
        this.pluginEnabled = false;
        try {
            lastNumberValue = String.valueOf(this.configPrefix) + "_emptydeletetime";
            temp = config.getValue(String.valueOf(this.configPrefix) + "_emptydeletetime");
            if (temp == null) {
                throw new NumberFormatException();
            }
            this.emptyDeleteTime = Long.parseLong(temp.trim()) * 60L * 60L;
            if (this.emptyDeleteTime < 3600L) {
                this.emptyDeleteTime = 3600L;
            }
            else if (this.emptyDeleteTime > 7920000L) {
                this.emptyDeleteTime = 7920000L;
            }
            this.ignorePermanent = config.getValue(String.valueOf(this.configPrefix) + "_ignore_permanent", "yes").trim().equalsIgnoreCase("yes");
            this.ignoreSemiPermanent = config.getValue(String.valueOf(this.configPrefix) + "_ignore_semipermanent", "no").trim().equalsIgnoreCase("yes");
            if (this.ignorePermanent && this.ignoreSemiPermanent) {
                throw new BotConfigurationException("Ignoring permanent and semi-permanent channels are activated, disabled Inactive Channel Check!");
            }
            temp = null;
            this.ignoreChannelList.clear();
            temp = config.getValue(String.valueOf(this.configPrefix) + "_ignore_channels");
            lastNumberValue = String.valueOf(this.configPrefix) + "_ignore_channels";
            if (temp != null && temp.length() > 0) {
                final StringTokenizer st = new StringTokenizer(temp, ",", false);
                while (st.hasMoreTokens()) {
                    this.ignoreChannelList.addElement(Integer.parseInt(st.nextToken().trim()));
                }
            }
            temp = null;
            this.parentChannelList.clear();
            temp = config.getValue(String.valueOf(this.configPrefix) + "_parentchannel_list");
            lastNumberValue = String.valueOf(this.configPrefix) + "_parentchannel_list";
            if (temp != null && temp.length() > 0) {
                final StringTokenizer st = new StringTokenizer(temp, ",", false);
                while (st.hasMoreTokens()) {
                    this.parentChannelList.addElement(Integer.parseInt(st.nextToken().trim()));
                }
            }
            this.parentChannelList_ignore = !config.getValue(String.valueOf(this.configPrefix) + "_parentchannel_list_mode", "ignore").trim().equalsIgnoreCase("only");
            this.pluginEnabled = true;
        }
        catch (NumberFormatException e) {
            final NumberFormatException nfe = new NumberFormatException("Config value of \"" + lastNumberValue + "\" is not a number! Current value: " + config.getValue(lastNumberValue, "not set"));
            nfe.setStackTrace(e.getStackTrace());
            throw nfe;
        }
        return this.pluginEnabled;
    }
    
    public void setListModes(final BitSet listOptions) {
    }
}
