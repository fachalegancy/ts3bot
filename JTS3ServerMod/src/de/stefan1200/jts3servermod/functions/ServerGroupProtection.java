package de.stefan1200.jts3servermod.functions;

import de.stefan1200.jts3servermod.interfaces.*;
import de.stefan1200.jts3servermod.*;
import de.stefan1200.util.*;
import java.sql.*;
import java.io.*;
import java.util.*;
import de.stefan1200.jts3serverquery.*;

public class ServerGroupProtection implements HandleBotEvents, HandleTS3Events, LoadConfiguration, HandleClientList
{
    private String configPrefix;
    private JTS3ServerMod_Interface modClass;
    private JTS3ServerQuery queryLib;
    private boolean pluginEnabled;
    private String SERVERGROUPPROTECTION_FILE;
    private String SERVERGROUPPROTECTION_MESSAGE_MODE;
    private String SERVERGROUPPROTECTION_MESSAGE;
    private boolean SERVERGROUPPROTECTION_ADD_MISSING_GROUPS;
    private boolean SERVERGROUPPROTECTION_KICK;
    private boolean SERVERGROUPPROTECTION_COMPLAINADD;
    private Vector<Integer> SERVERGROUPPROTECTION_GROUPS;
    private Vector<Vector<String>> SERVERGROUPPROTECTION_CLIENTS;
    private Vector<Boolean> SERVERGROUPPROTECTION_ADDALLOWED;
    private FunctionExceptionLog fel;
    
    public ServerGroupProtection() {
        this.configPrefix = "";
        this.modClass = null;
        this.queryLib = null;
        this.pluginEnabled = false;
        this.SERVERGROUPPROTECTION_FILE = null;
        this.SERVERGROUPPROTECTION_MESSAGE_MODE = null;
        this.SERVERGROUPPROTECTION_MESSAGE = null;
        this.SERVERGROUPPROTECTION_ADD_MISSING_GROUPS = false;
        this.SERVERGROUPPROTECTION_KICK = false;
        this.SERVERGROUPPROTECTION_COMPLAINADD = false;
        this.SERVERGROUPPROTECTION_GROUPS = new Vector<Integer>();
        this.SERVERGROUPPROTECTION_CLIENTS = new Vector<Vector<String>>();
        this.SERVERGROUPPROTECTION_ADDALLOWED = new Vector<Boolean>();
        this.fel = new FunctionExceptionLog();
    }
    
    public void initClass(final JTS3ServerMod_Interface modClass, final JTS3ServerQuery queryLib, final String prefix) {
        this.modClass = modClass;
        this.queryLib = queryLib;
        this.configPrefix = prefix.trim();
    }
    
    public void handleOnBotConnect() {
        if (!this.pluginEnabled) {
            return;
        }
        final StringBuffer sb = new StringBuffer();
        for (final int groupID : this.SERVERGROUPPROTECTION_GROUPS) {
            if (sb.length() != 0) {
                sb.append(", ");
            }
            sb.append(groupID);
        }
        final String msg = "Server Group Protection will remove not allowed members from protected server groups (id: " + sb.toString() + ")" + (this.SERVERGROUPPROTECTION_KICK ? " and kick them" : " but they will not kicked") + (this.SERVERGROUPPROTECTION_COMPLAINADD ? " (complaint will be added)" : "");
        this.modClass.addLogEntry(this.configPrefix, (byte)1, msg, true);
    }
    
    public void handleAfterCacheUpdate() {
        for (int i = 0; i < this.SERVERGROUPPROTECTION_GROUPS.size(); ++i) {
            this.SERVERGROUPPROTECTION_ADDALLOWED.setElementAt(this.modClass.getServerGroupType(this.SERVERGROUPPROTECTION_GROUPS.elementAt(i)) == 1, i);
        }
    }
    
    public void activate() {
    }
    
    public void disable() {
    }
    
    public void unload() {
        this.SERVERGROUPPROTECTION_GROUPS = null;
        this.SERVERGROUPPROTECTION_CLIENTS = null;
        this.SERVERGROUPPROTECTION_ADDALLOWED = null;
    }
    
    public boolean multipleInstances() {
        return true;
    }
    
    public void initConfig(final ArrangedPropertiesWriter config) {
        config.addKey(String.valueOf(this.configPrefix) + "_groups", "A comma separated list (without spaces) of server group ids, which should be protected.");
        config.addKey(String.valueOf(this.configPrefix) + "_kick", "Enable this to kick every client which using a protected server group and are not on the list of the bot, set yes or no here!", "yes");
        config.addKey(String.valueOf(this.configPrefix) + "_add_complain", "Add complaint entry to the user, set yes or no here!\nThis would only add a complaint, if the bot has to remove a server group.", "no");
        config.addKey(String.valueOf(this.configPrefix) + "_add_missing_groups", "If a client is listed in the servergroupprotection_file and miss a server group, they get added to the server group.\nThis only works for normal server groups (clients do not get added to groups like Admin Server Query)! Set yes or no here!", "yes");
        config.addKey(String.valueOf(this.configPrefix) + "_message_mode", "Select the message mode, how the client should get the message (useless if kick is enabled).\npoke, chat or none are valid values!", "poke");
        if (this.modClass.getMySQLConnection() == null) {
            config.addKey(String.valueOf(this.configPrefix) + "_file", "Path to file which contains the Server Group Protection client list and kick message.", "config/server1/servergroupprotection.cfg");
        }
        if (this.modClass.getMySQLConnection() != null) {
            config.addKey(String.valueOf(this.configPrefix) + "_message", "The kick or chat message for the server group protection.\nYou can use the following keywords, which will be replaced:\n%SERVER_GROUP_ID% - Replaced with the server group id.\n%SERVER_GROUP_NAME% - Replaced with the server group name.\nYou can use \\n for a new line and typical BBCode like in Teamspeak 3 Client.");
        }
    }
    
    public boolean loadConfig(final ArrangedPropertiesWriter config, final boolean slowMode) throws BotConfigurationException, NumberFormatException {
        String lastNumberValue = "";
        String temp = null;
        this.pluginEnabled = false;
        try {
            temp = null;
            this.SERVERGROUPPROTECTION_ADDALLOWED.clear();
            this.SERVERGROUPPROTECTION_GROUPS.clear();
            temp = config.getValue(String.valueOf(this.configPrefix) + "_groups");
            lastNumberValue = String.valueOf(this.configPrefix) + "_groups";
            if (temp == null || temp.length() <= 0) {
                throw new BotConfigurationException("Server Group Protection needs at least one server group set! Check config key: " + this.configPrefix + "_groups");
            }
            int groupID = 0;
            final StringTokenizer st = new StringTokenizer(temp, ",", false);
            while (st.hasMoreTokens()) {
                groupID = Integer.parseInt(st.nextToken().trim());
                this.SERVERGROUPPROTECTION_GROUPS.addElement(groupID);
                this.SERVERGROUPPROTECTION_ADDALLOWED.addElement(this.modClass.getServerGroupType(groupID) == 1);
            }
            this.SERVERGROUPPROTECTION_COMPLAINADD = config.getValue(String.valueOf(this.configPrefix) + "_add_complain", "no").trim().equalsIgnoreCase("yes");
            this.SERVERGROUPPROTECTION_ADD_MISSING_GROUPS = config.getValue(String.valueOf(this.configPrefix) + "_add_missing_groups", "no").trim().equalsIgnoreCase("yes");
            this.SERVERGROUPPROTECTION_KICK = config.getValue(String.valueOf(this.configPrefix) + "_kick", "no").trim().equalsIgnoreCase("yes");
            if (this.SERVERGROUPPROTECTION_KICK) {
                this.SERVERGROUPPROTECTION_MESSAGE_MODE = "kick";
            }
            this.SERVERGROUPPROTECTION_MESSAGE_MODE = config.getValue(String.valueOf(this.configPrefix) + "_message_mode", "chat").trim();
            this.SERVERGROUPPROTECTION_FILE = config.getValue(String.valueOf(this.configPrefix) + "_file");
            if (!this.loadServerGroupProtectionFile(config)) {
                throw new BotConfigurationException("Server Group Protection configuration does not exists or error while loading!");
            }
            if (!this.modClass.isMessageLengthValid(this.SERVERGROUPPROTECTION_MESSAGE_MODE, this.SERVERGROUPPROTECTION_MESSAGE)) {
                this.modClass.addLogEntry(this.configPrefix, (byte)2, "Server Group Protection message is to long! Make sure that " + this.SERVERGROUPPROTECTION_MESSAGE_MODE + " messages are not longer than " + Short.toString(this.modClass.getMaxMessageLength(this.SERVERGROUPPROTECTION_MESSAGE_MODE)) + " characters (including spaces and BBCode)" + ((this.modClass.getMySQLConnection() == null) ? (", check file: " + this.SERVERGROUPPROTECTION_FILE) : ""), true);
            }
            this.pluginEnabled = true;
        }
        catch (NumberFormatException e) {
            final NumberFormatException nfe = new NumberFormatException("Config value of \"" + lastNumberValue + "\" is not a number! Current value: " + config.getValue(lastNumberValue, "not set"));
            nfe.setStackTrace(e.getStackTrace());
            throw nfe;
        }
        return this.pluginEnabled;
    }
    
    boolean loadServerGroupProtectionFile(final ArrangedPropertiesWriter config) {
        if (this.modClass.getMySQLConnection() != null) {
            final MySQLConnect mysqlConnect = this.modClass.getMySQLConnection();
            this.SERVERGROUPPROTECTION_MESSAGE = config.getValue(String.valueOf(this.configPrefix) + "_message");
            boolean retValue = false;
            PreparedStatement pst = null;
            ResultSet rs = null;
            try {
                mysqlConnect.connect();
                pst = mysqlConnect.getPreparedStatement("SELECT servergroup_id, client_unique_id FROM jts3servermod_servergroupprotection WHERE instance_id = ? AND prefix = ?");
                pst.setInt(1, this.modClass.getInstanceID());
                pst.setString(2, this.configPrefix);
                rs = pst.executeQuery();
                rs.last();
                final int rowCount = rs.getRow();
                if (rowCount > 0) {
                    rs.beforeFirst();
                    this.SERVERGROUPPROTECTION_CLIENTS.clear();
                    for (int i = 0; i < this.SERVERGROUPPROTECTION_GROUPS.size(); ++i) {
                        this.SERVERGROUPPROTECTION_CLIENTS.addElement(new Vector<String>());
                    }
                    while (rs.next()) {
                        try {
                            final int indexPos = this.SERVERGROUPPROTECTION_GROUPS.indexOf(rs.getInt(1));
                            if (indexPos == -1) {
                                continue;
                            }
                            this.SERVERGROUPPROTECTION_CLIENTS.elementAt(indexPos).addElement(rs.getString(2).trim());
                        }
                        catch (Exception ex) {}
                    }
                    retValue = true;
                }
            }
            catch (Exception e2) {
                retValue = false;
                return retValue;
            }
            finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                }
                catch (Exception ex2) {}
                try {
                    if (pst != null) {
                        pst.close();
                    }
                }
                catch (Exception ex3) {}
                mysqlConnect.close();
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            }
            catch (Exception ex4) {}
            try {
                if (pst != null) {
                    pst.close();
                }
            }
            catch (Exception ex5) {}
            mysqlConnect.close();
            return retValue;
        }
        if (this.SERVERGROUPPROTECTION_FILE == null) {
            this.modClass.addLogEntry(this.configPrefix, (byte)3, "Path to Server Group Protection config file was not set in bot config! Check config key: " + this.configPrefix + "_file", true);
            return false;
        }
        this.SERVERGROUPPROTECTION_FILE = this.SERVERGROUPPROTECTION_FILE.trim();
        try {
            final BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.SERVERGROUPPROTECTION_FILE), this.modClass.getMessageEncoding()));
            String line = br.readLine();
            if (this.modClass.getMessageEncoding().equalsIgnoreCase("UTF-8") && line != null && line.charAt(0) == '\ufeff') {
                line = line.substring(1);
            }
            if (line == null || !line.equals("# JTS3ServerMod Config File")) {
                this.modClass.addLogEntry(this.configPrefix, (byte)3, "Special config file header is missing at Server Group Protection config file! File path: " + this.SERVERGROUPPROTECTION_FILE, true);
                this.modClass.addLogEntry(this.configPrefix, (byte)3, "Check if you set the right file at config key: " + this.configPrefix + "_file", true);
                br.close();
                return false;
            }
            this.SERVERGROUPPROTECTION_CLIENTS.clear();
            for (int j = 0; j < this.SERVERGROUPPROTECTION_GROUPS.size(); ++j) {
                this.SERVERGROUPPROTECTION_CLIENTS.addElement(new Vector<String>());
            }
            int count = 0;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("#")) {
                    continue;
                }
                if (line.length() <= 3) {
                    continue;
                }
                if (count == 0) {
                    line = line.replace("\\n", "\n");
                    this.SERVERGROUPPROTECTION_MESSAGE = line;
                }
                if (count >= 1) {
                    final int pos = line.indexOf(",");
                    if (pos == -1) {
                        continue;
                    }
                    if (pos == 0) {
                        continue;
                    }
                    try {
                        final int indexPos2 = this.SERVERGROUPPROTECTION_GROUPS.indexOf(Integer.parseInt(line.substring(0, pos)));
                        if (indexPos2 == -1) {
                            continue;
                        }
                        this.SERVERGROUPPROTECTION_CLIENTS.elementAt(indexPos2).addElement(line.substring(pos + 1).trim());
                    }
                    catch (Exception e2) {
                        continue;
                    }
                }
                ++count;
            }
            br.close();
        }
        catch (FileNotFoundException fnfe) {
            this.modClass.addLogEntry(this.configPrefix, (byte)3, "Server Group Protection config file you set at config key \"" + this.configPrefix + "_file\" does not exist or missing permission for reading, check file path: " + this.SERVERGROUPPROTECTION_FILE, true);
            return false;
        }
        catch (Exception e) {
            this.modClass.addLogEntry(this.configPrefix, (byte)3, "Unknown error while loading Server Group Protection config file! Check file you set at config key \"" + this.configPrefix + "_file\", the file path: " + this.SERVERGROUPPROTECTION_FILE, true);
            this.modClass.addLogEntry(this.configPrefix, e, true);
            return false;
        }
        return true;
    }
    
    private boolean saveServerGroupProtectionFile() {
        if (this.modClass.getMySQLConnection() != null) {
            final MySQLConnect mysqlConnect = this.modClass.getMySQLConnection();
            boolean retValue = false;
            PreparedStatement pst = null;
            try {
                mysqlConnect.connect();
                pst = mysqlConnect.getPreparedStatement("DELETE FROM jts3servermod_servergroupprotection WHERE instance_id = " + Integer.toString(this.modClass.getInstanceID()) + " AND prefix = ?");
                pst.setString(1, this.configPrefix);
                pst.executeUpdate();
                pst = mysqlConnect.getPreparedStatement("INSERT INTO jts3servermod_servergroupprotection (instance_id, prefix, servergroup_id, client_unique_id) VALUES (" + Integer.toString(this.modClass.getInstanceID()) + ", ?, ?, ?)");
                if (this.SERVERGROUPPROTECTION_CLIENTS != null && this.SERVERGROUPPROTECTION_GROUPS != null) {
                    for (int i = 0; i < this.SERVERGROUPPROTECTION_CLIENTS.size(); ++i) {
                        for (int j = 0; j < this.SERVERGROUPPROTECTION_CLIENTS.elementAt(i).size(); ++j) {
                            pst.setString(1, this.configPrefix);
                            pst.setInt(2, this.SERVERGROUPPROTECTION_GROUPS.elementAt(i));
                            pst.setString(3, this.SERVERGROUPPROTECTION_CLIENTS.elementAt(i).elementAt(j));
                            pst.executeUpdate();
                        }
                    }
                }
                retValue = true;
            }
            catch (Exception e2) {
                retValue = false;
                return retValue;
            }
            finally {
                mysqlConnect.close();
            }
            mysqlConnect.close();
            return retValue;
        }
        if (this.SERVERGROUPPROTECTION_FILE == null) {
            return false;
        }
        try {
            final PrintStream ps = new PrintStream(this.SERVERGROUPPROTECTION_FILE, this.modClass.getMessageEncoding());
            ps.println("# JTS3ServerMod Config File");
            ps.println("# The first line is the kick or chat message for the Server Group Protection.");
            ps.println("# You can use the following keywords, which will be replaced:");
            ps.println("# %SERVER_GROUP_ID% - Replaced with the server group id.");
            ps.println("# %SERVER_GROUP_NAME% - Replaced with the server group name.");
            ps.println("# Typical BBCode like in Teamspeak 3 Client possible.");
            if (this.SERVERGROUPPROTECTION_MESSAGE == null) {
                ps.println();
            }
            else {
                ps.println(this.SERVERGROUPPROTECTION_MESSAGE.replace("\n", "\\n"));
            }
            ps.println();
            ps.println("# This is the list of allowed clients in the protected server groups.");
            ps.println("# One line per client starting with the server group id, followed by a comma,");
            ps.println("# and ends with the unique id of the client.");
            ps.println("# If a client is member of two protected groups, make two lines with the");
            ps.println("# same unique id, but different server group id.");
            if (this.SERVERGROUPPROTECTION_CLIENTS != null && this.SERVERGROUPPROTECTION_GROUPS != null) {
                for (int k = 0; k < this.SERVERGROUPPROTECTION_CLIENTS.size(); ++k) {
                    for (int l = 0; l < this.SERVERGROUPPROTECTION_CLIENTS.elementAt(k).size(); ++l) {
                        ps.print(this.SERVERGROUPPROTECTION_GROUPS.elementAt(k));
                        ps.print(",");
                        ps.println(this.SERVERGROUPPROTECTION_CLIENTS.elementAt(k).elementAt(l));
                    }
                }
            }
            ps.flush();
            ps.close();
        }
        catch (Exception e) {
            this.modClass.addLogEntry(this.configPrefix, e, true);
            return false;
        }
        return true;
    }
    
    int addServerGroupProtectionEntry(final int serverGroupID, final String clientUniqueID) {
        if (this.SERVERGROUPPROTECTION_CLIENTS == null || this.SERVERGROUPPROTECTION_GROUPS == null) {
            return -3;
        }
        final int posGroup = this.SERVERGROUPPROTECTION_GROUPS.indexOf(serverGroupID);
        if (posGroup < 0) {
            return -2;
        }
        final int posClient = this.SERVERGROUPPROTECTION_CLIENTS.elementAt(posGroup).indexOf(clientUniqueID);
        if (posClient >= 0) {
            return 0;
        }
        this.SERVERGROUPPROTECTION_CLIENTS.elementAt(posGroup).addElement(clientUniqueID);
        return this.saveServerGroupProtectionFile() ? 1 : -1;
    }
    
    int removeServerGroupProtectionEntry(final int serverGroupID, final String clientUniqueID) {
        if (this.SERVERGROUPPROTECTION_CLIENTS == null || this.SERVERGROUPPROTECTION_GROUPS == null) {
            return -3;
        }
        final int posGroup = this.SERVERGROUPPROTECTION_GROUPS.indexOf(serverGroupID);
        if (posGroup < 0) {
            return -2;
        }
        final int posClient = this.SERVERGROUPPROTECTION_CLIENTS.elementAt(posGroup).indexOf(clientUniqueID);
        if (posClient < 0) {
            return 0;
        }
        this.SERVERGROUPPROTECTION_CLIENTS.elementAt(posGroup).remove(posClient);
        return this.saveServerGroupProtectionFile() ? 1 : -1;
    }
    
    public void setListModes(final BitSet listOptions) {
        listOptions.set(1);
        listOptions.set(4);
    }
    
    public String[] botChatCommandList(final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        if (isFullAdmin) {
            final String[] cmdList = { "add <server group id> <client unique id>", "remove <server group id> <client unique id>" };
            return cmdList;
        }
        return null;
    }
    
    public String botChatCommandHelp(final String command) {
        if (command.equals("add")) {
            return "Adds a client unique id to a protected server group (this will also saved into config).";
        }
        if (command.equals("remove")) {
            return "Removes a client unique id from a protected server group (this will also saved into config).";
        }
        return null;
    }
    
    public boolean handleChatCommands(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        if (isFullAdmin) {
            if (msg.toLowerCase().startsWith("add ") || msg.toLowerCase().equals("add")) {
                this.handleAddCommand(msg, eventInfo);
            }
            else {
                if (!msg.toLowerCase().startsWith("remove ") && !msg.toLowerCase().equals("remove")) {
                    return false;
                }
                this.handleRemoveCommand(msg, eventInfo);
            }
        }
        else {
            try {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master! You have to be full bot admin to use this command.");
            }
            catch (Exception e) {
                this.modClass.addLogEntry(this.configPrefix, e, false);
            }
        }
        return true;
    }
    
    private void handleAddCommand(final String msg, final HashMap<String, String> eventInfo) {
        try {
            if (msg.length() < 30) {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !" + this.configPrefix + " add <server group id> <client unique id>");
            }
            else {
                try {
                    final StringTokenizer st = new StringTokenizer(msg.substring(4), " ", false);
                    int serverGroupID = -1;
                    String clientUniqueID = null;
                    String tmp = st.nextToken().trim();
                    try {
                        serverGroupID = Integer.parseInt(tmp);
                        tmp = st.nextToken().trim();
                        clientUniqueID = new String(tmp);
                    }
                    catch (Exception e2) {
                        try {
                            clientUniqueID = new String(tmp);
                            tmp = st.nextToken().trim();
                            serverGroupID = Integer.parseInt(tmp);
                        }
                        catch (Exception ex) {}
                    }
                    if (clientUniqueID.length() < 20) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while reading given client unique id!");
                    }
                    else if (serverGroupID < 1) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while reading given server group id!");
                    }
                    else {
                        final int retValue = this.addServerGroupProtectionEntry(serverGroupID, clientUniqueID);
                        if (retValue == 1) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Successfully added client to protected server group!");
                        }
                        else if (retValue == 0) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Client was already on that list for server group " + Integer.toString(serverGroupID) + "!");
                        }
                        else if (retValue == -1) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Unable to save server group protection configuration file! Check if the configuration file is write protected!");
                        }
                        else if (retValue == -2) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Unable to add client to protected server group! Server group protection feature must be enabled and server group " + Integer.toString(serverGroupID) + " has to be on the watch list of the server group protection feature. Please make sure that this feature is enabled and add this server group to config value servergroupprotection_groups first!");
                        }
                        else if (retValue == -3) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Unable to add client to protected server group! Server group protection feature is disabled, enable it first!");
                        }
                    }
                }
                catch (NumberFormatException nfe) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while reading given server group id!");
                }
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(this.configPrefix, e, false);
        }
    }
    
    private void handleRemoveCommand(final String msg, final HashMap<String, String> eventInfo) {
        try {
            if (msg.length() < 33) {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !" + this.configPrefix + " remove <server group id> <client unique id>");
            }
            else {
                try {
                    final StringTokenizer st = new StringTokenizer(msg.substring(7), " ", false);
                    int serverGroupID = -1;
                    String clientUniqueID = null;
                    String tmp = st.nextToken().trim();
                    try {
                        serverGroupID = Integer.parseInt(tmp);
                        tmp = st.nextToken().trim();
                        clientUniqueID = new String(tmp);
                    }
                    catch (Exception e2) {
                        try {
                            clientUniqueID = new String(tmp);
                            tmp = st.nextToken().trim();
                            serverGroupID = Integer.parseInt(tmp);
                        }
                        catch (Exception ex) {}
                    }
                    if (clientUniqueID.length() < 20) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while reading given client unique id!");
                    }
                    else if (serverGroupID < 1) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while reading given server group id!");
                    }
                    else {
                        final int retValue = this.removeServerGroupProtectionEntry(serverGroupID, clientUniqueID);
                        if (retValue == 1) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Successfully removed client from protected server group!");
                        }
                        else if (retValue == 0) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Client is already not on that list for server group " + Integer.toString(serverGroupID) + "!");
                        }
                        else if (retValue == -1) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Unable to save server group protection configuration file! Check if the configuration file is write protected!");
                        }
                        else if (retValue == -2) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Unable to removed client from protected server group! Server group protection feature must be enabled and server group " + Integer.toString(serverGroupID) + " has to be on the watch list of the server group protection feature. Please make sure that this feature is enabled and add this server group to config value servergroupprotection_groups first!");
                        }
                        else if (retValue == -3) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Unable to removed client from protected server group! Server group protection feature is disabled, enable it first!");
                        }
                    }
                }
                catch (NumberFormatException nfe) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while reading given server group id!");
                }
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(this.configPrefix, e, false);
        }
    }
    
    public void handleClientEvents(final String eventType, final HashMap<String, String> eventInfo) {
    }
    
    public void handleClientCheck(final Vector<HashMap<String, String>> clientList) {
        if (!this.pluginEnabled) {
            return;
        }
        for (final HashMap<String, String> clientInfo : clientList) {
            if (clientInfo.get("client_type").equals("0")) {
                final int clientID = Integer.parseInt(clientInfo.get("clid"));
                final StringTokenizer groupTokenizer = new StringTokenizer(clientInfo.get("client_servergroups"), ",", false);
                int groupID = -1;
                String sgpMessage = "";
                final Vector<Integer> clientHasGroups = new Vector<Integer>();
                while (groupTokenizer.hasMoreTokens()) {
                    groupID = Integer.parseInt(groupTokenizer.nextToken());
                    clientHasGroups.addElement(groupID);
                    for (int i = 0; i < this.SERVERGROUPPROTECTION_GROUPS.size(); ++i) {
                        if (groupID == this.SERVERGROUPPROTECTION_GROUPS.elementAt(i) && this.SERVERGROUPPROTECTION_CLIENTS.elementAt(i).indexOf(clientInfo.get("client_unique_identifier")) == -1) {
                            final HashMap<String, String> response = this.queryLib.doCommand("servergroupdelclient sgid=" + Integer.toString(groupID) + " cldbid=" + clientInfo.get("client_database_id"));
                            if (response.get("id").equals("0")) {
                                this.modClass.addLogEntry(this.configPrefix, (byte)1, "Removed client \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + ") from server group " + Integer.toString(groupID) + "!", false);
                                this.fel.clearException(Integer.parseInt(clientInfo.get("client_database_id")));
                            }
                            else {
                                try {
                                    throw new TS3ServerQueryException("ServerGroupProtection", response.get("id"), response.get("msg"), response.get("extra_msg"), response.get("failed_permid"));
                                }
                                catch (TS3ServerQueryException sqe) {
                                    if (!this.fel.existsException(sqe, Integer.parseInt(clientInfo.get("client_database_id")))) {
                                        this.fel.addException(sqe, Integer.parseInt(clientInfo.get("client_database_id")));
                                        this.modClass.addLogEntry(this.configPrefix, (byte)3, "Error while removing client \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + ") from server group " + Integer.toString(groupID) + "!", false);
                                        this.modClass.addLogEntry(this.configPrefix, sqe, false);
                                    }
                                }
                            }
                            if (this.SERVERGROUPPROTECTION_COMPLAINADD) {
                                try {
                                    this.queryLib.complainAdd(Integer.parseInt(clientInfo.get("client_database_id")), "Not allowed server group (id: " + Integer.toString(groupID) + "): " + clientInfo.get("client_nickname"));
                                    this.modClass.addLogEntry(this.configPrefix, (byte)1, "Added complaint to client \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + "), not allowed to be in the server group " + Integer.toString(groupID) + "!", false);
                                    this.fel.clearException(Integer.parseInt(clientInfo.get("client_database_id")));
                                }
                                catch (TS3ServerQueryException sqe) {
                                    if (!this.fel.existsException(sqe, Integer.parseInt(clientInfo.get("client_database_id")))) {
                                        this.fel.addException(sqe, Integer.parseInt(clientInfo.get("client_database_id")));
                                        this.modClass.addLogEntry(this.configPrefix, (byte)3, "Error while adding complaint to client \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + "), not allowed to be in the server group " + Integer.toString(groupID) + "!", false);
                                        this.modClass.addLogEntry(this.configPrefix, sqe, false);
                                    }
                                }
                                catch (Exception e) {
                                    this.modClass.addLogEntry(this.configPrefix, (byte)3, "Error while adding complaint to client \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + "), not allowed to be in the server group " + Integer.toString(groupID) + "!", false);
                                    this.modClass.addLogEntry(this.configPrefix, e, false);
                                }
                            }
                            final String sgName = this.modClass.getServerGroupName(groupID);
                            sgpMessage = new String(this.SERVERGROUPPROTECTION_MESSAGE);
                            sgpMessage = sgpMessage.replace("%SERVER_GROUP_ID%", Integer.toString(groupID));
                            sgpMessage = sgpMessage.replace("%SERVER_GROUP_NAME%", (sgName == null) ? "Unknown" : sgName);
                            if (this.SERVERGROUPPROTECTION_KICK) {
                                try {
                                    this.queryLib.kickClient(clientID, false, sgpMessage);
                                    this.modClass.addLogEntry(this.configPrefix, (byte)1, "Client \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + ") was kicked for being member of the protected server group " + Integer.toString(groupID) + ", unique ID is not on list!", false);
                                    this.fel.clearException(Integer.parseInt(clientInfo.get("client_database_id")));
                                }
                                catch (TS3ServerQueryException sqe2) {
                                    if (!this.fel.existsException(sqe2, Integer.parseInt(clientInfo.get("client_database_id")))) {
                                        this.fel.addException(sqe2, Integer.parseInt(clientInfo.get("client_database_id")));
                                        this.modClass.addLogEntry(this.configPrefix, (byte)3, "Error while kicking client \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + ") for being member of the protected server group " + Integer.toString(groupID) + "!", false);
                                        this.modClass.addLogEntry(this.configPrefix, sqe2, false);
                                    }
                                }
                                catch (Exception e2) {
                                    this.modClass.addLogEntry(this.configPrefix, (byte)3, "Error while kicking client \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + ") for being member of the protected server group " + Integer.toString(groupID) + "!", false);
                                    this.modClass.addLogEntry(this.configPrefix, e2, false);
                                }
                            }
                            else {
                                this.modClass.sendMessageToClient(this.configPrefix, this.SERVERGROUPPROTECTION_MESSAGE_MODE, clientID, sgpMessage);
                            }
                        }
                    }
                }
                if (!this.SERVERGROUPPROTECTION_ADD_MISSING_GROUPS) {
                    continue;
                }
                for (int i = 0; i < this.SERVERGROUPPROTECTION_GROUPS.size(); ++i) {
                    if (this.SERVERGROUPPROTECTION_ADDALLOWED.elementAt(i)) {
                        if (this.SERVERGROUPPROTECTION_CLIENTS.elementAt(i).indexOf(clientInfo.get("client_unique_identifier")) != -1 && clientHasGroups.indexOf(this.SERVERGROUPPROTECTION_GROUPS.elementAt(i)) == -1) {
                            final HashMap<String, String> response = this.queryLib.doCommand("servergroupaddclient sgid=" + Integer.toString(this.SERVERGROUPPROTECTION_GROUPS.elementAt(i)) + " cldbid=" + clientInfo.get("client_database_id"));
                            if (response.get("id").equals("0")) {
                                this.modClass.addLogEntry(this.configPrefix, (byte)1, "Added client \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + ") to server group " + Integer.toString(this.SERVERGROUPPROTECTION_GROUPS.elementAt(i)) + "!", false);
                                this.fel.clearException(Integer.parseInt(clientInfo.get("client_database_id")));
                            }
                            else {
                                try {
                                    throw new TS3ServerQueryException("ServerGroupProtection", response.get("id"), response.get("msg"), response.get("extra_msg"), response.get("failed_permid"));
                                }
                                catch (TS3ServerQueryException sqe) {
                                    if (!this.fel.existsException(sqe, Integer.parseInt(clientInfo.get("client_database_id")))) {
                                        this.fel.addException(sqe, Integer.parseInt(clientInfo.get("client_database_id")));
                                        this.modClass.addLogEntry(this.configPrefix, (byte)3, "Error while adding client \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + ") to server group " + Integer.toString(this.SERVERGROUPPROTECTION_GROUPS.elementAt(i)) + "!", false);
                                        this.modClass.addLogEntry(this.configPrefix, sqe, false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
