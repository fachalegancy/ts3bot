package de.stefan1200.jts3servermod.functions;

import de.stefan1200.jts3servermod.interfaces.*;
import de.stefan1200.jts3servermod.*;
import java.io.*;
import de.stefan1200.util.*;
import java.sql.*;
import de.stefan1200.jts3serverquery.*;
import java.util.*;
import java.util.regex.*;

public class BadNicknameCheck implements HandleBotEvents, HandleClientList, LoadConfiguration
{
    private String configPrefix;
    private JTS3ServerMod_Interface modClass;
    private JTS3ServerQuery queryLib;
    private boolean pluginEnabled;
    private boolean BADNICKNAME_KICK;
    private String BADNICKNAME_FILE;
    private boolean BADNICKNAME_COMPLAINADD;
    private Vector<Integer> BADNICKNAME_GROUP_LIST;
    private boolean BADNICKNAME_GROUP_LIST_IGNORE;
    private String BADNICKNAME_MESSAGE;
    private String BADNICKNAME_MESSAGE_MODE;
    private Vector<Pattern> BADNICKNAME_RULES;
    private Vector<String> BADNICKNAME_CACHE;
    private FunctionExceptionLog fel;
    
    public BadNicknameCheck() {
        this.configPrefix = "";
        this.modClass = null;
        this.queryLib = null;
        this.pluginEnabled = false;
        this.BADNICKNAME_KICK = true;
        this.BADNICKNAME_FILE = null;
        this.BADNICKNAME_COMPLAINADD = false;
        this.BADNICKNAME_GROUP_LIST = new Vector<Integer>();
        this.BADNICKNAME_GROUP_LIST_IGNORE = true;
        this.BADNICKNAME_MESSAGE = null;
        this.BADNICKNAME_MESSAGE_MODE = null;
        this.BADNICKNAME_RULES = new Vector<Pattern>();
        this.BADNICKNAME_CACHE = new Vector<String>();
        this.fel = new FunctionExceptionLog();
    }
    
    public void initClass(final JTS3ServerMod_Interface modClass, final JTS3ServerQuery queryLib, final String prefix) {
        this.modClass = modClass;
        this.queryLib = queryLib;
        this.configPrefix = prefix.trim();
    }
    
    public void handleOnBotConnect() {
        if (!this.pluginEnabled) {
            return;
        }
        String msg = "Bad Nickname Check is enabled, " + Integer.toString(this.BADNICKNAME_RULES.size()) + " rules loaded";
        if (this.BADNICKNAME_COMPLAINADD && this.BADNICKNAME_KICK) {
            msg = String.valueOf(msg) + " (client will be kicked and a complaint will be added)";
        }
        else if (this.BADNICKNAME_KICK) {
            msg = String.valueOf(msg) + " (client will be kicked)";
        }
        else if (this.BADNICKNAME_COMPLAINADD) {
            msg = String.valueOf(msg) + " (complaint will be added)";
        }
        this.modClass.addLogEntry(this.configPrefix, (byte)1, msg, true);
    }
    
    public void handleAfterCacheUpdate() {
    }
    
    public void activate() {
    }
    
    public void disable() {
    }
    
    public void unload() {
        this.BADNICKNAME_GROUP_LIST = null;
        this.BADNICKNAME_RULES = null;
    }
    
    public boolean multipleInstances() {
        return true;
    }
    
    public void initConfig(final ArrangedPropertiesWriter config) {
        config.addKey(String.valueOf(this.configPrefix) + "_kick", "Kick client with a bad nickname? Set yes or no here!", "yes");
        config.addKey(String.valueOf(this.configPrefix) + "_add_complain", "Add complain entry to the user? Set yes or no here!", "no");
        config.addKey(String.valueOf(this.configPrefix) + "_group_list", "A comma separated list (without spaces) of server group ids.\nDepends on the given mode, this server groups can be ignored or only this server groups will be checked!\nIf no server groups should be ignored, set no server groups here and select the group list mode ignore!");
        config.addKey(String.valueOf(this.configPrefix) + "_group_list_mode", "Select one of the two modes for the server group list.\nignore = The selected server groups will be ignored and can have bad nicknames.\nonly = Only the selected server groups will be checked.", "ignore");
        config.addKey(String.valueOf(this.configPrefix) + "_message_mode", "Select the message mode, how the client should get the message.\npoke or chat are valid values!\nIf client kick is activated, the message will be always used as kick message!", "poke");
        if (this.modClass.getMySQLConnection() == null) {
            config.addKey(String.valueOf(this.configPrefix) + "_file", "Path to file which contains the bad nickname message and check rules.", "config/server1/badnickname.cfg");
        }
        if (this.modClass.getMySQLConnection() != null) {
            config.addKey(String.valueOf(this.configPrefix) + "_message", "Set kick message for using a bad nickname.\nYou can use the following keywords, which will be replaced:\n%CLIENT_NAME% - Client Name\nYou can use \\n for a new line and typical BBCode like in Teamspeak 3 Client.");
        }
    }
    
    public boolean loadConfig(final ArrangedPropertiesWriter config, final boolean slowMode) throws BotConfigurationException, NumberFormatException {
        String lastNumberValue = "";
        String temp = null;
        this.pluginEnabled = false;
        try {
            this.BADNICKNAME_KICK = config.getValue(String.valueOf(this.configPrefix) + "_kick", "yes").trim().equalsIgnoreCase("yes");
            this.BADNICKNAME_MESSAGE_MODE = config.getValue(String.valueOf(this.configPrefix) + "_message_mode", "chat").trim();
            this.BADNICKNAME_FILE = config.getValue(String.valueOf(this.configPrefix) + "_file");
            if (!this.loadBadNicknameFile(config)) {
                throw new BotConfigurationException("Bad Nickname Check message and configuration could not be loaded!");
            }
            if (this.BADNICKNAME_MESSAGE == null || this.BADNICKNAME_MESSAGE.length() == 0) {
                throw new BotConfigurationException("Bad Nickname Check message could not be loaded!");
            }
            if (!this.modClass.isMessageLengthValid(this.BADNICKNAME_KICK ? "kick" : this.BADNICKNAME_MESSAGE_MODE, this.BADNICKNAME_MESSAGE)) {
                this.modClass.addLogEntry(this.configPrefix, (byte)2, "Bad Nickname Check message is to long! Make sure that " + (this.BADNICKNAME_KICK ? "kick" : this.BADNICKNAME_MESSAGE_MODE) + " messages are not longer than " + Short.toString(this.modClass.getMaxMessageLength(this.BADNICKNAME_KICK ? "kick" : this.BADNICKNAME_MESSAGE_MODE)) + " characters (including spaces and BBCode)" + ((this.modClass.getMySQLConnection() == null) ? (", check file: " + this.BADNICKNAME_FILE) : ""), true);
            }
            temp = null;
            this.BADNICKNAME_GROUP_LIST.clear();
            temp = config.getValue(String.valueOf(this.configPrefix) + "_group_list");
            lastNumberValue = String.valueOf(this.configPrefix) + "_group_list";
            if (temp != null && temp.length() > 0) {
                final StringTokenizer st = new StringTokenizer(temp, ",", false);
                while (st.hasMoreTokens()) {
                    this.BADNICKNAME_GROUP_LIST.addElement(Integer.parseInt(st.nextToken().trim()));
                }
            }
            this.BADNICKNAME_GROUP_LIST_IGNORE = !config.getValue(String.valueOf(this.configPrefix) + "_group_list_mode", "ignore").trim().equalsIgnoreCase("only");
            this.BADNICKNAME_COMPLAINADD = config.getValue(String.valueOf(this.configPrefix) + "_add_complain", "no").trim().equalsIgnoreCase("yes");
            if (this.BADNICKNAME_RULES.size() == 0) {
                throw new BotConfigurationException("No bad nickname check rules was found! Config file: " + this.BADNICKNAME_FILE);
            }
            this.pluginEnabled = true;
        }
        catch (NumberFormatException e) {
            final NumberFormatException nfe = new NumberFormatException("Config value of \"" + lastNumberValue + "\" is not a number! Current value: " + config.getValue(lastNumberValue, "not set"));
            nfe.setStackTrace(e.getStackTrace());
            throw nfe;
        }
        return this.pluginEnabled;
    }
    
    boolean loadBadNicknameFile(final ArrangedPropertiesWriter config) {
        if (this.modClass.getMySQLConnection() != null) {
            final MySQLConnect mysqlConnect = this.modClass.getMySQLConnection();
            this.BADNICKNAME_MESSAGE = config.getValue(String.valueOf(this.configPrefix) + "_message");
            boolean retValue = false;
            PreparedStatement pst = null;
            ResultSet rs = null;
            try {
                mysqlConnect.connect();
                pst = mysqlConnect.getPreparedStatement("SELECT textentry FROM jts3servermod_badnickname WHERE instance_id = ? AND prefix = ?");
                pst.setInt(1, this.modClass.getInstanceID());
                pst.setString(2, this.configPrefix);
                rs = pst.executeQuery();
                this.BADNICKNAME_RULES.clear();
                while (rs.next()) {
                    this.BADNICKNAME_RULES.addElement(Pattern.compile(rs.getString(1), 66));
                }
                retValue = true;
            }
            catch (Exception e2) {
                retValue = false;
                return retValue;
            }
            finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                }
                catch (Exception ex) {}
                try {
                    if (pst != null) {
                        pst.close();
                    }
                }
                catch (Exception ex2) {}
                mysqlConnect.close();
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            }
            catch (Exception ex3) {}
            try {
                if (pst != null) {
                    pst.close();
                }
            }
            catch (Exception ex4) {}
            mysqlConnect.close();
            return retValue;
        }
        if (this.BADNICKNAME_FILE == null) {
            this.modClass.addLogEntry(this.configPrefix, (byte)3, "Path to Bad Nickname Check config file was not set in bot config! Check config key: " + this.configPrefix + "_file", true);
            return false;
        }
        this.BADNICKNAME_FILE = this.BADNICKNAME_FILE.trim();
        try {
            final BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.BADNICKNAME_FILE), this.modClass.getMessageEncoding()));
            this.BADNICKNAME_RULES.clear();
            String line = br.readLine();
            if (this.modClass.getMessageEncoding().equalsIgnoreCase("UTF-8") && line != null && line.charAt(0) == '\ufeff') {
                line = line.substring(1);
            }
            if (line == null || !line.equals("# JTS3ServerMod Config File")) {
                this.modClass.addLogEntry(this.configPrefix, (byte)3, "Special config file header is missing at Bad Nickname Check config file! File path: " + this.BADNICKNAME_FILE, true);
                this.modClass.addLogEntry(this.configPrefix, (byte)3, "Check if you set the right file at config key: " + this.configPrefix + "_file", true);
                br.close();
                return false;
            }
            int count = 0;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("#")) {
                    continue;
                }
                if (line.length() <= 3) {
                    continue;
                }
                if (count == 0) {
                    line = line.replace("\\n", "\n");
                    this.BADNICKNAME_MESSAGE = line;
                }
                if (count >= 1) {
                    this.BADNICKNAME_RULES.addElement(Pattern.compile(line, 66));
                }
                ++count;
            }
            br.close();
        }
        catch (FileNotFoundException fnfe) {
            this.modClass.addLogEntry(this.configPrefix, (byte)3, "Bad Nickname Check config file you set at config key \"" + this.configPrefix + "_file\" does not exist or missing permission for reading, check file path: " + this.BADNICKNAME_FILE, true);
            return false;
        }
        catch (Exception e) {
            this.modClass.addLogEntry(this.configPrefix, (byte)3, "Unknown error while loading Bad Nickname Check config file! Check file you set at config key \"" + this.configPrefix + "_file\", the file path: " + this.BADNICKNAME_FILE, true);
            this.modClass.addLogEntry(this.configPrefix, e, true);
            return false;
        }
        return true;
    }
    
    public void setListModes(final BitSet listOptions) {
        listOptions.set(1);
    }
    
    public void handleClientCheck(final Vector<HashMap<String, String>> clientList) {
        if (!this.pluginEnabled) {
            return;
        }
        for (final HashMap<String, String> clientInfo : clientList) {
            if (clientInfo.get("client_type").equals("0")) {
                if (!this.BADNICKNAME_KICK && this.BADNICKNAME_CACHE.indexOf(clientInfo.get("client_nickname")) >= 0) {
                    continue;
                }
                final int clientID = Integer.parseInt(clientInfo.get("clid"));
                final boolean result = this.modClass.isGroupListed(clientInfo.get("client_servergroups"), this.BADNICKNAME_GROUP_LIST);
                if (this.BADNICKNAME_GROUP_LIST_IGNORE) {
                    if (result) {
                        continue;
                    }
                }
                else if (!result) {
                    continue;
                }
                for (final Pattern rule : this.BADNICKNAME_RULES) {
                    final Matcher ruleCheck = rule.matcher(clientInfo.get("client_nickname"));
                    if (ruleCheck.matches()) {
                        if (this.BADNICKNAME_COMPLAINADD) {
                            try {
                                this.queryLib.complainAdd(Integer.parseInt(clientInfo.get("client_database_id")), "Bad Nickname: " + clientInfo.get("client_nickname"));
                                this.modClass.addLogEntry(this.configPrefix, (byte)1, "Added complaint to client with the bad nickname \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + ")!", false);
                                this.fel.clearException(Integer.parseInt(clientInfo.get("client_database_id")));
                            }
                            catch (TS3ServerQueryException sqe) {
                                if (!this.fel.existsException(sqe, Integer.parseInt(clientInfo.get("client_database_id")))) {
                                    this.fel.addException(sqe, Integer.parseInt(clientInfo.get("client_database_id")));
                                    this.modClass.addLogEntry(this.configPrefix, (byte)3, "Error while adding complaint to client with the bad nickname \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + ")!", false);
                                    this.modClass.addLogEntry(this.configPrefix, sqe, false);
                                }
                            }
                            catch (Exception e) {
                                this.modClass.addLogEntry(this.configPrefix, (byte)3, "Error while adding complaint to client with the bad nickname \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + ")!", false);
                                this.modClass.addLogEntry(this.configPrefix, e, false);
                            }
                        }
                        if (this.BADNICKNAME_KICK) {
                            try {
                                this.queryLib.kickClient(clientID, false, this.createMessage(clientInfo.get("client_nickname")));
                                this.modClass.addLogEntry(this.configPrefix, (byte)1, "Client \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + ") was kicked, nickname matched bad nickname rules!", false);
                                this.fel.clearException(Integer.parseInt(clientInfo.get("client_database_id")));
                            }
                            catch (TS3ServerQueryException sqe) {
                                if (this.fel.existsException(sqe, Integer.parseInt(clientInfo.get("client_database_id")))) {
                                    continue;
                                }
                                this.fel.addException(sqe, Integer.parseInt(clientInfo.get("client_database_id")));
                                this.modClass.addLogEntry(this.configPrefix, (byte)3, "Nickname of the client \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + ") match a bad nickname rule, but kick is not possible because of an error!", false);
                                this.modClass.addLogEntry(this.configPrefix, sqe, false);
                            }
                            catch (Exception e) {
                                this.modClass.addLogEntry(this.configPrefix, (byte)3, "Nickname of the client \"" + clientInfo.get("client_nickname") + "\" (db id: " + clientInfo.get("client_database_id") + ") match a bad nickname rule, but kick is not possible because of an error!", false);
                                this.modClass.addLogEntry(this.configPrefix, e, false);
                            }
                        }
                        else {
                            this.BADNICKNAME_CACHE.addElement(clientInfo.get("client_nickname"));
                            this.modClass.sendMessageToClient(this.configPrefix, this.BADNICKNAME_MESSAGE_MODE, clientID, this.createMessage(clientInfo.get("client_nickname")));
                        }
                    }
                }
            }
        }
        if (!this.BADNICKNAME_KICK) {
            for (int i = 0; i < this.BADNICKNAME_CACHE.size(); ++i) {
                boolean found = false;
                for (final HashMap<String, String> client : clientList) {
                    if (client.get("client_nickname").equals(this.BADNICKNAME_CACHE.elementAt(i))) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    this.BADNICKNAME_CACHE.removeElementAt(i);
                }
            }
        }
    }
    
    private String createMessage(final String clientName) {
        return this.BADNICKNAME_MESSAGE.replace("%CLIENT_NAME%", clientName);
    }
}
