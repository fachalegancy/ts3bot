package de.stefan1200.jts3servermod.functions;

import de.stefan1200.jts3servermod.interfaces.*;
import de.stefan1200.jts3serverquery.*;
import de.stefan1200.jts3servermod.*;
import java.io.*;
import de.stefan1200.util.*;
import java.sql.*;
import java.util.*;

public class AutoMove implements HandleBotEvents, HandleTS3Events, LoadConfiguration
{
    private String configPrefix;
    private JTS3ServerMod_Interface modClass;
    private JTS3ServerQuery queryLib;
    private boolean pluginEnabled;
    private HashMap<Integer, Integer> AUTOMOVE_SGCHANNEL_LIST;
    private String AUTOMOVE_MESSAGE_MODE;
    private String AUTOMOVE_MESSAGE;
    private String AUTOMOVE_FILE;
    
    public AutoMove() {
        this.configPrefix = "";
        this.modClass = null;
        this.queryLib = null;
        this.pluginEnabled = false;
        this.AUTOMOVE_SGCHANNEL_LIST = new HashMap<Integer, Integer>();
        this.AUTOMOVE_MESSAGE_MODE = null;
        this.AUTOMOVE_MESSAGE = null;
        this.AUTOMOVE_FILE = null;
    }
    
    public void initClass(final JTS3ServerMod_Interface modClass, final JTS3ServerQuery queryLib, final String prefix) {
        this.modClass = modClass;
        this.queryLib = queryLib;
        this.configPrefix = prefix.trim();
    }
    
    public void handleOnBotConnect() {
        if (!this.pluginEnabled) {
            return;
        }
        final String msg = "Auto Move is enabled, default channels for " + Integer.toString(this.AUTOMOVE_SGCHANNEL_LIST.size()) + " server groups set";
        this.modClass.addLogEntry(this.configPrefix, (byte)1, msg, true);
    }
    
    public void handleAfterCacheUpdate() {
    }
    
    public void activate() {
    }
    
    public void disable() {
    }
    
    public void unload() {
        this.AUTOMOVE_SGCHANNEL_LIST = null;
    }
    
    public boolean multipleInstances() {
        return false;
    }
    
    public void initConfig(final ArrangedPropertiesWriter config) {
        config.addKey(String.valueOf(this.configPrefix) + "_message_mode", "Select the message mode, how the clients should get the message.\npoke, chat or none are valid values!", "chat");
        if (this.modClass.getMySQLConnection() == null) {
            config.addKey(String.valueOf(this.configPrefix) + "_file", "Path to file which contains the auto move configuration and message.", "config/server1/automove.cfg");
        }
        if (this.modClass.getMySQLConnection() != null) {
            config.addKey(String.valueOf(this.configPrefix) + "_message", "Set the chat message for the auto move function.\nYou can use \\n for a new line and typical BBCode like in Teamspeak 3 Client.");
        }
    }
    
    public boolean loadConfig(final ArrangedPropertiesWriter config, final boolean slowMode) throws BotConfigurationException, NumberFormatException {
        this.pluginEnabled = false;
        if (slowMode) {
            this.modClass.addLogEntry(this.configPrefix, (byte)3, "Slow Mode activated, Auto Move disabled!", true);
            return false;
        }
        this.AUTOMOVE_MESSAGE_MODE = config.getValue(String.valueOf(this.configPrefix) + "_message_mode", "none").trim();
        this.AUTOMOVE_FILE = config.getValue(String.valueOf(this.configPrefix) + "_file");
        if (!this.loadAutoMoveFile(config)) {
            throw new BotConfigurationException("Auto Move message and configuration could not be loaded!");
        }
        if (this.AUTOMOVE_SGCHANNEL_LIST.size() == 0) {
            throw new BotConfigurationException("Auto Move needs at least one server group set!" + ((this.AUTOMOVE_FILE == null) ? "" : (" Config file: " + this.AUTOMOVE_FILE)));
        }
        if (this.AUTOMOVE_MESSAGE_MODE.equalsIgnoreCase("chat") || this.AUTOMOVE_MESSAGE_MODE.equalsIgnoreCase("poke")) {
            if (this.AUTOMOVE_MESSAGE == null || this.AUTOMOVE_MESSAGE.length() == 0) {
                throw new BotConfigurationException("Auto Move message could not be loaded!");
            }
            if (!this.modClass.isMessageLengthValid(this.AUTOMOVE_MESSAGE_MODE, this.AUTOMOVE_MESSAGE)) {
                this.modClass.addLogEntry(this.configPrefix, (byte)2, "Auto Move message is to long! Make sure that " + this.AUTOMOVE_MESSAGE_MODE + " messages are not longer than " + Short.toString(this.modClass.getMaxMessageLength(this.AUTOMOVE_MESSAGE_MODE)) + " characters (including spaces and BBCode)" + ((this.modClass.getMySQLConnection() == null) ? (", check file: " + this.AUTOMOVE_FILE) : ""), true);
            }
        }
        this.modClass.addTS3ServerEvent(this);
        return this.pluginEnabled = true;
    }
    
    private boolean loadAutoMoveFile(final ArrangedPropertiesWriter config) {
        if (this.modClass.getMySQLConnection() != null) {
            final MySQLConnect mysqlConnect = this.modClass.getMySQLConnection();
            this.AUTOMOVE_MESSAGE = config.getValue(String.valueOf(this.configPrefix) + "_message");
            boolean retValue = false;
            PreparedStatement pst = null;
            ResultSet rs = null;
            try {
                mysqlConnect.connect();
                pst = mysqlConnect.getPreparedStatement("SELECT servergroup_id, channel_id FROM jts3servermod_automove WHERE instance_id = ? AND prefix = ?");
                pst.setInt(1, this.modClass.getInstanceID());
                pst.setString(2, this.configPrefix);
                rs = pst.executeQuery();
                this.AUTOMOVE_SGCHANNEL_LIST.clear();
                while (rs.next()) {
                    this.AUTOMOVE_SGCHANNEL_LIST.put(rs.getInt(1), rs.getInt(2));
                }
                retValue = true;
            }
            catch (Exception e2) {
                retValue = false;
                return retValue;
            }
            finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                }
                catch (Exception ex) {}
                try {
                    if (pst != null) {
                        pst.close();
                    }
                }
                catch (Exception ex2) {}
                mysqlConnect.close();
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            }
            catch (Exception ex3) {}
            try {
                if (pst != null) {
                    pst.close();
                }
            }
            catch (Exception ex4) {}
            mysqlConnect.close();
            return retValue;
        }
        if (this.AUTOMOVE_FILE == null) {
            this.modClass.addLogEntry(this.configPrefix, (byte)3, "Path to Auto Move config file was not set in bot config! Check config key: " + this.configPrefix + "_file", true);
            return false;
        }
        this.AUTOMOVE_FILE = this.AUTOMOVE_FILE.trim();
        try {
            final BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.AUTOMOVE_FILE), this.modClass.getMessageEncoding()));
            String line = br.readLine();
            if (this.modClass.getMessageEncoding().equalsIgnoreCase("UTF-8") && line != null && line.charAt(0) == '\ufeff') {
                line = line.substring(1);
            }
            if (line == null || !line.equals("# JTS3ServerMod Config File")) {
                this.modClass.addLogEntry(this.configPrefix, (byte)3, "Special config file header is missing at Auto Move config file! File path: " + this.AUTOMOVE_FILE, true);
                this.modClass.addLogEntry(this.configPrefix, (byte)3, "Check if you set the right file at config key: " + this.configPrefix + "_file", true);
                br.close();
                return false;
            }
            this.AUTOMOVE_SGCHANNEL_LIST.clear();
            int count = 0;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("#")) {
                    continue;
                }
                if (line.length() < 3) {
                    continue;
                }
                if (count == 0) {
                    line = line.replace("\\n", "\n");
                    this.AUTOMOVE_MESSAGE = line;
                }
                if (count >= 1) {
                    final int pos = line.indexOf(",");
                    if (pos == -1) {
                        continue;
                    }
                    if (pos == 0) {
                        continue;
                    }
                    try {
                        this.AUTOMOVE_SGCHANNEL_LIST.put(Integer.parseInt(line.substring(0, pos).trim()), Integer.parseInt(line.substring(pos + 1).trim()));
                    }
                    catch (Exception e2) {
                        continue;
                    }
                }
                ++count;
            }
            br.close();
        }
        catch (FileNotFoundException fnfe) {
            this.modClass.addLogEntry(this.configPrefix, (byte)3, "Auto Move config file you set at config key \"" + this.configPrefix + "_file\" does not exist or missing permission for reading, check file path: " + this.AUTOMOVE_FILE, true);
            return false;
        }
        catch (Exception e) {
            this.modClass.addLogEntry(this.configPrefix, (byte)3, "Unknown error while loading Auto Move config file! Check file you set at config key \"" + this.configPrefix + "_file\", the file path: " + this.AUTOMOVE_FILE, true);
            this.modClass.addLogEntry(this.configPrefix, e, true);
            return false;
        }
        return true;
    }
    
    public void setListModes(final BitSet listOptions) {
    }
    
    public String[] botChatCommandList(final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        return null;
    }
    
    public String botChatCommandHelp(final String command) {
        return null;
    }
    
    public boolean handleChatCommands(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        return false;
    }
    
    public void handleClientEvents(final String eventType, final HashMap<String, String> eventInfo) {
        if (!this.pluginEnabled) {
            return;
        }
        if (eventType.equals("notifycliententerview") && Integer.parseInt(eventInfo.get("client_type")) == 0 && Integer.parseInt(eventInfo.get("ctid")) == this.modClass.getDefaultChannelID()) {
            final int clientID = Integer.parseInt(eventInfo.get("clid"));
            final int channelID = this.getTargetChannel(eventInfo.get("client_servergroups"), this.AUTOMOVE_SGCHANNEL_LIST);
            if (channelID > 0) {
                final String channelName = this.modClass.getChannelName(channelID);
                try {
                    this.queryLib.moveClient(clientID, channelID, null);
                    this.modClass.addLogEntry(this.configPrefix, (byte)1, "Client \"" + eventInfo.get("client_nickname") + "\" (db id: " + eventInfo.get("client_database_id") + ") has connected, moved to channel: " + ((channelName != null) ? (String.valueOf(channelName) + "(id: " + Integer.toString(channelID) + ")") : "Unknown"), false);
                    this.modClass.sendMessageToClient(this.configPrefix, this.AUTOMOVE_MESSAGE_MODE, clientID, this.AUTOMOVE_MESSAGE.replace("%CHANNEL_NAME%", (channelName != null) ? channelName : "Unknown"));
                }
                catch (Exception e) {
                    this.modClass.addLogEntry(this.configPrefix, (byte)1, "Client \"" + eventInfo.get("client_nickname") + "\" (db id: " + eventInfo.get("client_database_id") + ") has connected, but an error occurred while moving to channel: " + ((channelName != null) ? (String.valueOf(channelName) + "(id: " + Integer.toString(channelID) + ")") : "Unknown"), false);
                    this.modClass.addLogEntry(this.configPrefix, e, false);
                }
            }
        }
    }
    
    private int getTargetChannel(final String groupIDs, final HashMap<Integer, Integer> list) {
        final StringTokenizer groupTokenizer = new StringTokenizer(groupIDs, ",", false);
        Integer channelID = null;
        while (groupTokenizer.hasMoreTokens()) {
            final int groupID = Integer.parseInt(groupTokenizer.nextToken());
            channelID = list.get(groupID);
            if (channelID != null) {
                return channelID;
            }
        }
        return -1;
    }
}
