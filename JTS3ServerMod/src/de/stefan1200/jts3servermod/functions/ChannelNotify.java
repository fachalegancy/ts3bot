package de.stefan1200.jts3servermod.functions;

import de.stefan1200.jts3servermod.interfaces.*;
import de.stefan1200.jts3serverquery.*;
import de.stefan1200.util.*;
import de.stefan1200.jts3servermod.*;
import java.util.*;

public class ChannelNotify implements HandleBotEvents, HandleTS3Events, LoadConfiguration
{
    private String configPrefix;
    private JTS3ServerMod_Interface modClass;
    private boolean pluginEnabled;
    private Vector<Integer> CHANNELNOTIFY_GROUP_LIST;
    private boolean CHANNELNOTIFY_GROUP_LIST_IGNORE;
    private Vector<Integer> CHANNELNOTIFY_GROUPTARGETS;
    private String CHANNELNOTIFY_MESSAGE_MODE;
    private String CHANNELNOTIFY_MESSAGENOTIFIED_MODE;
    private String CHANNELNOTIFY_FILE;
    private String CHANNELNOTIFY_MESSAGE;
    private String CHANNELNOTIFY_MESSAGENOTIFIED;
    private String CHANNELNOTIFY_MESSAGENOTNOTIFIED;
    private int CHANNELNOTIFY_CHANNELID;
    private Vector<Integer> CHANNELNOTIFY_CHANNEL_LIST;
    private boolean CHANNELNOTIFY_CHANNEL_LIST_IGNORE;
    private String channelName;
    
    public ChannelNotify() {
        this.configPrefix = "";
        this.modClass = null;
        this.pluginEnabled = false;
        this.CHANNELNOTIFY_GROUP_LIST = new Vector<Integer>();
        this.CHANNELNOTIFY_GROUP_LIST_IGNORE = true;
        this.CHANNELNOTIFY_GROUPTARGETS = new Vector<Integer>();
        this.CHANNELNOTIFY_MESSAGE_MODE = null;
        this.CHANNELNOTIFY_MESSAGENOTIFIED_MODE = null;
        this.CHANNELNOTIFY_FILE = null;
        this.CHANNELNOTIFY_MESSAGE = null;
        this.CHANNELNOTIFY_MESSAGENOTIFIED = null;
        this.CHANNELNOTIFY_MESSAGENOTNOTIFIED = null;
        this.CHANNELNOTIFY_CHANNELID = -1;
        this.CHANNELNOTIFY_CHANNEL_LIST = new Vector<Integer>();
        this.CHANNELNOTIFY_CHANNEL_LIST_IGNORE = true;
        this.channelName = null;
    }
    
    public void initClass(final JTS3ServerMod_Interface modClass, final JTS3ServerQuery queryLib, final String prefix) {
        this.modClass = modClass;
        this.configPrefix = prefix.trim();
    }
    
    public void handleOnBotConnect() {
        if (!this.pluginEnabled) {
            return;
        }
        final StringBuffer groupTmp = new StringBuffer();
        for (final int groupID : this.CHANNELNOTIFY_GROUPTARGETS) {
            if (groupTmp.length() != 0) {
                groupTmp.append(", ");
            }
            groupTmp.append(groupID);
        }
        final String msg = "Watching for new clients in channel \"" + this.channelName + "\" (id: " + Integer.toString(this.CHANNELNOTIFY_CHANNELID) + "), sending message to all online clients of server group ids: " + groupTmp.toString();
        this.modClass.addLogEntry(this.configPrefix, (byte)1, msg, true);
    }
    
    public void handleAfterCacheUpdate() {
        if (!this.pluginEnabled) {
            return;
        }
        this.channelName = this.modClass.getChannelName(this.CHANNELNOTIFY_CHANNELID);
        if (this.channelName == null) {
            this.modClass.addLogEntry(this.configPrefix, (byte)3, "Critical: Channel ID " + this.CHANNELNOTIFY_CHANNELID + " don't exists! Check value " + this.configPrefix + "_channel_id in your configuration!", true);
            this.pluginEnabled = false;
        }
    }
    
    public void activate() {
    }
    
    public void disable() {
    }
    
    public void unload() {
        this.CHANNELNOTIFY_GROUPTARGETS = null;
        this.CHANNELNOTIFY_CHANNEL_LIST = null;
        this.CHANNELNOTIFY_GROUP_LIST = null;
    }
    
    public boolean multipleInstances() {
        return true;
    }
    
    public void initConfig(final ArrangedPropertiesWriter config) {
        config.addKey(String.valueOf(this.configPrefix) + "_channel_id", "Channel id that should be watched for new clients. You can only set one channel id here!");
        config.addKey(String.valueOf(this.configPrefix) + "_group_list", "A comma separated list (without spaces) of server group ids.\nDepends on the given mode, this server groups can be ignored or only this server groups will be watched!\nIf no server groups should be ignored, set no server groups here and select the group list mode ignore!");
        config.addKey(String.valueOf(this.configPrefix) + "_group_list_mode", "Select one of the two modes for the server group list.\nignore = The selected server groups will be ignored.\nonly = Send a notify message only if the selected server groups join the channel.", "ignore");
        config.addKey(String.valueOf(this.configPrefix) + "_grouptargets", "A comma separated list (without spaces) of server group ids, which should be notified about new clients in the specified channel.");
        config.addKey(String.valueOf(this.configPrefix) + "_channel_list", "A comma separated list (without spaces) of channel ids.\nDepends on the given mode, target clients in this channels can be ignored or only clients in this channels receive the notify message!\nIf no channels should be ignored, set no channels here and select the channel list mode ignore!");
        config.addKey(String.valueOf(this.configPrefix) + "_channel_list_mode", "Select one of the two modes for the channel list.\nignore = Clients in the selected channels will be ignored.\nonly = Only clients in the selected channels receive the notify message.", "ignore");
        config.addKey(String.valueOf(this.configPrefix) + "_message_mode", "Select the message mode, how the notified clients should get the message.\npoke or chat are valid values!", "poke");
        config.addKey(String.valueOf(this.configPrefix) + "_messagenotified_mode", "Select the message mode, how the clients (who joined the channel) should get the message.\npoke, chat or none are valid values!", "none");
        if (this.modClass.getMySQLConnection() == null) {
            config.addKey(String.valueOf(this.configPrefix) + "_file", "Path to file which contains the channel notify message", "config/server1/channelnotifymessages.cfg");
        }
        config.addKey(String.valueOf(this.configPrefix) + "_message", "Channel Notify message, specified clients get this message as chat or poke message.\nYou can use the following keywords, which will be replaced:\n%CLIENT_NAME% - Client Name\n%CLIENT_DBID% - Client Database ID\n%CLIENT_UNIQUEID% - Client Unique ID\n%CHANNEL_NAME% - Watched Channel Name\nTypical BBCode like in Teamspeak 3 Client possible. You can use \\n for a new line.", this.modClass.getMySQLConnection() != null);
        config.addKey(String.valueOf(this.configPrefix) + "_messagenotified", "Channel Notify message for the watched client.\nIf enabled, clients joining the watched channel get this message as chat or poke message.\nYou can use the following keywords, which will be replaced:\n%CLIENT_COUNT% - Number of target group clients who get informed about this client\n%CLIENT_NAMES% - List of names of target group clients who get informed about this client\n%CHANNEL_NAME% - Watched Channel Name\nTypical BBCode like in Teamspeak 3 Client possible. You can use \\n for a new line.", this.modClass.getMySQLConnection() != null);
        config.addKey(String.valueOf(this.configPrefix) + "_messagenotnotified", "Channel Notify message for the watched client, if no target group client is online.\nIf enabled, clients joining the watched channel get this message as chat or poke message.\nYou can use the following keywords, which will be replaced:\n%CHANNEL_NAME% - Watched Channel Name\nTypical BBCode like in Teamspeak 3 Client possible. You can use \\n for a new line.", this.modClass.getMySQLConnection() != null);
    }
    
    public boolean loadConfig(final ArrangedPropertiesWriter config, final boolean slowMode) throws BotConfigurationException, NumberFormatException {
        this.pluginEnabled = false;
        if (slowMode) {
            this.modClass.addLogEntry(this.configPrefix, (byte)3, "Slow Mode activated, Channel Notify disabled!", true);
            return false;
        }
        String lastNumberValue = "";
        String temp = null;
        try {
            this.CHANNELNOTIFY_FILE = config.getValue(String.valueOf(this.configPrefix) + "_file");
            this.CHANNELNOTIFY_MESSAGE_MODE = config.getValue(String.valueOf(this.configPrefix) + "_message_mode", "chat").trim();
            this.CHANNELNOTIFY_MESSAGENOTIFIED_MODE = config.getValue(String.valueOf(this.configPrefix) + "_messagenotified_mode", "chat").trim();
            final String[] configKeys = { String.valueOf(this.configPrefix) + "_message", String.valueOf(this.configPrefix) + "_messagenotified", String.valueOf(this.configPrefix) + "_messagenotnotified" };
            if (!this.modClass.loadMessages(this.configPrefix, "_file", configKeys)) {
                throw new BotConfigurationException("Channel Notify messages could not be loaded!");
            }
            this.CHANNELNOTIFY_MESSAGE = config.getValue(configKeys[0]);
            if (this.CHANNELNOTIFY_MESSAGE == null || this.CHANNELNOTIFY_MESSAGE.length() == 0) {
                throw new BotConfigurationException("Channel Notify message missing in config!");
            }
            if (this.CHANNELNOTIFY_MESSAGENOTIFIED_MODE.equalsIgnoreCase("poke") || this.CHANNELNOTIFY_MESSAGENOTIFIED_MODE.equalsIgnoreCase("chat")) {
                this.CHANNELNOTIFY_MESSAGENOTIFIED = config.getValue(configKeys[1]);
                if (this.CHANNELNOTIFY_MESSAGENOTIFIED == null || this.CHANNELNOTIFY_MESSAGENOTIFIED.length() == 0) {
                    throw new BotConfigurationException("Channel Notify message for the watched client missing in config!");
                }
                this.CHANNELNOTIFY_MESSAGENOTNOTIFIED = config.getValue(configKeys[2]);
                if (this.CHANNELNOTIFY_MESSAGENOTNOTIFIED == null || this.CHANNELNOTIFY_MESSAGENOTNOTIFIED.length() == 0) {
                    throw new BotConfigurationException("Channel Notify message for the watched client (if no target group client is online) missing in config!");
                }
            }
            lastNumberValue = String.valueOf(this.configPrefix) + "_channel_id";
            temp = config.getValue(String.valueOf(this.configPrefix) + "_channel_id");
            if (temp == null) {
                throw new NumberFormatException();
            }
            this.CHANNELNOTIFY_CHANNELID = Integer.parseInt(temp.trim());
            temp = null;
            this.CHANNELNOTIFY_GROUPTARGETS.clear();
            temp = config.getValue(String.valueOf(this.configPrefix) + "_grouptargets");
            lastNumberValue = String.valueOf(this.configPrefix) + "_grouptargets";
            if (temp != null && temp.length() > 0) {
                final StringTokenizer st = new StringTokenizer(temp, ",", false);
                while (st.hasMoreTokens()) {
                    this.CHANNELNOTIFY_GROUPTARGETS.addElement(Integer.parseInt(st.nextToken().trim()));
                }
            }
            temp = null;
            this.CHANNELNOTIFY_CHANNEL_LIST.clear();
            temp = config.getValue(String.valueOf(this.configPrefix) + "_channel_list");
            lastNumberValue = String.valueOf(this.configPrefix) + "_channel_list";
            if (temp != null && temp.length() > 0) {
                final StringTokenizer st = new StringTokenizer(temp, ",", false);
                while (st.hasMoreTokens()) {
                    this.CHANNELNOTIFY_CHANNEL_LIST.addElement(Integer.parseInt(st.nextToken().trim()));
                }
            }
            this.CHANNELNOTIFY_CHANNEL_LIST_IGNORE = !config.getValue(String.valueOf(this.configPrefix) + "_channel_list_mode", "ignore").trim().equalsIgnoreCase("only");
            temp = null;
            this.CHANNELNOTIFY_GROUP_LIST.clear();
            temp = config.getValue(String.valueOf(this.configPrefix) + "_group_list");
            lastNumberValue = String.valueOf(this.configPrefix) + "_group_list";
            if (temp != null && temp.length() > 0) {
                final StringTokenizer st = new StringTokenizer(temp, ",", false);
                while (st.hasMoreTokens()) {
                    this.CHANNELNOTIFY_GROUP_LIST.addElement(Integer.parseInt(st.nextToken().trim()));
                }
            }
            this.CHANNELNOTIFY_GROUP_LIST_IGNORE = !config.getValue(String.valueOf(this.configPrefix) + "_group_list_mode", "ignore").trim().equalsIgnoreCase("only");
            if (this.CHANNELNOTIFY_GROUPTARGETS.size() == 0) {
                throw new BotConfigurationException("No Channel Notify targets was defined! Please check configuration.");
            }
            this.modClass.addTS3ChannelEvent(this);
            this.pluginEnabled = true;
        }
        catch (NumberFormatException e) {
            final NumberFormatException nfe = new NumberFormatException("Config value of \"" + lastNumberValue + "\" is not a number! Current value: " + config.getValue(lastNumberValue, "not set"));
            nfe.setStackTrace(e.getStackTrace());
            throw nfe;
        }
        return this.pluginEnabled;
    }
    
    public void setListModes(final BitSet listOptions) {
        listOptions.set(1);
        listOptions.set(4);
    }
    
    public String[] botChatCommandList(final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        return null;
    }
    
    public String botChatCommandHelp(final String command) {
        return null;
    }
    
    public boolean handleChatCommands(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        return false;
    }
    
    public void handleClientEvents(final String eventType, final HashMap<String, String> eventInfo) {
        if (!this.pluginEnabled) {
            return;
        }
        if (eventType.equals("notifycliententerview") || eventType.equals("notifyclientmoved")) {
            if (Integer.parseInt(eventInfo.get("ctid")) != this.CHANNELNOTIFY_CHANNELID) {
                return;
            }
            final Vector<HashMap<String, String>> clientList = this.modClass.getClientList();
            if (clientList == null) {
                return;
            }
            HashMap<String, String> clientEvent = null;
            if (eventType.equals("notifycliententerview")) {
                clientEvent = eventInfo;
            }
            else if (eventType.equals("notifyclientmoved")) {
                clientEvent = new HashMap<String, String>();
                for (final HashMap<String, String> clientEventTemp : clientList) {
                    if (clientEventTemp.get("clid").equals(eventInfo.get("clid"))) {
                        clientEvent = clientEventTemp;
                        break;
                    }
                }
            }
            if (clientEvent != null && clientEvent.get("client_type").equals("0")) {
                final int groupID = this.modClass.getListedGroup(clientEvent.get("client_servergroups"), this.CHANNELNOTIFY_GROUPTARGETS);
                if (groupID == -1) {
                    boolean result = this.modClass.isGroupListed(clientEvent.get("client_servergroups"), this.CHANNELNOTIFY_GROUP_LIST);
                    if (this.CHANNELNOTIFY_GROUP_LIST_IGNORE) {
                        if (result) {
                            return;
                        }
                    }
                    else if (!result) {
                        return;
                    }
                    final Vector<String> targetClients = new Vector<String>();
                    final Vector<String> targetClients_UID = new Vector<String>();
                    String cnMessage = new String(this.CHANNELNOTIFY_MESSAGE);
                    cnMessage = cnMessage.replace("%CLIENT_NAME%", clientEvent.get("client_nickname"));
                    cnMessage = cnMessage.replace("%CLIENT_NAME_CLICKABLE%", "[URL=client://0/" + clientEvent.get("client_unique_identifier") + "]" + clientEvent.get("client_nickname") + "[/URL]");
                    cnMessage = cnMessage.replace("%CLIENT_DBID%", clientEvent.get("client_database_id"));
                    cnMessage = cnMessage.replace("%CLIENT_UNIQUEID%", clientEvent.get("client_unique_identifier"));
                    cnMessage = cnMessage.replace("%CHANNEL_NAME%", this.channelName);
                    cnMessage = cnMessage.replace("%CHANNEL_NAME_CLICKABLE%", "[URL=channelid://" + Integer.toString(this.CHANNELNOTIFY_CHANNELID) + "]" + this.channelName + "[/URL]");
                    if (!this.modClass.isMessageLengthValid(this.CHANNELNOTIFY_MESSAGE_MODE, cnMessage)) {
                        this.modClass.addLogEntry(this.configPrefix, (byte)2, "Channel Notify message is to long! Make sure that " + this.CHANNELNOTIFY_MESSAGE_MODE + " messages are not longer than " + Short.toString(this.modClass.getMaxMessageLength(this.CHANNELNOTIFY_MESSAGE_MODE)) + " characters (including spaces and BBCode)" + ((this.modClass.getMySQLConnection() == null) ? (", check file: " + this.CHANNELNOTIFY_FILE) : "!"), true);
                    }
                    int clientID = -1;
                    for (final HashMap<String, String> clientInfo : clientList) {
                        if (clientInfo.get("client_type").equals("0") && this.modClass.isGroupListed(clientInfo.get("client_servergroups"), this.CHANNELNOTIFY_GROUPTARGETS)) {
                            result = this.modClass.isIDListed(Integer.parseInt(clientInfo.get("cid")), this.CHANNELNOTIFY_CHANNEL_LIST);
                            if (this.CHANNELNOTIFY_CHANNEL_LIST_IGNORE) {
                                if (result) {
                                    continue;
                                }
                            }
                            else if (!result) {
                                continue;
                            }
                            clientID = Integer.parseInt(clientInfo.get("clid"));
                            if (!this.modClass.sendMessageToClient(this.configPrefix, this.CHANNELNOTIFY_MESSAGE_MODE, clientID, cnMessage)) {
                                continue;
                            }
                            targetClients.addElement(clientInfo.get("client_nickname"));
                            targetClients_UID.addElement(clientInfo.get("client_unique_identifier"));
                        }
                    }
                    if (this.CHANNELNOTIFY_MESSAGENOTIFIED_MODE.equalsIgnoreCase("poke") || this.CHANNELNOTIFY_MESSAGENOTIFIED_MODE.equalsIgnoreCase("chat")) {
                        clientID = Integer.parseInt(clientEvent.get("clid"));
                        if (targetClients.size() == 0) {
                            String cnMessageNotNotified = new String(this.CHANNELNOTIFY_MESSAGENOTNOTIFIED);
                            cnMessageNotNotified = cnMessageNotNotified.replace("%CHANNEL_NAME%", this.channelName);
                            if (!this.modClass.isMessageLengthValid(this.CHANNELNOTIFY_MESSAGENOTIFIED_MODE, cnMessageNotNotified)) {
                                this.modClass.addLogEntry(this.configPrefix, (byte)2, "Channel Notify message for the watched client (if no target group client is online) is to long! Make sure that " + this.CHANNELNOTIFY_MESSAGENOTIFIED_MODE + " messages are not longer than " + Short.toString(this.modClass.getMaxMessageLength(this.CHANNELNOTIFY_MESSAGENOTIFIED_MODE)) + " characters (including spaces and BBCode)" + ((this.modClass.getMySQLConnection() == null) ? (", check file: " + this.CHANNELNOTIFY_FILE) : "!"), true);
                            }
                            this.modClass.sendMessageToClient(this.configPrefix, this.CHANNELNOTIFY_MESSAGENOTIFIED_MODE, clientID, cnMessageNotNotified);
                        }
                        else {
                            final StringBuffer sb = new StringBuffer();
                            final StringBuffer sbURL = new StringBuffer();
                            for (int i = 0; i < targetClients.size(); ++i) {
                                if (i > 0) {
                                    sb.append(", ");
                                    sbURL.append(", ");
                                }
                                sb.append(targetClients.elementAt(i));
                                sbURL.append("[URL=client://0/" + targetClients_UID.elementAt(i) + "]" + targetClients.elementAt(i) + "[/URL]");
                            }
                            String cnMessageNotified = new String(this.CHANNELNOTIFY_MESSAGENOTIFIED);
                            cnMessageNotified = cnMessageNotified.replace("%CLIENT_COUNT%", Integer.toString(targetClients.size()));
                            cnMessageNotified = cnMessageNotified.replace("%CLIENT_NAMES%", sb.toString());
                            cnMessageNotified = cnMessageNotified.replace("%CLIENT_NAMES_CLICKABLE%", sbURL.toString());
                            cnMessageNotified = cnMessageNotified.replace("%CHANNEL_NAME%", this.channelName);
                            if (!this.modClass.isMessageLengthValid(this.CHANNELNOTIFY_MESSAGENOTIFIED_MODE, cnMessageNotified)) {
                                this.modClass.addLogEntry(this.configPrefix, (byte)2, "Channel Notify message for the watched client is to long! Make sure that " + this.CHANNELNOTIFY_MESSAGENOTIFIED_MODE + " messages are not longer than " + Short.toString(this.modClass.getMaxMessageLength(this.CHANNELNOTIFY_MESSAGENOTIFIED_MODE)) + " characters (including spaces and BBCode)" + ((this.modClass.getMySQLConnection() == null) ? (", check file: " + this.CHANNELNOTIFY_FILE) : "!"), true);
                            }
                            this.modClass.sendMessageToClient(this.configPrefix, this.CHANNELNOTIFY_MESSAGENOTIFIED_MODE, clientID, cnMessageNotified);
                        }
                    }
                }
            }
        }
    }
}
