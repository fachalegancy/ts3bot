package de.stefan1200.jts3servermod.functions;

import de.stefan1200.jts3serverquery.*;
import de.stefan1200.jts3servermod.interfaces.*;
import de.stefan1200.util.*;
import de.stefan1200.jts3servermod.*;
import java.util.*;

public class LastSeen implements HandleTS3Events, LoadConfiguration, HandleBotEvents
{
    private String configPrefix;
    private JTS3ServerMod_Interface modClass;
    private JTS3ServerQuery queryLib;
    private ClientDatabaseCache_Interface clientCache;
    private boolean pluginEnabled;
    static final byte COMMAND_LASTSEEN_ALL = 10;
    static final byte COMMAND_LASTSEEN_SERVERGROUPS = 5;
    static final byte COMMAND_LASTSEEN_BOTADMIN = 1;
    private byte COMMAND_LASTSEEN_MODE;
    private Vector<Integer> COMMAND_LASTSEEN_GROUP_LIST;
    private boolean COMMAND_LASTSEEN_GROUP_LIST_IGNORE;
    
    public LastSeen() {
        this.configPrefix = "";
        this.modClass = null;
        this.queryLib = null;
        this.clientCache = null;
        this.pluginEnabled = false;
        this.COMMAND_LASTSEEN_MODE = 0;
        this.COMMAND_LASTSEEN_GROUP_LIST = new Vector<Integer>();
        this.COMMAND_LASTSEEN_GROUP_LIST_IGNORE = true;
    }
    
    public void initClass(final JTS3ServerMod_Interface modClass, final JTS3ServerQuery queryLib, final String prefix) {
        this.modClass = modClass;
        this.queryLib = queryLib;
        this.configPrefix = prefix.trim();
    }
    
    public boolean multipleInstances() {
        return false;
    }
    
    public void handleOnBotConnect() {
        if (!this.pluginEnabled) {
            return;
        }
        this.clientCache = this.modClass.getClientCache();
        if (this.clientCache == null) {
            final String msg = "Client Database Cache in bot config is disabled, last seen command !" + this.configPrefix + " disabled!";
            this.modClass.addLogEntry(this.configPrefix, (byte)3, msg, true);
            this.modClass.unloadFunction(this);
        }
        else {
            String group = "everyone";
            if (this.COMMAND_LASTSEEN_MODE == 1) {
                group = "bot admins";
            }
            if (this.COMMAND_LASTSEEN_MODE == 5) {
                group = "specified server groups";
            }
            final String msg = "Check the last seen time of a client with the command !" + this.configPrefix + ", this can be used by " + group + "!";
            this.modClass.addLogEntry(this.configPrefix, (byte)1, msg, true);
        }
    }
    
    public void handleAfterCacheUpdate() {
    }
    
    public void activate() {
        if (this.clientCache == null) {
            this.clientCache = this.modClass.getClientCache();
        }
    }
    
    public void disable() {
    }
    
    public void unload() {
        this.COMMAND_LASTSEEN_GROUP_LIST = null;
    }
    
    public void initConfig(final ArrangedPropertiesWriter config) {
        config.addKey(String.valueOf(this.configPrefix) + "_user", "Who should be able to use the last seen command? Possible values: all, botadmin, servergroup\nThis command only works if the bot_clientdblist_cache is enabled!", "botadmin");
        config.addKey(String.valueOf(this.configPrefix) + "_group_list", "A comma separated list (without spaces) of server group ids.\nDepends on the given mode, this server groups can be ignored or only this server groups will be allowed to use the !lastseen command!\nThis is only needed, if user servergroup is selected!");
        config.addKey(String.valueOf(this.configPrefix) + "_group_list_mode", "Select one of the two modes for the server group list.\nignore = The selected server groups will be ignored.\nonly = Only the selected server groups are allowed to use the !lastseen command!", "ignore");
    }
    
    public boolean loadConfig(final ArrangedPropertiesWriter config, final boolean slowMode) throws BotConfigurationException, NumberFormatException {
        String lastNumberValue = "";
        String temp = null;
        this.pluginEnabled = false;
        try {
            temp = config.getValue(String.valueOf(this.configPrefix) + "_user", "botadmin").trim();
            if (temp.equals("all")) {
                this.COMMAND_LASTSEEN_MODE = 10;
            }
            else if (temp.equals("botadmin")) {
                this.COMMAND_LASTSEEN_MODE = 1;
            }
            else if (temp.equals("servergroup")) {
                this.COMMAND_LASTSEEN_MODE = 5;
                this.COMMAND_LASTSEEN_GROUP_LIST_IGNORE = !config.getValue(String.valueOf(this.configPrefix) + "_group_list_mode", "ignore").trim().equalsIgnoreCase("only");
                temp = null;
                this.COMMAND_LASTSEEN_GROUP_LIST.clear();
                temp = config.getValue(String.valueOf(this.configPrefix) + "_group_list");
                lastNumberValue = String.valueOf(this.configPrefix) + "_group_list";
                if (temp != null && temp.length() > 0) {
                    final StringTokenizer st = new StringTokenizer(temp, ",", false);
                    while (st.hasMoreTokens()) {
                        this.COMMAND_LASTSEEN_GROUP_LIST.addElement(Integer.parseInt(st.nextToken().trim()));
                    }
                }
            }
            this.pluginEnabled = true;
        }
        catch (NumberFormatException e) {
            final NumberFormatException nfe = new NumberFormatException("Config value of \"" + lastNumberValue + "\" is not a number! Current value: " + config.getValue(lastNumberValue, "not set"));
            nfe.setStackTrace(e.getStackTrace());
            throw nfe;
        }
        return this.pluginEnabled;
    }
    
    public String[] botChatCommandList(final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        if (!this.pluginEnabled) {
            return null;
        }
        if (isFullAdmin || isAdmin || this.isLastSeenAllowed(eventInfo.get("invokerid"))) {
            final String[] commands = { "<search string>" };
            return commands;
        }
        return null;
    }
    
    public String botChatCommandHelp(final String command) {
        return "Shows the last online time of a client. Use * as a wildcard.\nExample: !" + this.configPrefix + " *foo*bar*";
    }
    
    public boolean handleChatCommands(final String clientname, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        if (!this.pluginEnabled) {
            return false;
        }
        try {
            if (isFullAdmin || isAdmin || this.isLastSeenAllowed(eventInfo.get("invokerid"))) {
                if (this.clientCache == null) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Client database cache disabled, command disabled!");
                }
                else if (clientname.length() < 1) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !" + this.configPrefix + " <clientname>\nYou can use * as wildcard!");
                }
                else if (clientname.indexOf("**") != -1) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage, only single wildcards are allowed!");
                }
                else if (this.clientCache.isUpdateRunning()) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Client database cache is updating, please wait some time and try again!");
                }
                else {
                    final Vector<Integer> clientSearch = this.clientCache.searchClientNickname(clientname);
                    if (clientSearch == null) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage, use a valid search pattern with at least 3 characters!");
                    }
                    else if (clientSearch.size() == 0) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "No clients found in the database!");
                    }
                    else if (clientSearch.size() > 10) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Found " + Integer.toString(clientSearch.size()) + " entries in the database, please refine your search.");
                    }
                    else {
                        final StringBuffer sb = new StringBuffer("Found " + Integer.toString(clientSearch.size()) + " entries in the database:\n");
                        final Vector<HashMap<String, String>> clientList = this.modClass.getClientList();
                        boolean foundClient = false;
                        for (final int clientDBID : clientSearch) {
                            for (final HashMap<String, String> clientOnline : clientList) {
                                if (clientDBID == Integer.parseInt(clientOnline.get("client_database_id")) && Integer.parseInt(clientOnline.get("client_type")) == 0) {
                                    if (clientOnline.get("clid").equals(eventInfo.get("invokerid"))) {
                                        sb.append("[b]" + clientOnline.get("client_nickname") + "[/b] need a mirror :)\n");
                                    }
                                    else {
                                        sb.append("[b]" + clientOnline.get("client_nickname") + "[/b] is currently online\n");
                                    }
                                    foundClient = true;
                                    break;
                                }
                            }
                            if (!foundClient) {
                                try {
                                    final long lastOnline = this.clientCache.getLastOnline(clientDBID) * 1000L;
                                    sb.append("[b]" + this.clientCache.getNickname(clientDBID) + "[/b] was last seen at " + this.modClass.getStringFromTimestamp(lastOnline) + "\n");
                                }
                                catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            foundClient = false;
                        }
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, sb.toString());
                    }
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not allowed to use this command!");
            }
        }
        catch (Exception e2) {
            this.modClass.addLogEntry(this.configPrefix, e2, false);
        }
        return true;
    }
    
    public void handleClientEvents(final String eventType, final HashMap<String, String> eventInfo) {
    }
    
    public void setListModes(final BitSet listOptions) {
        listOptions.set(1);
    }
    
    private boolean isLastSeenAllowed(final String clientID) {
        if (this.COMMAND_LASTSEEN_MODE == 10) {
            return true;
        }
        if (this.COMMAND_LASTSEEN_MODE == 5) {
            final Vector<HashMap<String, String>> clientList = this.modClass.getClientList();
            if (this.COMMAND_LASTSEEN_MODE == 5 && clientList != null) {
                for (final HashMap<String, String> clientInfo : clientList) {
                    if (clientInfo.get("clid").equals(clientID)) {
                        final boolean result = this.modClass.isGroupListed(clientInfo.get("client_servergroups"), this.COMMAND_LASTSEEN_GROUP_LIST);
                        if (this.COMMAND_LASTSEEN_GROUP_LIST_IGNORE) {
                            if (result) {
                                return false;
                            }
                        }
                        else if (!result) {
                            return false;
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
