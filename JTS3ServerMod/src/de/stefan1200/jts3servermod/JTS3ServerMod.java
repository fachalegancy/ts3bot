package de.stefan1200.jts3servermod;

import java.net.*;
import java.util.regex.*;
import de.stefan1200.jts3servermod.interfaces.*;
import de.stefan1200.util.*;
import java.util.*;
import java.text.*;
import de.stefan1200.jts3serverquery.*;

import java.io.*;

public class JTS3ServerMod implements TeamspeakActionListener, JTS3ServerMod_Interface
{
    static final String[] LIST_OPTIONS;
    static final String[] ERROR_LEVEL_NAMES;
    static final byte ERROR_LEVEL_STATUS = 5;
    static final String VERSIONCHECK_URL = "http://www.stefan1200.de/versioncheck/JTS3ServerMod.version2";
    private int reloadState;
    private int reconnectTime;
    private String CONFIG_FILE_NAME;
    private String TS3_ADDRESS;
    private int TS3_QUERY_PORT;
    private String TS3_LOGIN;
    private String TS3_PASSWORD;
    private int TS3_VIRTUALSERVER_ID;
    private int TS3_VIRTUALSERVER_PORT;
    private int BOT_CHANNEL_ID;
    private boolean SLOW_MODE;
    private int CHECK_INTERVAL;
    private String MESSAGE_ENCODING;
    private String SERVER_QUERY_NAME;
    private String SERVER_QUERY_NAME_2;
    private boolean RECONNECT_FOREVER;
    private boolean CLIENT_DATABASE_CACHE;
    private boolean GLOBAL_MESSAGE_VARS;
    private Vector<String> FULL_ADMIN_UID_LIST;
    private Vector<String> ADMIN_UID_LIST;
    private String CSVLOGGER_FILE;
    private int DEFAULT_CHANNEL_ID;
    private String lastNumberValueAtConfigLoad;
    private String lastExceptionAtConfigLoad;
    private boolean updateCache;
    private boolean DEBUG;
    private byte botUpdateCheck;
    private SimpleDateFormat sdf;
    private SimpleDateFormat sdfDebug;
    private long startTime;
    private String instanceName;
    private String listArguments;
    private String lastActionString;
    private Vector<HashMap<String, String>> permissionListCache;
    private Vector<HashMap<String, String>> serverGroupListCache;
    private Vector<HashMap<String, String>> channelListCache;
    private Vector<HashMap<String, String>> clientList;
    private Vector<HandleBotEvents> classList_ts3EventServer;
    private Vector<HandleBotEvents> classList_ts3EventChannel;
    private Vector<String> functionList_Name;
    private Vector<String> functionList_Prefix;
    private Vector<Boolean> functionList_Enabled;
    private Vector<Boolean> functionList_Internal;
    private Vector<HandleBotEvents> functionList_Class;
    private ArrangedPropertiesWriter config;
    private JTS3ServerQuery queryLib;
    private InstanceManager manager;
    private ClientDatabaseCache clientCache;
    private ServerInfoCache serverInfoCache;
    private ChatCommands chatCommands;
    private PrintStream logFile;
    private PrintStream csvLogFile;
    private byte botLogLevel;
    private Timer botTimer;
    private TimerTask timerCheck;
    private TimerTask timerReconnect;
    private TimerTask timerUpdateCache;
    private BitSet listOptions;
    private Pattern patternFunctionPrefix;
    
    static {
        LIST_OPTIONS = new String[] { "-away", "-groups", "-info", "-times", "-uid", "-voice", "-country", "-ip" };
        ERROR_LEVEL_NAMES = new String[] { "DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL", "STATUS" };
    }
    
    public JTS3ServerMod(final String configFile) {
        this.reloadState = 2;
        this.reconnectTime = 65000;
        this.CONFIG_FILE_NAME = "server_bot.cfg";
        this.TS3_QUERY_PORT = 10011;
        this.SLOW_MODE = false;
        this.CLIENT_DATABASE_CACHE = false;
        this.GLOBAL_MESSAGE_VARS = false;
        this.FULL_ADMIN_UID_LIST = new Vector<String>();
        this.ADMIN_UID_LIST = new Vector<String>();
        this.DEFAULT_CHANNEL_ID = -1;
        this.lastNumberValueAtConfigLoad = null;
        this.lastExceptionAtConfigLoad = null;
        this.updateCache = false;
        this.DEBUG = false;
        this.botUpdateCheck = 0;
        this.sdfDebug = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.startTime = 0L;
        this.listArguments = null;
        this.lastActionString = "";
        this.permissionListCache = null;
        this.serverGroupListCache = null;
        this.channelListCache = null;
        this.clientList = null;
        this.classList_ts3EventServer = new Vector<HandleBotEvents>();
        this.classList_ts3EventChannel = new Vector<HandleBotEvents>();
        this.functionList_Name = new Vector<String>();
        this.functionList_Prefix = new Vector<String>();
        this.functionList_Enabled = new Vector<Boolean>();
        this.functionList_Internal = new Vector<Boolean>();
        this.functionList_Class = new Vector<HandleBotEvents>();
        this.config = null;
        this.clientCache = null;
        this.serverInfoCache = null;
        this.chatCommands = null;
        this.logFile = null;
        this.csvLogFile = null;
        this.botLogLevel = 1;
        this.listOptions = new BitSet(10);
        this.patternFunctionPrefix = Pattern.compile("[a-z0-9\\_\\-]+", 66);
        this.config = new ArrangedPropertiesWriter();
        this.initVars();
        this.CONFIG_FILE_NAME = configFile;
        this.initConfig();
        this.reloadState = 3;
    }
    
    public JTS3ServerMod(final ArrangedPropertiesWriter apw, final String configFile) {
        this.reloadState = 2;
        this.reconnectTime = 65000;
        this.CONFIG_FILE_NAME = "server_bot.cfg";
        this.TS3_QUERY_PORT = 10011;
        this.SLOW_MODE = false;
        this.CLIENT_DATABASE_CACHE = false;
        this.GLOBAL_MESSAGE_VARS = false;
        this.FULL_ADMIN_UID_LIST = new Vector<String>();
        this.ADMIN_UID_LIST = new Vector<String>();
        this.DEFAULT_CHANNEL_ID = -1;
        this.lastNumberValueAtConfigLoad = null;
        this.lastExceptionAtConfigLoad = null;
        this.updateCache = false;
        this.DEBUG = false;
        this.botUpdateCheck = 0;
        this.sdfDebug = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.startTime = 0L;
        this.listArguments = null;
        this.lastActionString = "";
        this.permissionListCache = null;
        this.serverGroupListCache = null;
        this.channelListCache = null;
        this.clientList = null;
        this.classList_ts3EventServer = new Vector<HandleBotEvents>();
        this.classList_ts3EventChannel = new Vector<HandleBotEvents>();
        this.functionList_Name = new Vector<String>();
        this.functionList_Prefix = new Vector<String>();
        this.functionList_Enabled = new Vector<Boolean>();
        this.functionList_Internal = new Vector<Boolean>();
        this.functionList_Class = new Vector<HandleBotEvents>();
        this.config = null;
        this.clientCache = null;
        this.serverInfoCache = null;
        this.chatCommands = null;
        this.logFile = null;
        this.csvLogFile = null;
        this.botLogLevel = 1;
        this.listOptions = new BitSet(10);
        this.patternFunctionPrefix = Pattern.compile("[a-z0-9\\_\\-]+", 66);
        this.initVars();
        this.config = apw;
        this.CONFIG_FILE_NAME = configFile;
        this.reloadState = 3;
    }
    
    public JTS3ServerMod(final InstanceManager manager, final String instanceName, final String configFile, final String logFilePath, final Vector<String> fullBotAdminList) {
        this.reloadState = 2;
        this.reconnectTime = 65000;
        this.CONFIG_FILE_NAME = "server_bot.cfg";
        this.TS3_QUERY_PORT = 10011;
        this.SLOW_MODE = false;
        this.CLIENT_DATABASE_CACHE = false;
        this.GLOBAL_MESSAGE_VARS = false;
        this.FULL_ADMIN_UID_LIST = new Vector<String>();
        this.ADMIN_UID_LIST = new Vector<String>();
        this.DEFAULT_CHANNEL_ID = -1;
        this.lastNumberValueAtConfigLoad = null;
        this.lastExceptionAtConfigLoad = null;
        this.updateCache = false;
        this.DEBUG = false;
        this.botUpdateCheck = 0;
        this.sdfDebug = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.startTime = 0L;
        this.listArguments = null;
        this.lastActionString = "";
        this.permissionListCache = null;
        this.serverGroupListCache = null;
        this.channelListCache = null;
        this.clientList = null;
        this.classList_ts3EventServer = new Vector<HandleBotEvents>();
        this.classList_ts3EventChannel = new Vector<HandleBotEvents>();
        this.functionList_Name = new Vector<String>();
        this.functionList_Prefix = new Vector<String>();
        this.functionList_Enabled = new Vector<Boolean>();
        this.functionList_Internal = new Vector<Boolean>();
        this.functionList_Class = new Vector<HandleBotEvents>();
        this.config = null;
        this.clientCache = null;
        this.serverInfoCache = null;
        this.chatCommands = null;
        this.logFile = null;
        this.csvLogFile = null;
        this.botLogLevel = 1;
        this.listOptions = new BitSet(10);
        this.patternFunctionPrefix = Pattern.compile("[a-z0-9\\_\\-]+", 66);
        this.CONFIG_FILE_NAME = configFile;
        this.manager = manager;
        this.instanceName = instanceName;
        this.FULL_ADMIN_UID_LIST = fullBotAdminList;
        this.config = new ArrangedPropertiesWriter();
        if (logFilePath != null) {
            String errorMessage = null;
            try {
                final File logFileCheck = new File(logFilePath);
                if (logFileCheck.exists()) {
                    final File oldLogFileCheck = new File(String.valueOf(logFilePath) + ".old");
                    if (oldLogFileCheck.exists()) {
                        if (oldLogFileCheck.delete()) {
                            if (!logFileCheck.renameTo(oldLogFileCheck)) {
                                errorMessage = "Unable to rename file " + logFilePath + " to " + logFilePath + ".old";
                            }
                        }
                        else {
                            errorMessage = "Unable to delete file " + logFilePath + ".old";
                        }
                    }
                    else if (!logFileCheck.renameTo(oldLogFileCheck)) {
                        errorMessage = "Unable to rename file " + logFilePath + " to " + logFilePath + ".old";
                    }
                }
            }
            catch (Exception e) {
                errorMessage = e.toString();
            }
            try {
                this.logFile = new PrintStream(new FileOutputStream(logFilePath, true), true, "UTF-8");
                if (errorMessage != null) {
                    this.addLogEntry((byte)3, "Error while checking old logfile: " + errorMessage, false);
                }
            }
            catch (Exception e) {
                this.logFile = null;
                System.out.println("Error while creating log file: " + logFilePath);
                e.printStackTrace();
            }
        }
        this.initConfig();
        this.queryLib = new JTS3ServerQuery(instanceName);
    }
    
    public JTS3ServerMod(final String instanceName, final String configFile) {
        this.reloadState = 2;
        this.reconnectTime = 65000;
        this.CONFIG_FILE_NAME = "server_bot.cfg";
        this.TS3_QUERY_PORT = 10011;
        this.SLOW_MODE = false;
        this.CLIENT_DATABASE_CACHE = false;
        this.GLOBAL_MESSAGE_VARS = false;
        this.FULL_ADMIN_UID_LIST = new Vector<String>();
        this.ADMIN_UID_LIST = new Vector<String>();
        this.DEFAULT_CHANNEL_ID = -1;
        this.lastNumberValueAtConfigLoad = null;
        this.lastExceptionAtConfigLoad = null;
        this.updateCache = false;
        this.DEBUG = false;
        this.botUpdateCheck = 0;
        this.sdfDebug = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.startTime = 0L;
        this.listArguments = null;
        this.lastActionString = "";
        this.permissionListCache = null;
        this.serverGroupListCache = null;
        this.channelListCache = null;
        this.clientList = null;
        this.classList_ts3EventServer = new Vector<HandleBotEvents>();
        this.classList_ts3EventChannel = new Vector<HandleBotEvents>();
        this.functionList_Name = new Vector<String>();
        this.functionList_Prefix = new Vector<String>();
        this.functionList_Enabled = new Vector<Boolean>();
        this.functionList_Internal = new Vector<Boolean>();
        this.functionList_Class = new Vector<HandleBotEvents>();
        this.config = null;
        this.clientCache = null;
        this.serverInfoCache = null;
        this.chatCommands = null;
        this.logFile = null;
        this.csvLogFile = null;
        this.botLogLevel = 1;
        this.listOptions = new BitSet(10);
        this.patternFunctionPrefix = Pattern.compile("[a-z0-9\\_\\-]+", 66);
        this.initVars();
        this.CONFIG_FILE_NAME = configFile;
        this.instanceName = instanceName;
        this.config = new ArrangedPropertiesWriter();
        this.initConfig();
        this.reloadState = 3;
        final int configOK = this.loadAndCheckConfig(true);
        if (configOK == 0) {
            if (this.config.save(this.CONFIG_FILE_NAME, "Config file of the JTS3ServerMod 5.4.2 (18.01.2015)\nhttp://www.stefan1200.de\nThis file must be saved with the encoding ISO-8859-1!")) {
                System.out.println(String.valueOf(instanceName) + ": Config OK and written updated file to disk!");
            }
            else {
                System.out.println(String.valueOf(instanceName) + ": Config OK, but an error occurred while writing to disk! Maybe file write protected?");
            }
        }
        else {
            final String errorMsg = this.getErrorMessage(configOK);
            System.out.println(String.valueOf(instanceName) + ": Config checked and found following errors:\n" + errorMsg + "\nNot written to disk!");
        }
    }
    
    static void showHelp() {
        System.out.println("JTS3ServerMod 5.4.2 (18.01.2015)");
        System.out.println("-help\t\tShows this help message");
        System.out.println("-version\tShows installed and latest version of JTS3ServerMod");
        System.out.println("-config <path>\tSet a different config file for the instance manager, default config/JTS3ServerMod_InstanceManager.cfg");
        System.out.println("-updateconfig\tCheck bot config file and write updated config file to disk");
        System.out.println("-log <path>\tSet a different path for the instance manager logfile, default JTS3ServerMod_InstanceManager.log");
    }
    
    public static void main(final String[] args) {
        String configFilePath = null;
        String logFilePath = null;
        if (args != null && args.length > 0) {
            for (int i = 0; i < args.length; ++i) {
                if (args[i].equalsIgnoreCase("-config") || args[i].equalsIgnoreCase("/config") || args[i].equalsIgnoreCase("--config")) {
                    if (args.length > i + 1) {
                        configFilePath = args[i + 1];
                        ++i;
                    }
                }
                else if (args[i].equalsIgnoreCase("-log") || args[i].equalsIgnoreCase("/log") || args[i].equalsIgnoreCase("--log")) {
                    if (args.length > i + 1) {
                        logFilePath = args[i + 1];
                        ++i;
                    }
                }
                else if (args[i].equalsIgnoreCase("-help") || args[i].equalsIgnoreCase("/help") || args[i].equalsIgnoreCase("--help")) {
                    showHelp();
                    System.exit(0);
                }
                else if (args[i].equalsIgnoreCase("-version") || args[i].equalsIgnoreCase("/version") || args[i].equalsIgnoreCase("--version")) {
                    showVersionCheck();
                    System.exit(0);
                }
                else if (args[i].equalsIgnoreCase("-versioncheck") || args[i].equalsIgnoreCase("/versioncheck") || args[i].equalsIgnoreCase("--versioncheck")) {
                    showVersionCheck();
                    System.exit(0);
                }
                else if (args[i].equalsIgnoreCase("-updateconfig") || args[i].equalsIgnoreCase("/updateconfig") || args[i].equalsIgnoreCase("--updateconfig")) {
                    new InstanceManager(configFilePath);
                    System.exit(0);
                }
            }
        }
        new InstanceManager(configFilePath, logFilePath);
    }
    
    static void showVersionCheck() {
        System.out.println("JTS3ServerMod");
        System.out.println("Current installed version:");
        System.out.println("5.4.2 (18.01.2015) [5402]");
        final HashMap<String, String> versionData = getVersionCheckData();
        if (versionData != null) {
            if (versionData.get("final.version") != null) {
                System.out.println("Latest final version:");
                System.out.println(String.valueOf(versionData.get("final.version")) + " [" + versionData.get("final.build") + "]");
            }
            if (versionData.get("dev.version") != null) {
                System.out.println("Latest development version:");
                System.out.println(String.valueOf(versionData.get("dev.version")) + " [" + versionData.get("dev.build") + "]");
            }
        }
    }
    
    static HashMap<String, String> getVersionCheckData() {
        final HashMap<String, String> versionData = new HashMap<String, String>();
        try {
            final URL versionCheckUrl = new URL("http://www.stefan1200.de/versioncheck/JTS3ServerMod.version2");
            final BufferedReader versionCheckStream = new BufferedReader(new InputStreamReader(versionCheckUrl.openStream()));
            String tmp;
            while ((tmp = versionCheckStream.readLine()) != null) {
                final StringTokenizer st = new StringTokenizer(tmp, ";", false);
                final String modeLine = st.nextToken();
                if (modeLine.equalsIgnoreCase("final")) {
                    versionData.put("final.build", st.nextToken());
                    versionData.put("final.version", st.nextToken());
                    versionData.put("final.url", st.nextToken());
                }
                if (modeLine.equalsIgnoreCase("development")) {
                    versionData.put("dev.build", st.nextToken());
                    versionData.put("dev.version", st.nextToken());
                    versionData.put("dev.url", st.nextToken());
                }
            }
            versionCheckStream.close();
        }
        catch (Exception ex) {}
        return versionData;
    }
    
    private String getPermissionName(final int permissionID) {
        if (this.permissionListCache == null) {
            return null;
        }
        final String sPermissionID = Integer.toString(permissionID);
        String retValue = null;
        for (final HashMap<String, String> permission : this.permissionListCache) {
            if (permission.get("permid").equals(sPermissionID)) {
                retValue = permission.get("permname");
                break;
            }
        }
        return retValue;
    }
    
    public String getMessageEncoding() {
        return this.MESSAGE_ENCODING;
    }
    
    public String getChannelName(final int channelID) {
        if (this.channelListCache == null) {
            return null;
        }
        final String sChannelID = Integer.toString(channelID);
        String retValue = null;
        for (final HashMap<String, String> channel : this.channelListCache) {
            if (channel.get("cid").equals(sChannelID)) {
                retValue = channel.get("channel_name");
                break;
            }
        }
        return retValue;
    }
    
    private void initConfig() {
        this.config.addKey("ts3_server_address", "Teamspeak 3 server address");
        this.config.addKey("ts3_server_query_port", "Teamspeak 3 server query port, default is 10011", "10011");
        this.config.addKey("ts3_server_query_login", "Teamspeak 3 server query admin account name");
        this.config.addKey("ts3_server_query_password", "Teamspeak 3 server query admin password");
        this.config.addKey("ts3_virtualserver_id", "Teamspeak 3 virtual server ID or -1 to use ts3_virtualserver_port");
        this.config.addKey("ts3_virtualserver_port", "Teamspeak 3 virtual server port, only needed if ts3_virtualserver_id is set to -1");
        this.config.addSeparator();
        this.config.addKey("bot_channel_id", "Channel id, the bot will join into it after connecting. If not wanted, use a negative number like -1.\nDon't set the default channel here, because the bot is already in the default channel after connecting.", "-1");
        this.config.addKey("bot_slowmode", "Activate the slow mode of the bot, 0 = disable, 1 = enable.\nIf slow mode is activated, the bot connects slower to the server\nand disables some bot features to reduce the amount of needed commands.\nThis feature may allow you to use the bot without whitelist the bot IP address.\nSlow mode disables the bad channel name check, channel notify, client auto move, client database cache,\nserver group notify, welcome message and do not allow the bot check interval to be lower than 3 seconds.", "0");
        this.config.addKey("bot_check_interval", "Check every X seconds, default is 1. Values between 1 and 30 are allowed.\nIf slow mode is activated, 3 is the lowest possible value.", "1");
        this.config.addKey("bot_messages_encoding", "A different encoding of the messages config files.\nDefault is UTF-8 which should be good for all EU and US languages.\nChange this only if you know what you are doing!\nFor English or German language you can also use the encoding ISO-8859-1\nA list of all valid ones: http://java.sun.com/j2se/1.5.0/docs/guide/intl/encoding.doc.html", "UTF-8");
        this.config.addKey("bot_clientdblist_cache", "This enables the client database list cache. This cache is needed for commands like !lastseen. 1 = Enable, 0 = Disable", "1");
        this.config.addKey("bot_global_message_vars", "This enables the global message variables. See readme file for a list of variables. 1 = Enable, 0 = Disable\nIf enabled, you can use all server variables in all messages. If not needed, disable this to save performance.", "0");
        this.config.addKey("bot_server_query_name", "Server Query name, this will be displayed as name of the connection.", "JTS3ServerMod");
        this.config.addKey("bot_server_query_name_2", "Second Server Query name, this will be displayed as name of the connection.\nThis name will be used, if the first name is already in use.", "MyJTS3ServerMod");
        this.config.addKey("bot_date_pattern", "Change the date pattern, which will be used to format a date in chat functions and welcome message.\nTo get help how to make such a pattern, look here: http://java.sun.com/j2se/1.5.0/docs/api/java/text/SimpleDateFormat.html", "yyyy-MM-dd HH:mm:ss");
        this.config.addKey("bot_connect_forever", "Should the bot try to connect forever if the Teamspeak server or the bot is offline? 0 = disable, 1 = enable", "0");
        this.config.addKey("bot_update_check", "Every time a bot full admin connects to the TS3 server it will be checked if an update for the JTS3ServerMod is available.\nIf an update is available, a chat message will be sent to the bot full admin.\n0 = disable, 1 = final versions, 2 = final and test versions", "0");
        this.config.addKey("bot_log_level", "Minimum log level, how much details you want to be written to the bot log files? Default is 1.\nHigher log levels will be also written, as an example: You set log level to 2, level 2, 3 and critical errors will be written to bot log file.\n0 = Debug\n1 = Information (recommended and default)\n2 = Warning (recommended for smaller log files)\n3 = Error (experts only)", "1");
        this.config.addKey("bot_admin_list", "A comma separated list (without spaces) of unique user ids, which should be able to use bot admin commands.\nThe unique user ids looks like this: mBbHRXwDAG7R19Rv3PorhMwbZW4=");
        this.config.addSeparator();
        this.config.addKey("bot_functions", "Set a comma separated list (without spaces) of needed bot functions here.\nEach function needs the function class and the function name, both separated with a colon.\nAll possible function classes are listed below, that class is case sensitive!\nThat function name you choose is important and has to be unique. It will be used as prefix for the configuration key names and chat commands.\nDon't use spaces in the function names, only use letters, numbers, minus and underscore!\nMost functions allow multiple usage, that allows you to set as many welcome messages or idle check rules, as you want.\nDon't forget that you have to put all settings of the functions in this file.\nHint: Start the bot with the argument -updateconfig after adding bot functions, that writes the configuration for all functions into this file!\nWhole command: java -jar JTS3ServerMod.jar -updateconfig\nNotice: This -updateconfig will also delete all lines of removed or renamed functions in this config file!\nFor more information about the functions read documents/ConfigHelp.html or documents/ConfigHelp_deutsch.html!\nExample: IdleCheck:idle,IdleCheck:idle_guest,MuteMover:mute,WelcomeMessage:welcome,WelcomeMessage:welcome_guest\nThis example gives you the following:\n- Two IdleCheck with the name idle and idle_guest\n- One MuteMover with the name mute\n- Two WelcomeMessage with the name welcome and welcome_guest\n\nFunction list (use only once!):\nAutoMove - Move connecting clients of a specified server group to a specified channel\nLastSeen - Chat command to check the last online time of a client (client database list cache must be enabled!)\n\nFunction list (multiple use possible):\nAdvertising - Send messages to channel or server chat every X minutes\nAwayMover - Move the client as soon as away status is set for longer than X seconds\nBadChannelNameCheck - Checking for bad channel names, can delete the channel and punish the client\nBadNicknameCheck - Checking for bad nicknames and can punish the client\nChannelNotify - Notify specified server groups about clients joining a specified channel\nIdleCheck - Move or kick an idle client, can also send an idle warning message\nInactiveChannelCheck - Delete channels if empty for more than X hours\nMuteMover - Move the client as soon as the specified mute status is set for longer than X seconds\nRecordCheck - Move or kick a recording client (of course only the record function of the Teamspeak client is detected)\nServerGroupNotify - Notify specified server groups about clients of specified server groups connecting to the TS3 server\nServerGroupProtection - Make sure that only specified clients are members of the specified server groups\nWelcomeMessage - Sends a message to new connected clients");
        this.config.addKey("bot_functions_disabled", "Set a comma separated list (without spaces) of needed but disabled bot functions here.\nSame format as bot_functions!\nAll functions you set here are not activated at bot start, but you can switch on functions using chat commands.");
        this.config.addSeparator();
        this.config.addSeparator();
    }
    
    private void initVars() {
        if (this.config != null) {
            this.config.removeAllValues();
        }
        if (this.manager != null) {
            this.DEBUG = this.manager.isDebugModeEnabled(this);
        }
        else {
            this.DEBUG = false;
        }
        if (this.queryLib != null) {
            this.queryLib.DEBUG_COMMLOG_PATH = "JTS3ServerQuery-communication" + ((this.instanceName == null) ? "" : ("_" + this.instanceName)) + ".log";
            this.queryLib.DEBUG_ERRLOG_PATH = "JTS3ServerQuery-error" + ((this.instanceName == null) ? "" : ("_" + this.instanceName)) + ".log";
            this.queryLib.DEBUG = this.DEBUG;
        }
        this.TS3_ADDRESS = null;
        this.TS3_QUERY_PORT = 10011;
        this.TS3_LOGIN = null;
        this.TS3_PASSWORD = null;
        this.TS3_VIRTUALSERVER_ID = -1;
        this.TS3_VIRTUALSERVER_PORT = -1;
        this.BOT_CHANNEL_ID = -1;
        this.botLogLevel = 1;
        this.SLOW_MODE = false;
        this.CHECK_INTERVAL = 1;
        this.MESSAGE_ENCODING = "UTF-8";
        this.SERVER_QUERY_NAME = null;
        this.SERVER_QUERY_NAME_2 = null;
        this.RECONNECT_FOREVER = false;
        this.ADMIN_UID_LIST = new Vector<String>();
        this.DEFAULT_CHANNEL_ID = -1;
        this.lastNumberValueAtConfigLoad = null;
        this.updateCache = false;
        this.botUpdateCheck = 0;
        this.permissionListCache = null;
        this.clientCache = null;
        this.serverInfoCache = null;
        this.serverGroupListCache = null;
        this.lastActionString = "";
        this.listArguments = null;
        this.listOptions.clear();
        this.botTimer = new Timer(true);
    }
    
    int loadAndCheckConfig(final boolean fromFile) {
        final int errorCode = this.loadConfig(fromFile);
        if (errorCode != 0) {
            return errorCode;
        }
        return 0;
    }
    
    String getErrorMessage(final int errorCode) {
        if (errorCode == 20) {
            if (this.lastNumberValueAtConfigLoad != null) {
                return "Critical: An expected number value in config is not a number or is completely missing! Look at value " + this.lastNumberValueAtConfigLoad + " in config file: " + this.CONFIG_FILE_NAME;
            }
            return "Critical: An expected number value in config is not a number or is completely missing! Config file: " + this.CONFIG_FILE_NAME;
        }
        else {
            if (errorCode == 23) {
                return "Critical: Teamspeak 3 server address, loginname or password missing in config file! Config file: " + this.CONFIG_FILE_NAME;
            }
            if (errorCode == 31) {
                return "Critical: Server query name in config file need at least 3 characters! Config file: " + this.CONFIG_FILE_NAME;
            }
            if (errorCode == 32) {
                return "Critical: Config file missing or is not readable, check: " + this.CONFIG_FILE_NAME;
            }
            if (errorCode == 40) {
                return "Critical: Unexpected error occurred in bot configuration! Config file: " + this.CONFIG_FILE_NAME + ((this.lastExceptionAtConfigLoad != null && this.logFile == null) ? ("\n" + this.lastExceptionAtConfigLoad) : "");
            }
            return "Unknown";
        }
    }
    
    private void loadFunctions() {
        int duplicateClassCount = 0;
        boolean loadBotClass = false;
        int configKeyCount = 0;
        for (int i = 0; i < this.functionList_Prefix.size(); ++i) {
            try {
                final Class<?> newFunction = Class.forName("de.stefan1200.jts3servermod.functions." + this.functionList_Name.elementAt(i));
                if (HandleBotEvents.class.isAssignableFrom(newFunction)) {
                    loadBotClass = false;
                    duplicateClassCount = Collections.frequency(this.functionList_Name, this.functionList_Name.elementAt(i));
                    final Object functionClass = (HandleBotEvents)newFunction.newInstance();
                    if (duplicateClassCount > 1) {
                        if (((HandleBotEvents)functionClass).multipleInstances()) {
                            loadBotClass = true;
                        }
                        else {
                            this.addLogEntry((byte)2, "Function class \"" + this.functionList_Name.elementAt(i) + "\" can only used once per virtual bot instance! Skipping all of this.", this.getMySQLConnection() == null);
                            this.removeDuplicateFunctions(this.functionList_Name.elementAt(i));
                            --i;
                        }
                    }
                    else {
                        loadBotClass = true;
                    }
                    if (loadBotClass) {
                        ((HandleBotEvents)functionClass).initClass(this, this.queryLib, this.functionList_Prefix.elementAt(i));
                        this.functionList_Class.setElementAt((HandleBotEvents)functionClass, i);
                        if (LoadConfiguration.class.isAssignableFrom(newFunction)) {
                            configKeyCount = this.config.getKeyCount();
                            ((LoadConfiguration)functionClass).initConfig(this.config);
                            if (configKeyCount < this.config.getKeyCount()) {
                                this.config.addSeparator();
                            }
                        }
                        this.addLogEntry((byte)1, "Successfully loaded function: " + this.functionList_Name.elementAt(i) + " / " + this.functionList_Prefix.elementAt(i), false);
                    }
                }
                else {
                    this.addLogEntry((byte)3, "Can't load function class \"" + this.functionList_Name.elementAt(i) + "\", it's not a valid bot function! Skipping all of this.", false);
                    this.removeDuplicateFunctions(this.functionList_Name.elementAt(i));
                    --i;
                }
            }
            catch (Exception e) {
                this.addLogEntry((byte)3, "Error loading function, skipping: " + this.functionList_Name.elementAt(i) + " / " + this.functionList_Prefix.elementAt(i), this.getMySQLConnection() == null);
                this.addLogEntry(e, false);
                this.functionList_Name.removeElementAt(i);
                this.functionList_Prefix.removeElementAt(i);
                this.functionList_Enabled.removeElementAt(i);
                this.functionList_Internal.removeElementAt(i);
                this.functionList_Class.removeElementAt(i);
                --i;
            }
        }
    }
    
    int[] reloadConfig() {
        final String adminListTemp = this.config.getValue("bot_admin_list");
        if (adminListTemp != null) {
            this.ADMIN_UID_LIST.clear();
            final StringTokenizer adminListTokenizer = new StringTokenizer(adminListTemp, ",", false);
            while (adminListTokenizer.hasMoreTokens()) {
                this.ADMIN_UID_LIST.addElement(adminListTokenizer.nextToken().trim());
            }
        }
        try {
            this.sdf = new SimpleDateFormat(this.config.getValue("bot_date_pattern", "yyyy-MM-dd HH:mm:ss"));
        }
        catch (Exception eSDF) {
            this.addLogEntry((byte)3, "Date pattern from config value of bot_date_pattern is invalid, using now the default date pattern!", this.getMySQLConnection() == null);
            this.addLogEntry(eSDF, false);
        }
        this.GLOBAL_MESSAGE_VARS = this.config.getValue("bot_global_message_vars", "0").equals("1");
        final int[] count = new int[2];
        for (int i = 0; i < this.functionList_Class.size(); ++i) {
            if (LoadConfiguration.class.isAssignableFrom(this.functionList_Class.elementAt(i).getClass())) {
                try {
                    if (((LoadConfiguration)this.functionList_Class.elementAt(i)).loadConfig(this.config, this.SLOW_MODE)) {
                        final int[] array = count;
                        final int n = 1;
                        ++array[n];
                    }
                    else {
                        final int[] array2 = count;
                        final int n2 = 0;
                        ++array2[n2];
                    }
                }
                catch (Exception e) {
                    final int[] array3 = count;
                    final int n3 = 0;
                    ++array3[n3];
                    this.addLogEntry(this.functionList_Prefix.elementAt(i), e, true);
                }
            }
        }
        return count;
    }
    
    int reloadConfig(final String prefix) {
        for (int i = 0; i < this.functionList_Prefix.size(); ++i) {
            if (this.functionList_Prefix.elementAt(i).equalsIgnoreCase(prefix)) {
                if (!LoadConfiguration.class.isAssignableFrom(this.functionList_Class.elementAt(i).getClass())) {
                    break;
                }
                try {
                    return ((LoadConfiguration)this.functionList_Class.elementAt(i)).loadConfig(this.config, this.SLOW_MODE) ? 1 : 0;
                }
                catch (Exception e) {
                    this.addLogEntry(e, true);
                    break;
                }
            }
        }
        return -1;
    }
    
    String[] getCurrentLoadedFunctions() {
        final StringBuffer sbTempEnabled = new StringBuffer();
        final StringBuffer sbTempDisabled = new StringBuffer();
        for (int i = 0; i < this.functionList_Prefix.size(); ++i) {
            if (this.functionList_Enabled.elementAt(i)) {
                if (sbTempEnabled.length() > 0) {
                    sbTempEnabled.append("\n");
                }
                sbTempEnabled.append(this.functionList_Prefix.elementAt(i));
                sbTempEnabled.append(" / ");
                sbTempEnabled.append(this.functionList_Name.elementAt(i));
            }
            else {
                if (sbTempDisabled.length() > 0) {
                    sbTempDisabled.append("\n");
                }
                sbTempDisabled.append(this.functionList_Prefix.elementAt(i));
                sbTempDisabled.append(" / ");
                sbTempDisabled.append(this.functionList_Name.elementAt(i));
            }
        }
        final String[] retValue = { sbTempEnabled.toString(), sbTempDisabled.toString() };
        return retValue;
    }
    
    byte activateFunction(final String prefix) {
        for (int i = 0; i < this.functionList_Prefix.size(); ++i) {
            if (this.functionList_Prefix.elementAt(i).equalsIgnoreCase(prefix)) {
                if (this.functionList_Enabled.elementAt(i)) {
                    return 0;
                }
                try {
                    this.functionList_Class.elementAt(i).activate();
                    this.functionList_Enabled.setElementAt(true, i);
                    this.createBotFunctionsConfig();
                    this.addLogEntry((byte)1, "Function activated: " + this.functionList_Name.elementAt(i) + " / " + this.functionList_Prefix.elementAt(i), false);
                    return 1;
                }
                catch (Exception e) {
                    this.addLogEntry(e, false);
                }
            }
        }
        return -1;
    }
    
    byte disableFunction(final String prefix) {
        for (int i = 0; i < this.functionList_Prefix.size(); ++i) {
            if (this.functionList_Prefix.elementAt(i).equalsIgnoreCase(prefix)) {
                if (!this.functionList_Enabled.elementAt(i)) {
                    return 0;
                }
                try {
                    this.functionList_Class.elementAt(i).disable();
                    this.functionList_Enabled.setElementAt(false, i);
                    this.createBotFunctionsConfig();
                    this.addLogEntry((byte)1, "Function disabled: " + this.functionList_Name.elementAt(i) + " / " + this.functionList_Prefix.elementAt(i), false);
                    return 1;
                }
                catch (Exception e) {
                    this.addLogEntry(e, false);
                }
            }
        }
        return -1;
    }
    
    void createBotFunctionsConfig() {
        final StringBuffer sbBotFunctions = new StringBuffer();
        final StringBuffer sbBotFunctionsDisabled = new StringBuffer();
        for (int i = 0; i < this.functionList_Prefix.size(); ++i) {
            if (this.functionList_Internal.elementAt(i)) {
                if (this.functionList_Enabled.elementAt(i)) {
                    if (sbBotFunctions.length() > 0) {
                        sbBotFunctions.append(",");
                    }
                    sbBotFunctions.append(this.functionList_Name.elementAt(i));
                    sbBotFunctions.append(":");
                    sbBotFunctions.append(this.functionList_Prefix.elementAt(i));
                }
                else {
                    if (sbBotFunctionsDisabled.length() > 0) {
                        sbBotFunctionsDisabled.append(",");
                    }
                    sbBotFunctionsDisabled.append(this.functionList_Name.elementAt(i));
                    sbBotFunctionsDisabled.append(":");
                    sbBotFunctionsDisabled.append(this.functionList_Prefix.elementAt(i));
                }
            }
        }
        this.config.setValue("bot_functions", sbBotFunctions.toString());
        this.config.setValue("bot_functions_disabled", sbBotFunctionsDisabled.toString());
    }
    
    String getCommandList(final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        final StringBuffer sbTemp = new StringBuffer();
        for (int i = 0; i < this.functionList_Class.size(); ++i) {
            if (this.functionList_Enabled.elementAt(i)) {
                if (HandleTS3Events.class.isAssignableFrom(this.functionList_Class.elementAt(i).getClass())) {
                    try {
                        final String sPrefix = this.functionList_Prefix.elementAt(i);
                        final String[] commands = ((HandleTS3Events)this.functionList_Class.elementAt(i)).botChatCommandList(eventInfo, isFullAdmin, isAdmin);
                        if (commands != null) {
                            String[] array;
                            for (int length = (array = commands).length, j = 0; j < length; ++j) {
                                final String command = array[j];
                                if (command.length() == 0) {
                                    sbTemp.append("\n!" + sPrefix);
                                }
                                else {
                                    sbTemp.append("\n!" + sPrefix + " " + command);
                                }
                            }
                        }
                    }
                    catch (Exception e) {
                        this.addLogEntry(e, true);
                    }
                }
            }
        }
        return sbTemp.toString();
    }
    
    String getCommandHelp(final String args) {
        String commandArgs = "";
        final int pos = args.indexOf(" ");
        String sPrefix;
        if (pos == -1) {
            sPrefix = args;
        }
        else {
            sPrefix = args.substring(0, pos);
            commandArgs = args.substring(pos + 1);
        }
        for (int i = 0; i < this.functionList_Prefix.size(); ++i) {
            if (this.functionList_Enabled.elementAt(i)) {
                if (this.functionList_Prefix.elementAt(i).equalsIgnoreCase(sPrefix) && HandleTS3Events.class.isAssignableFrom(this.functionList_Class.elementAt(i).getClass())) {
                    try {
                        return ((HandleTS3Events)this.functionList_Class.elementAt(i)).botChatCommandHelp(commandArgs);
                    }
                    catch (Exception e) {
                        this.addLogEntry(e, false);
                    }
                }
            }
        }
        return null;
    }
    
    private void removeDuplicateFunctions(final String className) {
        for (int i = 0; i < this.functionList_Prefix.size(); ++i) {
            if (this.functionList_Name.elementAt(i).equals(className)) {
                this.functionList_Name.removeElementAt(i);
                this.functionList_Prefix.removeElementAt(i);
                this.functionList_Enabled.removeElementAt(i);
                this.functionList_Internal.removeElementAt(i);
                this.functionList_Class.removeElementAt(i);
                --i;
            }
        }
    }
    
    private void unloadAllFunctions() {
        for (int i = 0; i < this.functionList_Class.size(); ++i) {
            if (this.functionList_Class.elementAt(i) != null) {
                this.functionList_Class.elementAt(0).disable();
            }
            if (this.functionList_Class.elementAt(i) != null) {
                this.functionList_Class.elementAt(0).unload();
            }
        }
        this.functionList_Name.clear();
        this.functionList_Prefix.clear();
        this.functionList_Enabled.clear();
        this.functionList_Internal.clear();
        this.functionList_Class.clear();
        this.classList_ts3EventChannel.clear();
        this.classList_ts3EventServer.clear();
        this.addLogEntry((byte)1, "Unloaded all functions!", false);
    }
    
    public void unloadFunction(final Object c) {
        final int pos = this.functionList_Class.indexOf(c);
        if (pos != -1) {
            this.addLogEntry((byte)1, "Successfully unloaded function: " + this.functionList_Name.elementAt(pos) + " / " + this.functionList_Prefix.elementAt(pos), false);
            ((HandleBotEvents)c).disable();
            ((HandleBotEvents)c).unload();
            this.functionList_Name.removeElementAt(pos);
            this.functionList_Prefix.removeElementAt(pos);
            this.functionList_Enabled.removeElementAt(pos);
            this.functionList_Internal.removeElementAt(pos);
            this.functionList_Class.removeElementAt(pos);
            this.classList_ts3EventChannel.removeElement(c);
            this.classList_ts3EventServer.removeElement(c);
        }
        else {
            this.addLogEntry((byte)2, "Unloading function not possible, function not found!", false);
        }
    }
    
    private String getFunctionPrefix(final Object c) {
        final int pos = this.functionList_Class.indexOf(c);
        if (pos != -1) {
            return this.functionList_Prefix.elementAt(pos);
        }
        return null;
    }
    
    boolean loadConfigValues() {
        return this.config.loadValues(this.CONFIG_FILE_NAME);
    }
    
    private int loadConfig(final boolean fromFile) {
        String lastNumberValue = null;
        if (fromFile && !this.config.loadValues(this.CONFIG_FILE_NAME)) {
            return 32;
        }
        try {
            if (this.DEBUG) {
                this.botLogLevel = 0;
            }
            else {
                lastNumberValue = "bot_log_level";
                this.botLogLevel = Byte.parseByte(this.config.getValue("bot_log_level", "1").trim());
                if (this.botLogLevel > 3) {
                    this.botLogLevel = 3;
                }
                this.addLogEntry((byte)5, "Activate log level: " + JTS3ServerMod.ERROR_LEVEL_NAMES[this.botLogLevel], false);
            }
            if (this.manager != null) {
                this.CSVLOGGER_FILE = this.manager.getConnectionLogPath(this);
                this.botUpdateCheck = this.manager.getBotUpdateCheckState();
            }
            else {
                this.csvLogFile = null;
                this.botUpdateCheck = 0;
            }
            String temp = this.config.getValue("bot_slowmode", "0");
            if (temp.equals("1")) {
                this.SLOW_MODE = true;
            }
            else {
                this.SLOW_MODE = false;
            }
            lastNumberValue = "bot_check_interval";
            this.CHECK_INTERVAL = Integer.parseInt(this.config.getValue("bot_check_interval", "1").trim());
            if (this.CHECK_INTERVAL > 30) {
                this.CHECK_INTERVAL = 30;
            }
            if (this.SLOW_MODE) {
                if (this.CHECK_INTERVAL < 3) {
                    this.CHECK_INTERVAL = 3;
                }
            }
            else if (this.CHECK_INTERVAL < 1) {
                this.CHECK_INTERVAL = 1;
            }
            this.SERVER_QUERY_NAME = this.config.getValue("bot_server_query_name", "JTS3ServerMod");
            this.SERVER_QUERY_NAME_2 = this.config.getValue("bot_server_query_name_2");
            this.MESSAGE_ENCODING = this.config.getValue("bot_messages_encoding", "UTF-8");
            if (this.SERVER_QUERY_NAME.length() < 3) {
                return 31;
            }
            temp = this.config.getValue("bot_clientdblist_cache", "0").trim();
            if (temp.equals("1")) {
                if (this.SLOW_MODE) {
                    this.CLIENT_DATABASE_CACHE = false;
                }
                else {
                    this.CLIENT_DATABASE_CACHE = true;
                }
            }
            else {
                this.CLIENT_DATABASE_CACHE = false;
            }
            temp = this.config.getValue("bot_connect_forever", "0").trim();
            if (temp.equals("1")) {
                this.RECONNECT_FOREVER = true;
            }
            else {
                this.RECONNECT_FOREVER = false;
            }
            this.TS3_ADDRESS = this.config.getValue("ts3_server_address");
            lastNumberValue = "ts3_server_query_port";
            temp = this.config.getValue("ts3_server_query_port");
            if (temp == null) {
                throw new NumberFormatException();
            }
            this.TS3_QUERY_PORT = Integer.parseInt(temp.trim());
            this.TS3_LOGIN = this.config.getValue("ts3_server_query_login");
            this.TS3_PASSWORD = this.config.getValue("ts3_server_query_password");
            lastNumberValue = "ts3_virtualserver_id";
            temp = this.config.getValue("ts3_virtualserver_id");
            if (temp == null) {
                throw new NumberFormatException();
            }
            this.TS3_VIRTUALSERVER_ID = Integer.parseInt(temp.trim());
            lastNumberValue = "bot_channel_id";
            this.BOT_CHANNEL_ID = Integer.parseInt(this.config.getValue("bot_channel_id", "-1").trim());
            if (this.TS3_ADDRESS.equalsIgnoreCase("ts3.server.net")) {
                this.addLogEntry((byte)3, " ", this.getMySQLConnection() == null);
                this.addLogEntry((byte)3, "**********************************************************************************", this.getMySQLConnection() == null);
                this.addLogEntry((byte)3, "It seems you never touched the config, check file " + this.CONFIG_FILE_NAME, this.getMySQLConnection() == null);
                this.addLogEntry((byte)3, "**********************************************************************************", this.getMySQLConnection() == null);
                this.addLogEntry((byte)3, " ", this.getMySQLConnection() == null);
            }
            if (this.TS3_VIRTUALSERVER_ID < 1) {
                lastNumberValue = "ts3_virtualserver_port";
                temp = this.config.getValue("ts3_virtualserver_port");
                if (temp == null) {
                    throw new NumberFormatException();
                }
                this.TS3_VIRTUALSERVER_PORT = Integer.parseInt(temp.trim());
            }
            if (this.TS3_ADDRESS == null || this.TS3_LOGIN == null || this.TS3_PASSWORD == null) {
                return 23;
            }
            temp = this.config.getValue("bot_functions", "").trim();
            if (temp != null && temp.length() > 0) {
                String sFunctionTemp = null;
                String[] functionTemp = null;
                final StringTokenizer st = new StringTokenizer(temp, ",", false);
                while (st.hasMoreTokens()) {
                    sFunctionTemp = st.nextToken().trim();
                    functionTemp = sFunctionTemp.split(":");
                    if (functionTemp.length == 2) {
                        if (this.functionList_Prefix.indexOf(functionTemp[1]) == -1) {
                            final Matcher ruleCheck = this.patternFunctionPrefix.matcher(functionTemp[1]);
                            if (ruleCheck.matches()) {
                                this.functionList_Name.addElement(functionTemp[0]);
                                this.functionList_Prefix.addElement(functionTemp[1]);
                                this.functionList_Enabled.addElement(true);
                                this.functionList_Internal.addElement(true);
                                this.functionList_Class.addElement(null);
                            }
                            else {
                                this.addLogEntry((byte)3, "Your chosen function name is not valid. Don't use spaces, only use letters, numbers, underscore and minus. Skipping this one: " + functionTemp[1], this.getMySQLConnection() == null);
                            }
                        }
                        else {
                            this.addLogEntry((byte)3, "Your chosen function name is not unique, skipping this one: " + functionTemp[1], this.getMySQLConnection() == null);
                        }
                    }
                    else {
                        this.addLogEntry((byte)3, "This wrong: \"" + sFunctionTemp + "\". Skipping this one. Right format is function class and function name separated with a colon!", this.getMySQLConnection() == null);
                    }
                }
            }
            temp = this.config.getValue("bot_functions_disabled", "").trim();
            if (temp != null && temp.length() > 0) {
                String sFunctionTemp = null;
                String[] functionTemp = null;
                final StringTokenizer st = new StringTokenizer(temp, ",", false);
                while (st.hasMoreTokens()) {
                    sFunctionTemp = st.nextToken().trim();
                    functionTemp = sFunctionTemp.split(":");
                    if (functionTemp.length == 2) {
                        if (this.functionList_Prefix.indexOf(functionTemp[1]) == -1) {
                            final Matcher ruleCheck = this.patternFunctionPrefix.matcher(functionTemp[1]);
                            if (ruleCheck.matches()) {
                                this.functionList_Name.addElement(functionTemp[0]);
                                this.functionList_Prefix.addElement(functionTemp[1]);
                                this.functionList_Enabled.addElement(false);
                                this.functionList_Internal.addElement(true);
                                this.functionList_Class.addElement(null);
                            }
                            else {
                                this.addLogEntry((byte)3, "Your chosen function name is not valid. Don't use spaces, only use letters, numbers, underscore and minus. Skipping this one: " + functionTemp[1], this.getMySQLConnection() == null);
                            }
                        }
                        else {
                            this.addLogEntry((byte)3, "Your chosen function name is not unique, skipping this one: " + functionTemp[1], this.getMySQLConnection() == null);
                        }
                    }
                    else {
                        this.addLogEntry((byte)3, "This wrong: \"" + sFunctionTemp + "\". Skipping this one. Right format is function class and function name separated with a colon!", this.getMySQLConnection() == null);
                    }
                }
            }
            this.loadFunctions();
            if (fromFile && !this.config.loadValues(this.CONFIG_FILE_NAME)) {
                return 32;
            }
            this.reloadConfig();
        }
        catch (NumberFormatException nfe) {
            this.addLogEntry(nfe, false);
            this.lastNumberValueAtConfigLoad = new String(lastNumberValue);
            return 20;
        }
        catch (Exception e1) {
            this.addLogEntry(e1, false);
            this.lastExceptionAtConfigLoad = getStackTrace(e1);
            return 40;
        }
        return 0;
    }
    
    public boolean loadMessages(final String configPrefix, final String configKey_Path, final String[] configValues) {
        String path = this.config.getValue(String.valueOf(configPrefix) + configKey_Path);
        if (path == null) {
            this.addLogEntry(configPrefix, (byte)3, "Path to messages was not set in bot config! Check config key: " + configPrefix + configKey_Path, this.getMySQLConnection() == null);
            return false;
        }
        path = path.trim();
        try {
            final BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), this.MESSAGE_ENCODING));
            String line = br.readLine();
            if (this.MESSAGE_ENCODING.equalsIgnoreCase("UTF-8") && line != null && line.charAt(0) == '\ufeff') {
                line = line.substring(1);
            }
            if (line == null || !line.equals("# JTS3ServerMod Config File")) {
                this.addLogEntry(configPrefix, (byte)3, "Special config file header is missing at the config file! File path: " + path, this.getMySQLConnection() == null);
                this.addLogEntry(configPrefix, (byte)3, "Check if you set the right file at config key: " + configPrefix + configKey_Path, this.getMySQLConnection() == null);
                br.close();
                return false;
            }
            int count = 0;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("#")) {
                    continue;
                }
                if (line.length() <= 3) {
                    continue;
                }
                line = line.replace("\\n", "\n");
                this.config.setValue(configValues[count], line);
                if (++count >= configValues.length) {
                    break;
                }
            }
            br.close();
        }
        catch (FileNotFoundException fnfe) {
            this.addLogEntry(configPrefix, (byte)3, "The file you set at config key \"" + configPrefix + configKey_Path + "\" does not exist or missing permission for reading, check file path: " + path, this.getMySQLConnection() == null);
            return false;
        }
        catch (Exception e) {
            this.addLogEntry(configPrefix, (byte)3, "Unknown error while loading messages, check file you set at config key \"" + configPrefix + configKey_Path + "\", the file path: " + path, this.getMySQLConnection() == null);
            this.addLogEntry(configPrefix, e, false);
            return false;
        }
        return true;
    }
    
    private void outputStartMSG() {
        if (this.CSVLOGGER_FILE != null) {
            final String msg = "Server connection log is activated and will be written into the " + (this.CSVLOGGER_FILE.equals("sql") ? "MySQL database" : ("file: " + this.CSVLOGGER_FILE));
            this.addLogEntry((byte)1, msg, true);
        }
        for (int i = 0; i < this.functionList_Class.size(); ++i) {
            if (this.functionList_Enabled.elementAt(i)) {
                if (HandleBotEvents.class.isAssignableFrom(this.functionList_Class.elementAt(i).getClass())) {
                    try {
                        this.functionList_Class.elementAt(i).handleOnBotConnect();
                        this.functionList_Class.elementAt(i).activate();
                    }
                    catch (Exception e) {
                        this.addLogEntry(e, true);
                    }
                }
            }
        }
    }
    
    private void setListOptions() {
        this.listOptions.clear();
        if (this.CSVLOGGER_FILE != null || this.clientCache != null) {
            this.listOptions.set(4);
            this.listOptions.set(7);
        }
        for (int i = 0; i < this.functionList_Class.size(); ++i) {
            if (LoadConfiguration.class.isAssignableFrom(this.functionList_Class.elementAt(i).getClass())) {
                try {
                    ((LoadConfiguration)this.functionList_Class.elementAt(i)).setListModes(this.listOptions);
                }
                catch (Exception e) {
                    this.addLogEntry(e, true);
                }
            }
        }
    }
    
    private void createListArguments() {
        this.listArguments = "";
        for (int i = this.listOptions.nextSetBit(0); i >= 0; i = this.listOptions.nextSetBit(i + 1)) {
            if (this.listArguments.equals("")) {
                this.listArguments = String.valueOf(this.listArguments) + JTS3ServerMod.LIST_OPTIONS[i];
            }
            else {
                this.listArguments = String.valueOf(this.listArguments) + "," + JTS3ServerMod.LIST_OPTIONS[i];
            }
        }
    }
    
    void runThread() {
        final Thread t = new Thread(new Runnable() {
            public void run() {
                do {
                    if (JTS3ServerMod.this.reloadState == 2) {
                        JTS3ServerMod.this.initVars();
                        JTS3ServerMod.this.runMod();
                    }
                    try {
                        Thread.sleep(100L);
                    }
                    catch (Exception ex) {}
                } while (JTS3ServerMod.this.reloadState != 0);
                JTS3ServerMod.this.addLogEntry((byte)5, "Virtual bot instance \"" + JTS3ServerMod.this.instanceName + "\" stopped", JTS3ServerMod.this.getMySQLConnection() == null);
            }
        });
        t.setName(this.instanceName);
        t.start();
    }
    
    private void runMod() {
        this.reloadState = 3;
        this.startTime = System.currentTimeMillis();
        this.addLogEntry((byte)5, "Virtual bot instance \"" + this.instanceName + "\" starts now" + (this.DEBUG ? " - Debug mode activated!" : ""), this.getMySQLConnection() == null);
        final int configOK = this.loadAndCheckConfig(true);
        this.reloadState = 1;
        if (configOK != 0) {
            this.addLogEntry((byte)4, this.getErrorMessage(configOK), this.getMySQLConnection() == null);
            this.stopBotInstance(0);
            return;
        }
        if (this.SLOW_MODE) {
            this.addLogEntry((byte)1, "Slow mode activated (using less commands at once)", this.getMySQLConnection() == null);
        }
        try {
            this.queryLib.connectTS3Query(this.TS3_ADDRESS, this.TS3_QUERY_PORT);
            this.addLogEntry((byte)1, "Successful connected to " + this.TS3_ADDRESS + "!", this.getMySQLConnection() == null);
        }
        catch (Exception e) {
            this.addLogEntry((byte)4, "Unable to connect to Teamspeak 3 server at " + this.TS3_ADDRESS + "!", this.getMySQLConnection() == null);
            this.addLogEntry(e, this.getMySQLConnection() == null);
            this.reconnectBot(false);
            return;
        }
        try {
            this.queryLib.loginTS3(this.TS3_LOGIN, this.TS3_PASSWORD);
            this.addLogEntry((byte)1, "Login as \"" + this.TS3_LOGIN + "\" successful!", this.getMySQLConnection() == null);
        }
        catch (Exception e) {
            this.addLogEntry((byte)4, "Unable to login as \"" + this.TS3_LOGIN + "\"! Maybe this IP address is banned for some minutes on that server!", this.getMySQLConnection() == null);
            this.addLogEntry(e, this.getMySQLConnection() == null);
            this.stopBotInstance(0);
            return;
        }
        try {
            this.permissionListCache = this.queryLib.getList(6);
        }
        catch (Exception e) {
            this.addLogEntry((byte)2, "Unable to receive permission list! If wanted, set permission b_serverinstance_permission_list to server group Guest Server Query.", this.getMySQLConnection() == null);
            this.addLogEntry(e, false);
        }
        if (this.SLOW_MODE) {
            try {
                Thread.sleep(3000L);
            }
            catch (Exception ex) {}
            if (this.botTimer == null) {
                return;
            }
        }
        Label_0737: {
            if (this.TS3_VIRTUALSERVER_ID >= 1) {
                try {
                    this.queryLib.selectVirtualServer(this.TS3_VIRTUALSERVER_ID, false);
                    this.addLogEntry((byte)1, "Successful selected virtual server " + Integer.toString(this.TS3_VIRTUALSERVER_ID) + "!", this.getMySQLConnection() == null);
                    break Label_0737;
                }
                catch (Exception e) {
                    this.addLogEntry((byte)4, "Unable to select virtual server " + Integer.toString(this.TS3_VIRTUALSERVER_ID) + "!", this.getMySQLConnection() == null);
                    this.addLogEntry(e, this.getMySQLConnection() == null);
                    this.reconnectBot(false);
                    return;
                }
            }
            try {
                this.queryLib.selectVirtualServer(this.TS3_VIRTUALSERVER_PORT, true);
                this.addLogEntry((byte)1, "Successful selected virtual server on port " + Integer.toString(this.TS3_VIRTUALSERVER_PORT) + "!", this.getMySQLConnection() == null);
            }
            catch (Exception e) {
                this.addLogEntry((byte)4, "Unable to select virtual server on port " + Integer.toString(this.TS3_VIRTUALSERVER_PORT) + "!", this.getMySQLConnection() == null);
                this.addLogEntry(e, this.getMySQLConnection() == null);
                this.reconnectBot(false);
                return;
            }
            try {
                this.queryLib.setDisplayName(this.SERVER_QUERY_NAME);
            }
            catch (Exception e) {
                this.addLogEntry((byte)2, "Unable to change name to \"" + this.SERVER_QUERY_NAME + "\"!", this.getMySQLConnection() == null);
                this.addLogEntry(e, this.getMySQLConnection() == null);
                if (this.SERVER_QUERY_NAME_2 != null && this.SERVER_QUERY_NAME_2.length() >= 3) {
                    this.addLogEntry((byte)1, "Try using \"" + this.SERVER_QUERY_NAME_2 + "\"...", this.getMySQLConnection() == null);
                    try {
                        this.queryLib.setDisplayName(this.SERVER_QUERY_NAME_2);
                    }
                    catch (Exception e2) {
                        this.addLogEntry((byte)2, "Unable to change name to \"" + this.SERVER_QUERY_NAME_2 + "\"!", this.getMySQLConnection() == null);
                        this.addLogEntry(e, this.getMySQLConnection() == null);
                    }
                }
            }
        }
        if (this.SLOW_MODE) {
            try {
                Thread.sleep(3000L);
            }
            catch (Exception ex2) {}
            if (this.botTimer == null) {
                return;
            }
        }
        if (this.BOT_CHANNEL_ID > 0) {
            if (this.queryLib.getCurrentQueryClientID() == -1) {
                this.reconnectBot(false);
            }
            try {
                this.queryLib.moveClient(this.queryLib.getCurrentQueryClientID(), this.BOT_CHANNEL_ID, null);
            }
            catch (Exception e) {
                this.addLogEntry((byte)3, "Unable to switch channel (own client ID: " + Integer.toString(this.queryLib.getCurrentQueryClientID()) + ")!", this.getMySQLConnection() == null);
                this.addLogEntry((byte)3, "Notice: If channel ID " + Integer.toString(this.BOT_CHANNEL_ID) + " is the default channel, use -1 in bot config!", this.getMySQLConnection() == null);
                this.addLogEntry(e, this.getMySQLConnection() == null);
            }
        }
        if (this.SLOW_MODE) {
            try {
                Thread.sleep(3000L);
            }
            catch (Exception ex3) {}
            if (this.botTimer == null) {
                return;
            }
        }
        this.serverInfoCache = new ServerInfoCache();
        this.queryLib.setTeamspeakActionListener(this);
        try {
            this.queryLib.addEventNotify(4, 0);
        }
        catch (Exception e) {
            final StringBuffer sbNameList = new StringBuffer();
            if (this.CSVLOGGER_FILE != null) {
                sbNameList.append("Connection Log");
            }
            if (this.botUpdateCheck == 1 || this.botUpdateCheck == 2) {
                sbNameList.append("Bot Update Check");
                this.botUpdateCheck = 0;
            }
            String tempName = null;
            while (this.classList_ts3EventServer.size() > 0) {
                tempName = this.getFunctionPrefix(this.classList_ts3EventServer.elementAt(0));
                if (tempName != null) {
                    if (sbNameList.length() > 0) {
                        sbNameList.append(", ");
                    }
                    sbNameList.append(tempName);
                }
                this.unloadFunction(this.classList_ts3EventServer.elementAt(0));
            }
            this.addLogEntry((byte)3, "Unable to see joining clients! Following functions disabled: " + sbNameList.toString(), this.getMySQLConnection() == null);
            this.addLogEntry(e, this.getMySQLConnection() == null);
            this.CSVLOGGER_FILE = null;
            if (this.csvLogFile != null) {
                this.csvLogFile.close();
                this.csvLogFile = null;
            }
        }
        try {
            this.queryLib.addEventNotify(5, 0);
        }
        catch (Exception e) {
            final StringBuffer sbNameList = new StringBuffer();
            String tempName = null;
            while (this.classList_ts3EventChannel.size() > 0) {
                tempName = this.getFunctionPrefix(this.classList_ts3EventChannel.elementAt(0));
                if (tempName != null) {
                    if (sbNameList.length() > 0) {
                        sbNameList.append(", ");
                    }
                    sbNameList.append(tempName);
                }
                this.unloadFunction(this.classList_ts3EventChannel.elementAt(0));
            }
            this.addLogEntry((byte)3, "Unable to receive channel join events! Following functions disabled: " + sbNameList.toString(), this.getMySQLConnection() == null);
            this.addLogEntry(e, this.getMySQLConnection() == null);
        }
        try {
            this.queryLib.addEventNotify(2, 0);
        }
        catch (Exception e) {
            this.addLogEntry((byte)3, "Unable to receive channel chat messages!", this.getMySQLConnection() == null);
            this.addLogEntry(e, this.getMySQLConnection() == null);
        }
        try {
            this.queryLib.addEventNotify(3, 0);
        }
        catch (Exception e) {
            this.addLogEntry((byte)3, "Unable to receive private chat messages!", this.getMySQLConnection() == null);
            this.addLogEntry(e, this.getMySQLConnection() == null);
        }
        if (this.SLOW_MODE) {
            try {
                Thread.sleep(3000L);
            }
            catch (Exception ex4) {}
            if (this.botTimer == null) {
                return;
            }
        }
        try {
            this.queryLib.addEventNotify(1, 0);
        }
        catch (Exception e) {
            this.addLogEntry((byte)3, "Unable to receive server chat messages!", this.getMySQLConnection() == null);
            this.addLogEntry(e, this.getMySQLConnection() == null);
        }
        if (!this.handleUpdateCache()) {
            this.reconnectBot(false);
            return;
        }
        if (this.SLOW_MODE) {
            try {
                Thread.sleep(3000L);
            }
            catch (Exception ex5) {}
            if (this.botTimer == null) {
                return;
            }
        }
        if (this.CLIENT_DATABASE_CACHE) {
            this.clientCache = new ClientDatabaseCache(this.queryLib, this);
        }
        this.outputStartMSG();
        this.setListOptions();
        this.createListArguments();
        this.chatCommands = new ChatCommands(this.queryLib, this, this.clientCache, this.sdf, this.config, this.manager);
        this.timerCheck = new TimerTask() {
            public void run() {
                JTS3ServerMod.this.runCheck();
            }
        };
        this.botTimer.schedule(this.timerCheck, this.CHECK_INTERVAL * 1000);
        this.timerUpdateCache = new TimerTask() {
            public void run() {
                JTS3ServerMod.access$5(JTS3ServerMod.this, true);
            }
        };
        this.botTimer.schedule(this.timerUpdateCache, 60000L, 60000L);
        this.addLogEntry((byte)1, "Bot started and connected successful, write !botinfo in server chat to get an answer!", this.getMySQLConnection() == null);
    }
    
    private void reconnectBot(final boolean force) {
        if (this.RECONNECT_FOREVER || force) {
            this.prepareStopBot();
            this.addLogEntry((byte)1, "Reconnecting in " + Integer.toString(this.reconnectTime / 1000) + " seconds...", this.getMySQLConnection() == null);
            this.timerReconnect = new TimerTask() {
                public void run() {
                    if (JTS3ServerMod.this.botTimer != null) {
                        JTS3ServerMod.this.botTimer.cancel();
                    }
                    JTS3ServerMod.access$7(JTS3ServerMod.this, null);
                    JTS3ServerMod.access$8(JTS3ServerMod.this, 2);
                }
            };
            this.botTimer.schedule(this.timerReconnect, this.reconnectTime);
            return;
        }
        this.stopBotInstance(0);
    }
    
    private void prepareStopBot() {
        this.unloadAllFunctions();
        if (this.timerCheck != null) {
            this.timerCheck.cancel();
        }
        this.timerCheck = null;
        if (this.clientCache != null) {
            this.clientCache.stopUpdating();
        }
        this.clientCache = null;
        if (this.timerUpdateCache != null) {
            this.timerUpdateCache.cancel();
        }
        this.timerUpdateCache = null;
        try {
            this.queryLib.removeTeamspeakActionListener();
        }
        catch (Exception ex) {}
        this.queryLib.closeTS3Connection();
    }
    
    void stopBotInstance(final int mode) {
        if (mode == 2) {
            this.addLogEntry((byte)5, "Virtual bot instance \"" + this.instanceName + "\" restarts", this.getMySQLConnection() == null);
        }
        this.prepareStopBot();
        if (this.timerReconnect != null) {
            this.timerReconnect.cancel();
        }
        this.timerReconnect = null;
        if (this.botTimer != null) {
            this.botTimer.cancel();
        }
        this.botTimer = null;
        if ((this.reloadState = mode) == 0) {
            if (this.csvLogFile != null) {
                this.csvLogFile.close();
                this.csvLogFile = null;
            }
            this.manager.removeInstance(this);
        }
    }
    
    private void checkTS3Clients() {
        if (!this.queryLib.isConnected()) {
            final String msg = "Connection to Teamspeak 3 server lost...";
            this.addLogEntry((byte)4, msg, true);
            this.reconnectBot(true);
            return;
        }
        try {
            this.clientList = this.queryLib.getList(1, this.listArguments);
        }
        catch (Exception e) {
            this.addLogEntry((byte)4, "Error while getting client list!", this.getMySQLConnection() == null);
            this.addLogEntry(e, true);
            this.reconnectBot(true);
            return;
        }
        if (this.serverInfoCache != null) {
            this.serverInfoCache.updateClientCount(this.clientList);
        }
        if (this.updateCache) {
            this.handleUpdateCache();
        }
        for (int i = 0; i < this.functionList_Class.size(); ++i) {
            if (this.functionList_Enabled.elementAt(i)) {
                if (HandleClientList.class.isAssignableFrom(this.functionList_Class.elementAt(i).getClass())) {
                    try {
                        ((HandleClientList)this.functionList_Class.elementAt(i)).handleClientCheck(this.clientList);
                    }
                    catch (Exception e2) {
                        this.addLogEntry(e2, true);
                    }
                }
            }
        }
    }
    
    private boolean handleUpdateCache() {
        this.updateCache = false;
        try {
            this.channelListCache = this.queryLib.getList(2, "-flags,-secondsempty");
        }
        catch (Exception e) {
            this.addLogEntry((byte)3, "Error while getting channel list!", this.getMySQLConnection() == null);
            this.addLogEntry(e, this.getMySQLConnection() == null);
            return false;
        }
        for (final HashMap<String, String> channel : this.channelListCache) {
            if (channel.get("channel_flag_default").equals("1")) {
                this.DEFAULT_CHANNEL_ID = Integer.parseInt(channel.get("cid"));
            }
        }
        try {
            this.serverGroupListCache = this.queryLib.getList(4);
        }
        catch (Exception e) {
            this.addLogEntry((byte)3, "Error while getting server group list!", this.getMySQLConnection() == null);
            this.addLogEntry(e, this.getMySQLConnection() == null);
            return false;
        }
        if (this.serverInfoCache != null) {
            try {
                this.serverInfoCache.updateServerInfo(this.queryLib.getInfo(11, -1));
            }
            catch (Exception e) {
                this.addLogEntry((byte)3, "Error while getting server info!", this.getMySQLConnection() == null);
                this.addLogEntry(e, this.getMySQLConnection() == null);
                return false;
            }
        }
        for (int i = 0; i < this.functionList_Class.size(); ++i) {
            if (this.functionList_Enabled.elementAt(i)) {
                if (HandleBotEvents.class.isAssignableFrom(this.functionList_Class.elementAt(i).getClass())) {
                    try {
                        this.functionList_Class.elementAt(i).handleAfterCacheUpdate();
                    }
                    catch (Exception e2) {
                        this.addLogEntry(e2, false);
                    }
                }
            }
        }
        return true;
    }
    
    public ServerInfoCache_Interface getServerInfoCache() {
        return this.serverInfoCache;
    }
    
    public Vector<HashMap<String, String>> getClientList() {
        return this.clientList;
    }
    
    public Vector<HashMap<String, String>> getChannelList() {
        return this.channelListCache;
    }
    
    public MySQLConnect getMySQLConnection() {
        return null;
    }
    
    public int getInstanceID() {
        return -1;
    }
    
    public int getDefaultChannelID() {
        return this.DEFAULT_CHANNEL_ID;
    }
    
    public ClientDatabaseCache_Interface getClientCache() {
        return this.clientCache;
    }
    
    public int getCheckInterval() {
        return this.CHECK_INTERVAL;
    }
    
    public String getStringFromTimestamp(final long timestamp) {
        return this.sdf.format(new Date(timestamp));
    }
    
    public void addTS3ChannelEvent(final Object obj) {
        if (obj != null) {
            this.classList_ts3EventChannel.addElement((HandleBotEvents)obj);
        }
    }
    
    public void addTS3ServerEvent(final Object obj) {
        if (obj != null) {
            this.classList_ts3EventServer.addElement((HandleBotEvents)obj);
        }
    }
    
    public void addBotTimer(final TimerTask task, final long firstStart, final long interval) {
        if (interval > 0L) {
            this.botTimer.schedule(task, firstStart, interval);
        }
        else {
            this.botTimer.schedule(task, firstStart);
        }
    }
    
    public boolean isGlobalMessageVarsEnabled() {
        return this.GLOBAL_MESSAGE_VARS;
    }
    
    public String replaceGlobalMessageVars(final String message) {
        String newMessage = new String(message);
        if (this.serverInfoCache != null) {
            if (this.serverInfoCache.getServerName() != null) {
                newMessage = newMessage.replace("%SERVER_NAME%", this.serverInfoCache.getServerName());
            }
            if (this.serverInfoCache.getServerPlatform() != null) {
                newMessage = newMessage.replace("%SERVER_PLATFORM%", this.serverInfoCache.getServerPlatform());
            }
            if (this.serverInfoCache.getServerVersion() != null) {
                newMessage = newMessage.replace("%SERVER_VERSION%", this.getVersionString(this.serverInfoCache.getServerVersion()));
            }
            newMessage = newMessage.replace("%SERVER_UPTIME%", this.getDifferenceTime(this.serverInfoCache.getServerUptimeTimestamp(), System.currentTimeMillis()));
            newMessage = newMessage.replace("%SERVER_CREATED_DATE%", this.getStringFromTimestamp(this.serverInfoCache.getServerCreatedAt()));
            newMessage = newMessage.replace("%SERVER_UPTIME_DATE%", this.getStringFromTimestamp(this.serverInfoCache.getServerUptimeTimestamp()));
            newMessage = newMessage.replace("%SERVER_UPLOAD_QUOTA%", Long.toString(this.serverInfoCache.getServerUploadQuota()));
            newMessage = newMessage.replace("%SERVER_DOWNLOAD_QUOTA%", Long.toString(this.serverInfoCache.getServerDownloadQuota()));
            newMessage = newMessage.replace("%SERVER_MONTH_BYTES_UPLOADED%", this.getFileSizeString(this.serverInfoCache.getServerMonthBytesUploaded(), false));
            newMessage = newMessage.replace("%SERVER_MONTH_BYTES_DOWNLOADED%", this.getFileSizeString(this.serverInfoCache.getServerMonthBytesDownloaded(), false));
            newMessage = newMessage.replace("%SERVER_TOTAL_BYTES_UPLOADED%", this.getFileSizeString(this.serverInfoCache.getServerTotalBytesUploaded(), false));
            newMessage = newMessage.replace("%SERVER_TOTAL_BYTES_DOWNLOADED%", this.getFileSizeString(this.serverInfoCache.getServerTotalBytesDownloaded(), false));
            newMessage = newMessage.replace("%SERVER_MAX_CLIENTS%", Integer.toString(this.serverInfoCache.getServerMaxClients()));
            newMessage = newMessage.replace("%SERVER_RESERVED_SLOTS%", Integer.toString(this.serverInfoCache.getServerReservedSlots()));
            newMessage = newMessage.replace("%SERVER_CHANNEL_COUNT%", Integer.toString(this.serverInfoCache.getServerChannelCount()));
            newMessage = newMessage.replace("%SERVER_CLIENT_COUNT%", Integer.toString(this.serverInfoCache.getServerClientCount()));
            newMessage = newMessage.replace("%SERVER_CLIENT_CONNECTIONS_COUNT%", Long.toString(this.serverInfoCache.getServerClientConnectionsCount()));
        }
        return newMessage;
    }
    
    public boolean sendMessageToClient(final String configPrefix, final String mode, final int clientID, String message) {
        if (mode == null) {
            return false;
        }
        if (message == null) {
            return false;
        }
        if (this.GLOBAL_MESSAGE_VARS) {
            message = this.replaceGlobalMessageVars(message);
        }
        boolean retValue = false;
        if (mode.equalsIgnoreCase("poke")) {
            try {
                this.queryLib.pokeClient(clientID, message);
                retValue = true;
            }
            catch (Exception e) {
                this.addLogEntry(configPrefix, (byte)3, "Error while poking Client ID: " + Integer.toString(clientID), false);
                this.addLogEntry(configPrefix, e, false);
            }
        }
        else if (mode.equalsIgnoreCase("chat")) {
            try {
                this.queryLib.sendTextMessage(clientID, 1, message);
                retValue = true;
            }
            catch (Exception e) {
                this.addLogEntry(configPrefix, (byte)3, "Error while sending chat message to Client ID: " + Integer.toString(clientID), false);
                this.addLogEntry(configPrefix, e, false);
            }
        }
        return retValue;
    }
    
    public short getMaxMessageLength(final String type) {
        if (type == null) {
            return 32767;
        }
        if (type.equalsIgnoreCase("chat")) {
            return 1023;
        }
        if (type.equalsIgnoreCase("poke")) {
            return 100;
        }
        if (type.equalsIgnoreCase("kick")) {
            return 80;
        }
        return 32767;
    }
    
    public boolean isMessageLengthValid(final String type, final String message) {
        return message.length() <= this.getMaxMessageLength(type);
    }
    
    public long getIdleTime(final HashMap<String, String> clientInfo, final int ignoreInChannel) {
        long currentIdleTime = Long.MAX_VALUE;
        try {
            currentIdleTime = Long.parseLong(clientInfo.get("client_idle_time"));
        }
        catch (NumberFormatException nfe) {
            if (!clientInfo.get("cid").equals(Integer.toString(ignoreInChannel))) {
                this.addLogEntry((byte)2, "TS3 Server sends wrong client_idle_time for client " + clientInfo.get("client_nickname") + " (id: " + clientInfo.get("clid") + ")", false);
            }
        }
        return currentIdleTime;
    }
    
    public boolean isIDListed(final int searchID, final Vector<Integer> list) {
        for (final int listID : list) {
            if (searchID == listID) {
                return true;
            }
        }
        return false;
    }
    
    public String getServerGroupName(final int groupID) {
        if (this.serverGroupListCache == null || groupID < 0) {
            return null;
        }
        for (final HashMap<String, String> serverGroupInfo : this.serverGroupListCache) {
            if (serverGroupInfo.get("sgid").equals(Integer.toString(groupID))) {
                return serverGroupInfo.get("name");
            }
        }
        return null;
    }
    
    public int getServerGroupType(final int groupID) {
        if (this.serverGroupListCache == null || groupID < 0) {
            return -1;
        }
        for (final HashMap<String, String> serverGroupInfo : this.serverGroupListCache) {
            if (serverGroupInfo.get("sgid").equals(Integer.toString(groupID))) {
                return Integer.parseInt(serverGroupInfo.get("type"));
            }
        }
        return -1;
    }
    
    public int getListedGroup(final String groupIDs, final Vector<Integer> list) {
        final StringTokenizer groupTokenizer = new StringTokenizer(groupIDs, ",", false);
        while (groupTokenizer.hasMoreTokens()) {
            final int groupID = Integer.parseInt(groupTokenizer.nextToken());
            for (final int gID : list) {
                if (groupID == gID) {
                    return groupID;
                }
            }
        }
        return -1;
    }
    
    public boolean isGroupListed(final String groupIDs, final Vector<Integer> list) {
        final StringTokenizer groupTokenizer = new StringTokenizer(groupIDs, ",", false);
        while (groupTokenizer.hasMoreTokens()) {
            final int groupID = Integer.parseInt(groupTokenizer.nextToken());
            for (final int gID : list) {
                if (groupID == gID) {
                    return true;
                }
            }
        }
        return false;
    }
    
    boolean isGroupListed(final String groupIDs, final int searchGroupID) {
        final StringTokenizer groupTokenizer = new StringTokenizer(groupIDs, ",", false);
        while (groupTokenizer.hasMoreTokens()) {
            final int groupID = Integer.parseInt(groupTokenizer.nextToken());
            if (groupID == searchGroupID) {
                return true;
            }
        }
        return false;
    }
    
    private void handleChatMessage(final HashMap<String, String> eventInfo) throws NumberFormatException, IOException, TS3ServerQueryException {
        if (Integer.parseInt(eventInfo.get("invokerid")) == this.queryLib.getCurrentQueryClientID()) {
            return;
        }
        String msg = eventInfo.get("msg").trim();
        msg = msg.replace(' ', ' ');
        if (!msg.startsWith("!")) {
            return;
        }
        boolean isFullAdmin = false;
        for (final String fulladminUID : this.FULL_ADMIN_UID_LIST) {
            if (fulladminUID.equals(eventInfo.get("invokeruid"))) {
                isFullAdmin = true;
                break;
            }
        }
        boolean isAdmin = false;
        if (!isFullAdmin) {
            for (final String adminUID : this.ADMIN_UID_LIST) {
                if (adminUID.equals(eventInfo.get("invokeruid"))) {
                    isAdmin = true;
                    break;
                }
            }
        }
        if (msg.toLowerCase().equalsIgnoreCase("!botquit") || msg.equalsIgnoreCase("!quit") || msg.equalsIgnoreCase("!exit")) {
            this.chatCommands.handleBotQuit(msg, eventInfo, this.instanceName, isFullAdmin);
        }
        else if (msg.toLowerCase().startsWith("!botinstancestop")) {
            this.chatCommands.handleBotInstanceStop(msg, eventInfo, isFullAdmin, this.instanceName);
        }
        else if (msg.toLowerCase().startsWith("!botinstancestart")) {
            this.chatCommands.handleBotInstanceStart(msg, eventInfo, isFullAdmin, this.instanceName);
        }
        else if (msg.equalsIgnoreCase("!botinstancelist")) {
            this.chatCommands.handleBotInstanceList(msg, eventInfo, isFullAdmin);
        }
        else if (msg.equalsIgnoreCase("!botinstancelistreload")) {
            this.chatCommands.handleBotInstanceListReload(msg, eventInfo, isFullAdmin);
        }
        else if (msg.equalsIgnoreCase("!botreload") || msg.equalsIgnoreCase("!reconnect")) {
            this.chatCommands.handleBotReload(msg, eventInfo, isFullAdmin, isAdmin, this.CONFIG_FILE_NAME);
        }
        else if (msg.equalsIgnoreCase("!botreloadall")) {
            this.chatCommands.handleBotReloadAll(msg, eventInfo, isFullAdmin);
        }
        else if (msg.equalsIgnoreCase("!botversion") || msg.equalsIgnoreCase("!botversioncheck") || msg.equalsIgnoreCase("!version")) {
            this.chatCommands.handleBotVersionCheck(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.equalsIgnoreCase("!status")) {
            this.chatCommands.handleStatus(msg, eventInfo, isFullAdmin, isAdmin);
        }else if (msg.equalsIgnoreCase("!sexo") || msg.equalsIgnoreCase("!botversioncheck") || msg.equalsIgnoreCase("!version")) {
    		this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Voce nao pode fazer sexo no TS...");
        }
        else if (msg.equalsIgnoreCase("!botfunctionlist") || msg.equalsIgnoreCase("!functionlist")) {
            this.chatCommands.handleBotFunctionList(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!botfunctionactivate") || msg.toLowerCase().startsWith("!functionon")) {
            this.chatCommands.handleBotFunctionActivate(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!botfunctiondisable") || msg.toLowerCase().startsWith("!functionoff")) {
            this.chatCommands.handleBotFunctionDisable(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!botcfgreload")) {
            this.chatCommands.handleBotCfgReload(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!botcfghelp")) {
            this.chatCommands.handleBotCfgHelp(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!botcfgget")) {
            this.chatCommands.handleBotCfgGet(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!botcfgset")) {
            this.chatCommands.handleBotCfgSet(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.equalsIgnoreCase("!botcfgcheck")) {
            this.chatCommands.handleBotCfgCheck(msg, eventInfo, isFullAdmin, isAdmin, this.CONFIG_FILE_NAME);
        }
        else if (msg.equalsIgnoreCase("!botcfgsave")) {
            this.chatCommands.handleBotCfgSave(msg, eventInfo, isFullAdmin, isAdmin, this.CONFIG_FILE_NAME);
        }
        else if (msg.toLowerCase().startsWith("!clientsearch") || msg.toLowerCase().startsWith("!clients") || msg.toLowerCase().startsWith("!clientlist")) {
            this.chatCommands.handleClientSearch(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!searchip")) {
            this.chatCommands.handleSearchIP(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!listinactiveclients") || msg.toLowerCase().startsWith("!inactiveclients")) {
            this.chatCommands.handleListInactiveClients(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!listinactivechannels") || msg.toLowerCase().startsWith("!emptychannels")) {
            this.chatCommands.handleListInactiveChannels(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!setchannelgroup")) {
            this.chatCommands.handleSetChannelGroup(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!removeservergroups")) {
            this.chatCommands.handleRemoveServerGroups(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!removechannelgroups")) {
            this.chatCommands.handleRemoveChannelGroups(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!msgchannelgroup")) {
            this.chatCommands.handleMsgChannelGroup(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!msgservergroup")) {
            this.chatCommands.handleMsgServerGroup(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!botjoinchannel") || msg.toLowerCase().startsWith("!joinchannel")) {
            this.chatCommands.handleBotJoinChannel(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!botrename")) {
            this.chatCommands.handleBotRename(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!setchannelname") || msg.toLowerCase().startsWith("!renamechannel")) {
            this.chatCommands.handleSetChannelName(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!getchannelid") || msg.toLowerCase().startsWith("!channellist")) {
            this.chatCommands.handleGetChannelID(msg, eventInfo, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!exec")) {
            this.chatCommands.handleExec(msg, eventInfo, isFullAdmin);
        }
        else if (msg.equalsIgnoreCase("!botinfo") || msg.equalsIgnoreCase("!info")) {
            this.chatCommands.handleBotInfo(msg, eventInfo, this.startTime, isFullAdmin, isAdmin);
        }
        else if (msg.toLowerCase().startsWith("!bothelp") || msg.toLowerCase().startsWith("!h")) {
            this.chatCommands.handleBotHelp(msg, eventInfo, isFullAdmin, isAdmin);
        }
        for (int i = 0; i < this.functionList_Class.size(); ++i) {
            if (this.functionList_Enabled.elementAt(i)) {
                if (HandleTS3Events.class.isAssignableFrom(this.functionList_Class.elementAt(i).getClass())) {
                    try {
                        final String sPrefix = "!" + this.functionList_Prefix.elementAt(i).toLowerCase();
                        if (msg.toLowerCase().startsWith(String.valueOf(sPrefix) + " ") || msg.toLowerCase().equals(sPrefix)) {
                            this.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
                            if (!((HandleTS3Events)this.functionList_Class.elementAt(i)).handleChatCommands((msg.length() > sPrefix.length()) ? msg.substring(sPrefix.length() + 1) : "", eventInfo, isFullAdmin, isAdmin)) {
                                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "No such command: [b]" + msg + "[/b]");
                                break;
                            }
                            break;
                        }
                    }
                    catch (Exception e) {
                        this.addLogEntry(e, true);
                    }
                }
            }
        }
    }
    
    private void handleCSVLog(final HashMap<String, String> eventInfo) {
        if (this.CSVLOGGER_FILE == null) {
            return;
        }
        try {
            if (this.csvLogFile == null) {
                this.csvLogFile = new PrintStream(new FileOutputStream(this.CSVLOGGER_FILE, true), false, "UTF-8");
            }
            this.csvLogFile.print(this.sdfDebug.format(new Date(System.currentTimeMillis())));
            this.csvLogFile.print(";");
            this.csvLogFile.print(eventInfo.get("client_unique_identifier"));
            this.csvLogFile.print(";");
            this.csvLogFile.print((eventInfo.get("connection_client_ip") != null) ? eventInfo.get("connection_client_ip") : "***");
            this.csvLogFile.print(";");
            this.csvLogFile.println(eventInfo.get("client_nickname"));
            this.csvLogFile.flush();
        }
        catch (Exception e) {
            this.addLogEntry(e, false);
        }
    }
    
    private void handleUpdateCheck(final HashMap<String, String> eventInfo) {
        if (this.botUpdateCheck != 1 && this.botUpdateCheck != 2) {
            return;
        }
        boolean isFullAdmin = false;
        for (final String fulladminUID : this.FULL_ADMIN_UID_LIST) {
            if (fulladminUID.equals(eventInfo.get("client_unique_identifier"))) {
                isFullAdmin = true;
                break;
            }
        }
        if (!isFullAdmin) {
            return;
        }
        final StringBuffer versionInfo = new StringBuffer();
        try {
            final HashMap<String, String> versionData = getVersionCheckData();
            if (versionData != null) {
                final long devBuild = (versionData.get("dev.build") == null) ? 0L : Long.parseLong(versionData.get("dev.build"));
                final long finalBuild = (versionData.get("final.build") == null) ? 0L : Long.parseLong(versionData.get("final.build"));
                if (5402L < finalBuild && versionData.get("final.version") != null && versionData.get("final.url") != null) {
                    versionInfo.append("\n[b]Latest final version:[/b] " + versionData.get("final.version") + " [" + versionData.get("final.build") + "]" + " - [url=" + versionData.get("final.url") + "]Download[/url]");
                }
                if (this.botUpdateCheck == 2 && 5402L < devBuild && versionData.get("dev.version") != null && versionData.get("dev.url") != null) {
                    versionInfo.append("\n[b]Latest development version:[/b] " + versionData.get("dev.version") + " [" + versionData.get("dev.build") + "]" + " - [url=" + versionData.get("dev.url") + "]Download[/url]");
                }
            }
        }
        catch (Exception e) {
            this.addLogEntry(e, false);
        }
        if (versionInfo.length() > 0) {
            try {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("clid")), 1, "New JTS3ServerMod version is available!\n[b]Current installed version:[/b] 5.4.2 (18.01.2015) [5402]" + versionInfo.toString());
            }
            catch (Exception e) {
                this.addLogEntry(e, false);
            }
        }
    }
    
    public String getVersionString(final String version) {
        final String searchString = " [Build: ";
        final int pos1 = version.indexOf(searchString);
        final int pos2 = version.indexOf("]", pos1 + searchString.length());
        try {
            final long lTime = Long.parseLong(version.substring(pos1 + searchString.length(), pos2)) * 1000L;
            return String.valueOf(version.substring(0, pos1)) + " (" + this.sdf.format(new Date(lTime)) + ")";
        }
        catch (Exception e) {
            return version;
        }
    }
    
    public String getFileSizeString(final long size, final boolean base1000) {
        final int base1001 = base1000 ? 1000 : 1024;
        final NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumFractionDigits(1);
        nf.setMaximumFractionDigits(2);
        String retValue;
        if (size > base1001 * base1001 * base1001) {
            final double value = size / base1001 / base1001 / base1001;
            retValue = String.valueOf(nf.format(value)) + " " + (base1000 ? "GB" : "GiB");
        }
        else if (size > base1001 * base1001) {
            final double value = size / base1001 / base1001;
            retValue = String.valueOf(nf.format(value)) + " " + (base1000 ? "MB" : "MiB");
        }
        else if (size > base1001) {
            final double value = size / base1001;
            retValue = String.valueOf(nf.format(value)) + " " + (base1000 ? "kB" : "KiB");
        }
        else {
            retValue = String.valueOf(size) + " byte";
        }
        return retValue;
    }
    
    public void teamspeakActionPerformed(final String eventType, final HashMap<String, String> eventInfo) {
        try {
            if (eventType.equals("notifytextmessage")) {
                this.handleChatMessage(eventInfo);
            }
            else {
                if ((String.valueOf(eventType) + eventInfo.toString()).equals(this.lastActionString)) {
                    return;
                }
                this.lastActionString = String.valueOf(eventType) + eventInfo.toString();
                if (eventType.equals("notifyclientleftview") && this.clientCache != null) {
                    final Vector<HashMap<String, String>> clientListCache = this.clientList;
                    for (final HashMap<String, String> clientInfo : clientListCache) {
                        if (clientInfo.get("clid").equals(eventInfo.get("clid")) && Integer.parseInt(clientInfo.get("client_type")) == 0) {
                            this.clientCache.updateSingleClient(clientInfo);
                            break;
                        }
                    }
                }
                if (eventType.equals("notifycliententerview") && Integer.parseInt(eventInfo.get("client_type")) == 0) {
                    final HashMap<String, String> clientInfo2 = this.queryLib.getInfo(13, Integer.parseInt(eventInfo.get("clid")));
                    if (clientInfo2 != null) {
                        eventInfo.put("connection_client_ip", clientInfo2.get("connection_client_ip"));
                    }
                    this.handleCSVLog(eventInfo);
                    if (this.clientCache != null) {
                        this.clientCache.updateSingleClient(eventInfo);
                    }
                    this.handleUpdateCheck(eventInfo);
                }
                for (int i = 0; i < this.functionList_Class.size(); ++i) {
                    if (this.functionList_Enabled.elementAt(i)) {
                        if (HandleTS3Events.class.isAssignableFrom(this.functionList_Class.elementAt(i).getClass())) {
                            try {
                                ((HandleTS3Events)this.functionList_Class.elementAt(i)).handleClientEvents(eventType, eventInfo);
                            }
                            catch (Exception e) {
                                this.addLogEntry(e, true);
                            }
                        }
                    }
                }
            }
        }
        catch (Throwable e2) {
            this.addLogEntry(e2, false);
        }
    }
    
    private void runCheck() {
        try {
            this.checkTS3Clients();
        }
        catch (NullPointerException e0) {
            this.addLogEntry((byte)2, "An error occurred, make sure that you use at least TS3 server version beta 19!", true);
            this.addLogEntry(e0, false);
        }
        catch (OutOfMemoryError ome) {
            this.addLogEntry((byte)4, "Out of Memory Error occurred!", true);
            this.addLogEntry((byte)4, "Check if you set enough RAM for the java virtual machine (-mx argument)!", true);
            this.addLogEntry(ome, false);
            this.manager.stopAllInstances("CRITICAL", "Error occurred, check logfile of the virtual bot instance " + this.instanceName);
            return;
        }
        catch (VirtualMachineError vme) {
            this.addLogEntry((byte)4, "Virtual Machine Error occurred!", true);
            this.addLogEntry(vme, false);
            this.manager.stopAllInstances("CRITICAL", "Error occurred, check logfile of the virtual bot instance " + this.instanceName);
            return;
        }
        catch (Throwable ex) {
            this.addLogEntry(ex, false);
        }
        finally {
            if (this.timerCheck != null) {
                this.timerCheck = new TimerTask() {
                    public void run() {
                        JTS3ServerMod.this.runCheck();
                    }
                };
                this.botTimer.schedule(this.timerCheck, this.CHECK_INTERVAL * 1000);
            }
        }
        if (this.timerCheck != null) {
            this.timerCheck = new TimerTask() {
                public void run() {
                    JTS3ServerMod.this.runCheck();
                }
            };
            this.botTimer.schedule(this.timerCheck, this.CHECK_INTERVAL * 1000);
        }
    }
    
    public String getDifferenceTime(final long from, final long to) {
        final long difference = to - from;
        final int days = (int)(difference / 86400000L);
        final int hours = (int)(difference / 3600000L % 24L);
        final int minutes = (int)(difference / 60000L % 60L);
        final int seconds = (int)(difference / 1000L % 60L);
        final NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumIntegerDigits(2);
        nf.setMaximumIntegerDigits(2);
        final StringBuffer timeString = new StringBuffer();
        if (days > 0) {
            timeString.append(days);
            timeString.append(" days and ");
        }
        if (days > 0 || hours > 0) {
            timeString.append(hours);
            timeString.append(":");
            timeString.append(nf.format(minutes));
            timeString.append(":");
            timeString.append(nf.format(seconds));
            timeString.append(" hours");
        }
        else if (minutes > 0) {
            timeString.append(minutes);
            timeString.append(":");
            timeString.append(nf.format(seconds));
            timeString.append(" minutes");
        }
        else {
            timeString.append(seconds);
            timeString.append(" seconds");
        }
        return timeString.toString();
    }
    
    void addLogEntry(final byte type, final String msg, final boolean outputToSystemOut) {
        this.addLogEntry(null, type, msg, outputToSystemOut);
    }
    
    public void addLogEntry(final String functionName, final byte type, final String msg, final boolean outputToSystemOut) {
        try {
            if (outputToSystemOut || this.DEBUG) {
                System.out.println(String.valueOf(this.instanceName) + ((functionName == null) ? "" : (" / " + functionName)) + ": " + msg);
            }
            if (type < this.botLogLevel) {
                return;
            }
            if (this.logFile != null) {
                this.logFile.println(String.valueOf(this.sdfDebug.format(new Date(System.currentTimeMillis()))) + "\t" + ((functionName == null) ? "JTS3ServerMod" : ("Function " + functionName)) + "\t" + JTS3ServerMod.ERROR_LEVEL_NAMES[type] + "\t" + msg);
            }
        }
        catch (Exception ex) {}
    }
    
    void addLogEntry(final Throwable e, final boolean outputToSystemOut) {
        this.addLogEntry(null, e, outputToSystemOut);
    }
    
    public void addLogEntry(final String functionName, final Throwable e, final boolean outputToSystemOut) {
        try {
            if (outputToSystemOut || this.DEBUG) {
                System.out.println(String.valueOf(this.instanceName) + ((functionName == null) ? "" : (" / " + functionName)) + ": " + e.toString());
            }
            if (this.logFile != null) {
                this.logFile.println(String.valueOf(this.sdfDebug.format(new Date(System.currentTimeMillis()))) + "\t" + ((functionName == null) ? "JTS3ServerMod" : ("Function " + functionName)) + "\t" + "EXCEPTION" + "\t" + "Bot Version: " + "5.4.2 (18.01.2015)");
                e.printStackTrace(this.logFile);
            }
            if (e instanceof TS3ServerQueryException && ((TS3ServerQueryException)e).getFailedPermissionID() >= 0) {
                final String permissionName = this.getPermissionName(((TS3ServerQueryException)e).getFailedPermissionID());
                if (permissionName != null) {
                    final String permissionMsg = "Missing permission or not enough power: " + permissionName;
                    if (outputToSystemOut) {
                        System.out.println(String.valueOf(this.instanceName) + ((functionName == null) ? "" : (" / " + functionName)) + ": " + permissionMsg);
                    }
                    if (this.logFile != null) {
                        this.logFile.println(String.valueOf(this.sdfDebug.format(new Date(System.currentTimeMillis()))) + "\t" + ((functionName == null) ? "JTS3ServerMod" : ("Function " + functionName)) + "\tPERMISSION_ERROR\t" + permissionMsg);
                    }
                }
            }
        }
        catch (Exception ex) {}
    }
    
    public int getClientDBID(final String uniqueID) {
        if (uniqueID == null || uniqueID.length() < 25) {
            return -1;
        }
        if (this.clientCache != null) {
            return this.clientCache.getDatabaseID(uniqueID);
        }
        try {
            HashMap<String, String> response = this.queryLib.doCommand("clientgetdbidfromuid cluid=" + uniqueID);
            if (!response.get("id").equals("0")) {
                return -1;
            }
            response = this.queryLib.parseLine(response.get("response"));
            return Integer.parseInt(response.get("cldbid"));
        }
        catch (Exception e) {
            return -1;
        }
    }
    
    static String getStackTrace(final Throwable aThrowable) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        return result.toString();
    }
    
    static /* synthetic */ void access$5(final JTS3ServerMod jts3ServerMod, final boolean updateCache) {
        jts3ServerMod.updateCache = updateCache;
    }
    
    static /* synthetic */ void access$7(final JTS3ServerMod jts3ServerMod, final Timer botTimer) {
        jts3ServerMod.botTimer = botTimer;
    }
    
    static /* synthetic */ void access$8(final JTS3ServerMod jts3ServerMod, final int reloadState) {
        jts3ServerMod.reloadState = reloadState;
    }
}
