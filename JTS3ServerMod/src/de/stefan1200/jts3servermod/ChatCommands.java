package de.stefan1200.jts3servermod;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.net.SocketFactory;

import de.stefan1200.jts3servermod.interfaces.ClientDatabaseCache_Interface;
import de.stefan1200.jts3servermod.interfaces.ServerInfoCache_Interface;
import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import de.stefan1200.jts3serverquery.TS3ServerQueryException;
import de.stefan1200.util.ArrangedPropertiesWriter;

class ChatCommands
{
    ArrangedPropertiesWriter config;
    JTS3ServerMod modClass;
    JTS3ServerQuery queryLib;
    ClientDatabaseCache_Interface clientCache;
    InstanceManager manager;
    SimpleDateFormat sdf;
    
    ChatCommands(final JTS3ServerQuery queryLib, final JTS3ServerMod modClass, final ClientDatabaseCache_Interface clientCache, final SimpleDateFormat sdf, final ArrangedPropertiesWriter config, final InstanceManager manager) {
        this.queryLib = queryLib;
        this.modClass = modClass;
        this.clientCache = clientCache;
        this.sdf = sdf;
        this.config = config;
        this.manager = manager;
    }
    
    void handleBotQuit(final String msg, final HashMap<String, String> eventInfo, final String instanceName, final boolean isFullAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        if (isFullAdmin) {
            try {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Bye Bye, my master! Stopping all instances...");
            }
            catch (Exception e) {
                this.modClass.addLogEntry(e, false);
            }
            this.manager.stopAllInstances("COMMAND", "Got !botquit command from " + eventInfo.get("invokername") + " (UID: " + eventInfo.get("invokeruid") + ") on virtual bot instance " + instanceName);
        }
        else {
            try {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master! You have to be full bot admin to use this command.");
            }
            catch (Exception e) {
                this.modClass.addLogEntry(e, false);
            }
        }
    }
    
    void handleBotReload(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin, final String CONFIG_FILE_NAME) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        if (isFullAdmin || isAdmin) {
            final JTS3ServerMod configCheck = new JTS3ServerMod(CONFIG_FILE_NAME);
            final int configOK = configCheck.loadAndCheckConfig(true);
            if (configOK == 0) {
                try {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Config file OK, restarting now!");
                }
                catch (Exception e) {
                    this.modClass.addLogEntry(e, false);
                }
                this.modClass.stopBotInstance(2);
            }
            else {
                String errorMsg = configCheck.getErrorMessage(configOK);
                try {
                    if (errorMsg.length() > 954) {
                        errorMsg = String.valueOf(errorMsg.substring(0, 950)) + "\n[...]";
                    }
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Config checked and found following errors:\n" + errorMsg);
                }
                catch (Exception e2) {
                    this.modClass.addLogEntry(e2, false);
                }
            }
        }
        else {
            try {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
            catch (Exception e3) {
                this.modClass.addLogEntry(e3, false);
            }
        }
    }
    //personalizado
    public boolean verificar(String ip, int porta) throws IOException{
    	boolean open = true;
        try {
        	Socket socket = SocketFactory.getDefault().createSocket();
            socket.setSoTimeout(500);
            socket.connect(new InetSocketAddress(ip, porta));
            socket.close();
        } catch (ConnectException e) {
            open = false;
            //System.err.println(e);
        }

        return open;
    }
    void handleStatus(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) throws IOException, NumberFormatException, TS3ServerQueryException {
		this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "[B][COLOR=#0000ff]=== [/COLOR][COLOR=#ffaa00]Status dos Servidores[/COLOR] [COLOR=#0000ff]===[/COLOR][/B]");
    	if(verificar("127.0.0.1", 5000)){
    		this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Era do Futuro: [COLOR=#00ff00][B]ONLINE[/B][/COLOR]");
    	}else{
    		this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Era do Futuro: [COLOR=#ff0000][B]OFFLINE[/B][/COLOR]");
    	}
    	if(verificar("127.0.0.1", 3000)){
    		this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Survival: [COLOR=#00ff00][B]ONLINE[/B][/COLOR]");
    	}else{
    		this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Survival: [COLOR=#ff0000][B]OFFLINE[/B][/COLOR]");
    	}
    	if(verificar("127.0.0.1", 4000)){
    		this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "PVP: [COLOR=#00ff00][B]ONLINE[/B][/COLOR]");
    	}else{
    		this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "PVP: [COLOR=#ff0000][B]OFFLINE[/B][/COLOR]");
    	}
    	if(verificar("127.0.0.1", 6000)){
    		this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "FTB: [COLOR=#00ff00][B]ONLINE[/B][/COLOR]");
    	}else{
    		this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "FTB: [COLOR=#ff0000][B]OFFLINE[/B][/COLOR]");
    	}
    }
    //personalizado
    void handleBotVersionCheck(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        try {
            if (isFullAdmin || isAdmin) {
                final StringBuffer versionInfo = new StringBuffer();
                try {
                    final HashMap<String, String> versionData = JTS3ServerMod.getVersionCheckData();
                    if (versionData != null) {
                        if (versionData.get("final.version") != null && versionData.get("final.url") != null) {
                            versionInfo.append("\n[b]Latest final version:[/b] " + versionData.get("final.version") + " [" + versionData.get("final.build") + "]" + " - [url=" + versionData.get("final.url") + "]Download[/url]");
                        }
                        if (versionData.get("dev.version") != null && versionData.get("dev.url") != null) {
                            versionInfo.append("\n[b]Latest development version:[/b] " + versionData.get("dev.version") + " [" + versionData.get("dev.build") + "]" + " - [url=" + versionData.get("dev.url") + "]Download[/url]");
                        }
                    }
                }
                catch (Exception ex) {}
                if (versionInfo.length() == 0) {
                    versionInfo.append("\nError while getting version information!");
                }
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "[b]Current installed version:[/b] 5.4.2 (18.01.2015) [5402]" + versionInfo.toString());
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotFunctionList(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        try {
            if (isFullAdmin || isAdmin) {
                final String[] funcList = this.modClass.getCurrentLoadedFunctions();
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, String.valueOf((funcList[0].length() == 0) ? "[b]No bot functions currently activated![/b]" : new StringBuilder("[b]The following bot functions are activated:[/b]\n").append(funcList[0]).toString()) + ((funcList[1].length() == 0) ? "\n[b]No bot functions currently disabled![/b]" : ("\n[b]The following bot functions are disabled:[/b]\n" + funcList[1])));
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotFunctionActivate(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                String answermsg = "Unknown Error!";
                if (arguments.length() == 0) {
                    answermsg = "Wrong usage! Right: !botfunctionactivate <function prefix>";
                }
                else {
                    final byte result = this.modClass.activateFunction(arguments);
                    if (result == -1) {
                        answermsg = "Unable to activate function \"" + arguments + "\", unknown name!";
                    }
                    else if (result == 0) {
                        answermsg = "Function \"" + arguments + "\" already activated!";
                    }
                    else if (result == 1) {
                        answermsg = "Function \"" + arguments + "\" activated successfully! If wanted, save bot config with !botcfgsave to make change permanent.";
                    }
                }
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, answermsg);
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotFunctionDisable(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                String answermsg = "Unknown Error!";
                if (arguments.length() == 0) {
                    answermsg = "Wrong usage! Right: !botfunctiondisable <function prefix>";
                }
                else {
                    final byte result = this.modClass.disableFunction(arguments);
                    if (result == -1) {
                        answermsg = "Unable to disable function \"" + arguments + "\", unknown name!";
                    }
                    else if (result == 0) {
                        answermsg = "Function \"" + arguments + "\" already disabled!";
                    }
                    else if (result == 1) {
                        answermsg = "Function \"" + arguments + "\" disabled successfully! If wanted, save bot config with !botcfgsave to make change permanent.";
                    }
                }
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, answermsg);
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotInfo(final String msg, final HashMap<String, String> eventInfo, final long startTime, final boolean isFullAdmin, final boolean isAdmin) {
        String adminText = "You have no bot admin permissions!";
        if (isFullAdmin) {
            adminText = "You have all bot admin permissions!";
        }
        else if (isAdmin) {
            adminText = "You have limited bot admin permissions!";
        }
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        try {
            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "On this server runs JTS3ServerMod 5.4.2 (18.01.2015) since " + this.modClass.getDifferenceTime(startTime, System.currentTimeMillis()) + ".\nTry !bothelp for a list of commands! " + adminText + "\nModificado por Facha");
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotInstanceStop(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final String instanceName) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        if (isFullAdmin) {
            if (arguments.length() == 0) {
                try {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Bye Bye, my master! Stopping this instance...");
                }
                catch (Exception e) {
                    this.modClass.addLogEntry(e, false);
                }
                this.modClass.stopBotInstance(0);
            }
            else if (arguments.equalsIgnoreCase(instanceName)) {
                try {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Bye Bye, my master! Stopping this instance...");
                }
                catch (Exception e) {
                    this.modClass.addLogEntry(e, false);
                }
                this.modClass.stopBotInstance(0);
            }
            else {
                try {
                    if (this.manager.stopInstance(arguments)) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Instance [b]" + arguments + "[/b] stopped!");
                    }
                    else {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Instance name [b]" + arguments + "[/b] not found or not running! Get a instance name list with !botinstancelist");
                    }
                }
                catch (Exception e) {
                    this.modClass.addLogEntry(e, false);
                }
            }
        }
        else {
            try {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master! You have to be full bot admin to use this command.");
            }
            catch (Exception e) {
                this.modClass.addLogEntry(e, false);
            }
        }
    }
    
    void handleBotInstanceStart(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final String instanceName) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin) {
                if (arguments.length() == 0) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !botinstancestart <name>");
                }
                else if (arguments.equalsIgnoreCase(instanceName)) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Instance [b]" + arguments + "[/b] is already running!");
                }
                else if (this.manager.startInstance(arguments)) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Instance [b]" + arguments + "[/b] started!");
                }
                else {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Instance name [b]" + arguments + "[/b] not found, is already running or config file missing! Get a instance name list with !botinstancelist");
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master! You have to be full bot admin to use this command.");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotInstanceList(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        try {
            if (isFullAdmin) {
                final StringBuffer instanceString = new StringBuffer();
                final Vector<String> instanceNames = this.manager.getInstanceNames();
                for (final String string : instanceNames) {
                    if (instanceString.length() > 0) {
                        instanceString.append("\n");
                    }
                    instanceString.append(string);
                    instanceString.append(" - ");
                    if (this.manager.isInstanceRunning(string) == 1) {
                        instanceString.append("Running");
                    }
                    else {
                        instanceString.append("Not Running");
                    }
                }
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "[b]List of instance names:[/b]\n" + instanceString.toString());
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master! You have to be full bot admin to use this command.");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotInstanceListReload(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        try {
            if (isFullAdmin) {
                if (this.manager.loadConfig()) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Succesfully reloaded the instance list!");
                }
                else {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while reloading the instance list!");
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master! You have to be full bot admin to use this command.");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotReloadAll(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        if (isFullAdmin) {
            try {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Restarting all instances!");
            }
            catch (Exception e) {
                this.modClass.addLogEntry(e, false);
            }
            this.manager.reloadAllInstances();
        }
        else {
            try {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
            catch (Exception e) {
                this.modClass.addLogEntry(e, false);
            }
        }
    }
    
    void handleBotCfgReload(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (this.modClass.loadConfigValues()) {
                    if (arguments.length() > 1) {
                        final int result = this.modClass.reloadConfig(arguments);
                        String msgTemp;
                        if (result == 1) {
                            msgTemp = "Bot function \"" + arguments + "\" reloaded config successfully!";
                        }
                        else if (result == 0) {
                            msgTemp = "Error while reloading config of bot function \"" + arguments + "\"!";
                        }
                        else {
                            msgTemp = "Bot function \"" + arguments + "\" not found!";
                        }
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, msgTemp);
                    }
                    else {
                        final int[] count = this.modClass.reloadConfig();
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "All bot functions reloaded config, successfull: " + Integer.toString(count[1]) + " / with error: " + Integer.toString(count[0]));
                    }
                }
                else {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Main bot config file could not be reloaded!");
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotCfgHelp(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (arguments.length() == 0) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "[b]List of config keys:[/b]\n");
                    StringBuffer keyString = new StringBuffer();
                    final Vector<String> configKeys = this.config.getKeys();
                    for (int i = 0; i < configKeys.size(); ++i) {
                        if (isFullAdmin || !configKeys.elementAt(i).toLowerCase().startsWith("ts3_")) {
                            if (keyString.length() != 0) {
                                keyString.append(", ");
                            }
                            keyString.append(configKeys.elementAt(i));
                            if (keyString.length() > 900) {
                                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, keyString.toString());
                                keyString = new StringBuffer();
                            }
                        }
                    }
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, keyString.toString());
                }
                else {
                    String helpText = this.config.getHelpText(arguments);
                    if (helpText == null) {
                        final StringBuffer keyString2 = new StringBuffer();
                        final Vector<String> configKeys2 = this.config.getKeys();
                        for (int j = 0; j < configKeys2.size(); ++j) {
                            if (configKeys2.elementAt(j).toLowerCase().startsWith(arguments.toLowerCase())) {
                                if (keyString2.length() != 0) {
                                    keyString2.append(", ");
                                }
                                keyString2.append(configKeys2.elementAt(j));
                            }
                        }
                        if (keyString2.length() > 0) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "[b]List of config keys starting with:[/b] " + arguments + "\n");
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, keyString2.toString());
                        }
                        else {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Key [b]" + arguments + "[/b] is not valid!");
                        }
                    }
                    else {
                        if (helpText.length() > 1000 - arguments.length()) {
                            helpText = String.valueOf(helpText.substring(0, 997 - arguments.length())) + "...";
                        }
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "[b]Help of " + arguments + ":[/b]\n" + helpText);
                    }
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotCfgGet(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (arguments.length() == 0) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !botcfgget <key>");
                }
                else if (arguments.length() < 3) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !botcfgget <key>");
                }
                else if (!isFullAdmin && arguments.toLowerCase().startsWith("ts3_")) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Requesting value of key [b]" + arguments + "[/b] is not allowed!");
                }
                else {
                    final String value = this.config.getValue(arguments);
                    if (value == null) {
                        if (this.config.getKeys().indexOf(arguments) == -1) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Key [b]" + arguments + "[/b] is not valid!");
                        }
                        else {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "No value set for key [b]" + arguments + "[/b]!");
                        }
                    }
                    else {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Value of [b]" + arguments + "[/b]:" + ((value.length() > 10) ? "\n" : " ") + value);
                    }
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotCfgSet(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (arguments.length() == 0) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !botcfgset <key> = <value>");
                }
                else {
                    final int pos = arguments.indexOf("=");
                    if (arguments.length() < 3 || pos == -1) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !botcfgset <key> = <value>");
                    }
                    else {
                        final String key = arguments.substring(0, pos).trim();
                        final String value = arguments.substring(pos + 1).trim();
                        if (!this.config.canSaveToFile(key)) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Setting value for key [b]" + key + "[/b] is not possible, because it's write protected! Please change it directly at the file and use !botcfgreload to reload this values without restarting the bot.");
                            return;
                        }
                        if (!isFullAdmin && key.toLowerCase().startsWith("ts3_")) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Setting value for key [b]" + key + "[/b] is not allowed!");
                            return;
                        }
                        if (this.config.setValue(key, value)) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Successfully set key [b]" + key + "[/b] to value:" + ((value.length() > 6) ? "\n" : " ") + value + "\nDon't forget to do [b]!botcfgsave[/b] to make this change permanent!");
                        }
                        else {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Key [b]" + key + "[/b] is not valid!");
                        }
                    }
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotCfgCheck(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin, final String CONFIG_FILE_NAME) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        try {
            if (isFullAdmin || isAdmin) {
                final JTS3ServerMod configCheck = new JTS3ServerMod(this.config, CONFIG_FILE_NAME);
                final int configOK = configCheck.loadAndCheckConfig(false);
                if (configOK == 0) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Config OK!");
                }
                else {
                    String errorMsg = configCheck.getErrorMessage(configOK);
                    if (errorMsg.length() > 954) {
                        errorMsg = String.valueOf(errorMsg.substring(0, 950)) + "\n[...]";
                    }
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Config checked and found following errors:\n" + errorMsg);
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotCfgSave(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin, final String CONFIG_FILE_NAME) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        try {
            if (isFullAdmin || isAdmin) {
                final JTS3ServerMod configCheck = new JTS3ServerMod(this.config, CONFIG_FILE_NAME);
                final int configOK = configCheck.loadAndCheckConfig(false);
                if (configOK == 0) {
                    if (this.config.save(CONFIG_FILE_NAME, "Config file of the JTS3ServerMod\nhttp://www.stefan1200.de\nThis file must be saved with the encoding ISO-8859-1!")) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Config OK and written to disk! Do [b]!botcfgreload[/b] or [b]!botreload[/b] to see the changes!");
                    }
                    else {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Config OK, but an error occurred while writing to disk! Maybe file write protected?");
                    }
                }
                else {
                    String errorMsg = configCheck.getErrorMessage(configOK);
                    if (errorMsg.length() > 954) {
                        errorMsg = String.valueOf(errorMsg.substring(0, 950)) + "\n[...]";
                    }
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Config checked and found following errors:\n" + errorMsg + "\nNot written to disk!");
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleClientSearch(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (this.clientCache == null) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Client database cache disabled, command disabled!");
                }
                else if (arguments.length() == 0) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !clientsearch <clientname or unique id>\nYou can use * as wildcard (client name only)!");
                }
                else if (arguments.indexOf("**") != -1) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage, only single wildcards are allowed!");
                }
                else if (this.clientCache.isUpdateRunning()) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Client database cache is updating, please wait some time and try again!");
                }
                else {
                    final Vector<Integer> clientSearch = this.clientCache.searchClientNickname(arguments);
                    if (clientSearch == null) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage, use a valid search pattern with at least 3 characters!");
                    }
                    else if (clientSearch.size() == 0) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "No clients found in the database!");
                    }
                    else if (clientSearch.size() > 5 && clientSearch.size() <= 20) {
                        final StringBuffer sb = new StringBuffer("Found " + Integer.toString(clientSearch.size()) + " entries in the database:\n");
                        boolean firstLine = true;
                        for (final int clientDBID : clientSearch) {
                            if (!firstLine) {
                                sb.append(", ");
                            }
                            sb.append(this.clientCache.getNickname(clientDBID));
                            if (firstLine) {
                                firstLine = false;
                            }
                        }
                        sb.append("\nPlease refine your search!");
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, sb.toString());
                    }
                    else if (clientSearch.size() > 20) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Found " + Integer.toString(clientSearch.size()) + " entries in the database, please refine your search.");
                    }
                    else {
                        final StringBuffer sb = new StringBuffer("Found " + Integer.toString(clientSearch.size()) + " entries in the database:\n");
                        final Vector<HashMap<String, String>> clientList = this.modClass.getClientList();
                        boolean foundClient = false;
                        for (final int clientDBID2 : clientSearch) {
                            try {
                                final long createdAt = this.clientCache.getCreatedAt(clientDBID2) * 1000L;
                                sb.append("[b]" + this.clientCache.getNickname(clientDBID2) + "[/b] ([i]DB ID:[/i] " + Integer.toString(clientDBID2) + ")  [i]public unique ID:[/i] " + this.clientCache.getUniqueID(clientDBID2) + "  [i]last IP:[/i] " + this.clientCache.getLastIP(clientDBID2) + "  [i]created at:[/i] " + this.sdf.format(new Date(createdAt)) + "  [i]last seen at:[/i] ");
                                for (final HashMap<String, String> clientOnline : clientList) {
                                    if (clientDBID2 == Integer.parseInt(clientOnline.get("client_database_id"))) {
                                        sb.append("currently online (id: " + clientOnline.get("clid") + ")\n");
                                        foundClient = true;
                                        break;
                                    }
                                }
                                if (!foundClient) {
                                    try {
                                        final long lastOnline = this.clientCache.getLastOnline(clientDBID2) * 1000L;
                                        sb.append(String.valueOf(this.sdf.format(new Date(lastOnline))) + "\n");
                                    }
                                    catch (Exception e) {
                                        this.modClass.addLogEntry(e, false);
                                    }
                                }
                                foundClient = false;
                            }
                            catch (Exception e2) {
                                this.modClass.addLogEntry(e2, false);
                            }
                        }
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, sb.toString());
                    }
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e3) {
            this.modClass.addLogEntry(e3, false);
        }
    }
    
    void handleSearchIP(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (this.clientCache == null) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Client database cache disabled, command disabled!");
                }
                else if (arguments.length() == 0) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !searchip <ip address>\nYou can use * as wildcard!");
                }
                else if (arguments.indexOf("**") != -1) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage, only single wildcards are allowed!");
                }
                else if (this.clientCache.isUpdateRunning()) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Client database cache is updating, please wait some time and try again!");
                }
                else {
                    final Vector<Integer> ipSearch = this.clientCache.searchIPAddress(arguments);
                    if (ipSearch == null) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage, use a valid search pattern with at least 3 characters!");
                    }
                    else if (ipSearch.size() == 0) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "No clients found in the database!");
                    }
                    else if (ipSearch.size() > 5 && ipSearch.size() <= 20) {
                        final StringBuffer sb = new StringBuffer("Found " + Integer.toString(ipSearch.size()) + " entries in the database:\n");
                        boolean firstLine = true;
                        for (final int clientDBID : ipSearch) {
                            if (!firstLine) {
                                sb.append(", ");
                            }
                            sb.append(this.clientCache.getLastIP(clientDBID));
                            if (firstLine) {
                                firstLine = false;
                            }
                        }
                        sb.append("\nPlease refine your search!");
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, sb.toString());
                    }
                    else if (ipSearch.size() > 20) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Found " + Integer.toString(ipSearch.size()) + " entries in the database, please refine your search.");
                    }
                    else {
                        final StringBuffer sb = new StringBuffer("Found " + Integer.toString(ipSearch.size()) + " entries in the database:\n");
                        final Vector<HashMap<String, String>> clientList = this.modClass.getClientList();
                        boolean foundClient = false;
                        for (final int clientDBID2 : ipSearch) {
                            try {
                                final long createdAt = this.clientCache.getCreatedAt(clientDBID2) * 1000L;
                                sb.append("[b]" + this.clientCache.getNickname(clientDBID2) + "[/b] ([i]DB ID:[/i] " + Integer.toString(clientDBID2) + ")  [i]public unique ID:[/i] " + this.clientCache.getUniqueID(clientDBID2) + "  [i]last IP:[/i] " + this.clientCache.getLastIP(clientDBID2) + "  [i]created at:[/i] " + this.sdf.format(new Date(createdAt)) + "  [i]last seen at:[/i] ");
                                for (final HashMap<String, String> clientOnline : clientList) {
                                    if (clientDBID2 == Integer.parseInt(clientOnline.get("client_database_id"))) {
                                        sb.append("currently online (id: " + clientOnline.get("clid") + ")\n");
                                        foundClient = true;
                                        break;
                                    }
                                }
                                if (!foundClient) {
                                    try {
                                        final long lastOnline = this.clientCache.getLastOnline(clientDBID2) * 1000L;
                                        sb.append(String.valueOf(this.sdf.format(new Date(lastOnline))) + "\n");
                                    }
                                    catch (Exception e) {
                                        this.modClass.addLogEntry(e, false);
                                    }
                                }
                                foundClient = false;
                            }
                            catch (Exception e2) {
                                this.modClass.addLogEntry(e2, false);
                            }
                        }
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, sb.toString());
                    }
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e3) {
            this.modClass.addLogEntry(e3, false);
        }
    }
    
    void handleListInactiveClients(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (this.clientCache == null) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Client database cache disabled, command disabled!");
                }
                else if (arguments.length() == 0) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !listinactiveclients <minimum days inactive>");
                }
                else {
                    int days = -1;
                    try {
                        days = Integer.parseInt(arguments);
                    }
                    catch (Exception e2) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !listinactiveclients <minimum days inactive>");
                    }
                    if (days >= 10) {
                        final Vector<Integer> result = this.clientCache.searchInactiveClients(days);
                        if (result == null) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Please wait until the client database cache is ready!");
                        }
                        else if (result.size() > 10) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Found " + Integer.toString(result.size()) + " clients which are inactive for at least " + Integer.toString(days) + " days! Please use a higher day value to list this clients.");
                        }
                        else {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Found " + Integer.toString(result.size()) + " clients which are inactive for at least " + Integer.toString(days) + " days!");
                            final StringBuffer clientList = new StringBuffer();
                            final long currentTime = System.currentTimeMillis() / 1000L;
                            for (int i = 0; i < result.size(); ++i) {
                                if (clientList.length() > 0) {
                                    clientList.append(", ");
                                }
                                clientList.append("[b]" + this.clientCache.getNickname(result.elementAt(i)) + "[/b] ([i]DB ID:[/i] ");
                                clientList.append(result.elementAt(i));
                                clientList.append(" - ");
                                clientList.append((int)((currentTime - this.clientCache.getLastOnline(result.elementAt(i))) / 86400L));
                                clientList.append(" [i]days[/i])");
                            }
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, clientList.toString());
                        }
                    }
                    else {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Lowest possible days to list inactive players are 10 days!");
                    }
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleListInactiveChannels(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (this.modClass.getChannelList() == null || this.modClass.getChannelList().size() == 0) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Missing channel list, command disabled!");
                    return;
                }
                final Vector<HashMap<String, String>> channelList = (Vector<HashMap<String, String>>)this.modClass.getChannelList().clone();
                final ChannelEmptyComparator cec = new ChannelEmptyComparator();
                Collections.sort(channelList, cec);
                final long curtime = System.currentTimeMillis();
                final StringBuffer sbChannelList = new StringBuffer();
                if (arguments.length() > 0) {
                    int channelID = -1;
                    try {
                        channelID = Integer.parseInt(arguments);
                    }
                    catch (NumberFormatException ex) {}
                    int count = 0;
                    for (final HashMap<String, String> channel : channelList) {
                        if (channelID >= 0) {
                            if (Integer.parseInt(channel.get("cid")) == channelID) {
                                String timeString = "Not empty!";
                                if (Integer.parseInt(channel.get("seconds_empty")) >= 0) {
                                    timeString = "Empty since " + this.modClass.getDifferenceTime(curtime - Integer.parseInt(channel.get("seconds_empty")) * 1000, curtime);
                                }
                                sbChannelList.append("[b]" + channel.get("channel_name") + "[/b] (ID: ");
                                sbChannelList.append(channel.get("cid"));
                                sbChannelList.append(") - ");
                                sbChannelList.append(timeString);
                                ++count;
                                break;
                            }
                            continue;
                        }
                        else {
                            if (Integer.parseInt(channel.get("seconds_empty")) <= 0) {
                                break;
                            }
                            if (channel.get("channel_name").toLowerCase().indexOf(arguments.toLowerCase()) == -1) {
                                continue;
                            }
                            if (sbChannelList.length() > 0) {
                                sbChannelList.append("\n");
                            }
                            sbChannelList.append("[b]" + channel.get("channel_name") + "[/b] (ID: ");
                            sbChannelList.append(channel.get("cid"));
                            sbChannelList.append(") - ");
                            sbChannelList.append(this.modClass.getDifferenceTime(curtime - Integer.parseInt(channel.get("seconds_empty")) * 1000, curtime));
                            ++count;
                            if (sbChannelList.length() > 800 - arguments.length()) {
                                break;
                            }
                            continue;
                        }
                    }
                    if (channelID >= 0) {
                        if (count == 0) {
                            sbChannelList.append("No channel found with channel ID " + Integer.toString(channelID) + "!");
                        }
                    }
                    else {
                        if (count == 0) {
                            sbChannelList.append("No channels found with this name!");
                        }
                        sbChannelList.insert(0, "List of empty channels (sorted by empty since time). Displaying " + Integer.toString(count) + " of " + Integer.toString(channelList.size()) + " channels with the search string \"" + arguments + "\":\n");
                    }
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, sbChannelList.toString());
                }
                else {
                    for (int i = 0; i < channelList.size(); ++i) {
                        if (Integer.parseInt(channelList.get(i).get("seconds_empty")) <= 0) {
                            break;
                        }
                        if (sbChannelList.length() > 0) {
                            sbChannelList.append("\n");
                        }
                        sbChannelList.append("[b]" + channelList.get(i).get("channel_name") + "[/b] (ID: ");
                        sbChannelList.append(channelList.get(i).get("cid"));
                        sbChannelList.append(") - ");
                        sbChannelList.append(this.modClass.getDifferenceTime(curtime - Integer.parseInt(channelList.get(i).get("seconds_empty")) * 1000, curtime));
                        if (sbChannelList.length() > 850) {
                            break;
                        }
                    }
                    if (sbChannelList.length() > 0) {
                        sbChannelList.insert(0, "List of empty channels (sorted by empty since time):\n");
                    }
                    else {
                        sbChannelList.append("No channel is empty!");
                    }
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, sbChannelList.toString());
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleSetChannelGroup(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (arguments.length() >= 5) {
                    int clientDBIDArg = -1;
                    int channelGroupID = -1;
                    final Vector<Integer> channelList = new Vector<Integer>();
                    try {
                        int pos = 0;
                        int pos2 = arguments.indexOf(" ", pos);
                        try {
                            clientDBIDArg = Integer.parseInt(arguments.substring(pos, pos2));
                        }
                        catch (NumberFormatException e2) {
                            clientDBIDArg = this.modClass.getClientDBID(arguments.substring(pos, pos2));
                            if (clientDBIDArg < 0) {
                                throw new NumberFormatException("Got invalid client database id or unique id!");
                            }
                        }
                        pos = pos2 + 1;
                        pos2 = arguments.indexOf(" ", pos);
                        channelGroupID = Integer.parseInt(arguments.substring(pos, pos2));
                        final StringTokenizer st = new StringTokenizer(arguments.substring(pos2 + 1), ",", false);
                        while (st.hasMoreTokens()) {
                            channelList.addElement(Integer.parseInt(st.nextToken().trim()));
                        }
                    }
                    catch (NumberFormatException nfe) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while parsing numbers, command aborted...");
                        this.modClass.addLogEntry(nfe, false);
                    }
                    if (channelList.size() == 0) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage, no channels given! Right: !setchannelgroup <client database id or unique id> <channel group id> <channel list separated with comma>");
                        return;
                    }
                    int count = 0;
                    int errorcount = 0;
                    for (final int channelID : channelList) {
                        final HashMap<String, String> actionResponse = this.queryLib.doCommand("setclientchannelgroup cgid=" + Integer.toString(channelGroupID) + " cid=" + channelID + " cldbid=" + Integer.toString(clientDBIDArg));
                        if (actionResponse.get("id").equals("0")) {
                            ++count;
                        }
                        else {
                            ++errorcount;
                        }
                    }
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Channel group for client database id " + Integer.toString(clientDBIDArg) + " successfully set to channel group id " + Integer.toString(channelGroupID) + " for " + Integer.toString(count) + " channels!" + ((errorcount > 0) ? (" " + Integer.toString(errorcount) + " channels could not set to default channel group!") : ""));
                }
                else {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !setchannelgroup <client database id or unique id> <channel group id> <channel list separated with comma>");
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleRemoveServerGroups(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (arguments.length() >= 1) {
                    final Vector<Integer> serverGroupList = new Vector<Integer>();
                    int clientDBIDArg = -1;
                    boolean doAction = false;
                    try {
                        try {
                            clientDBIDArg = Integer.parseInt(arguments);
                        }
                        catch (NumberFormatException e2) {
                            clientDBIDArg = this.modClass.getClientDBID(arguments);
                            if (clientDBIDArg < 0) {
                                throw new NumberFormatException("Got invalid client database id or unique id!");
                            }
                        }
                        final HashMap<String, String> sgcListResponse = this.queryLib.doCommand("servergroupsbyclientid cldbid=" + Integer.toString(clientDBIDArg));
                        if (sgcListResponse.get("id").equals("0")) {
                            final ServerInfoCache_Interface serverInfoCache = this.modClass.getServerInfoCache();
                            final int defsgid = serverInfoCache.getServerDefaultServerGroup();
                            final Vector<HashMap<String, String>> serverGroupClientList = this.queryLib.parseRawData(sgcListResponse.get("response"));
                            for (final HashMap<String, String> hashMap : serverGroupClientList) {
                                if (defsgid != Integer.parseInt(hashMap.get("sgid"))) {
                                    serverGroupList.addElement(Integer.parseInt(hashMap.get("sgid")));
                                }
                            }
                            doAction = true;
                        }
                    }
                    catch (NumberFormatException nfe) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while parsing numbers, command aborted...");
                        this.modClass.addLogEntry(nfe, false);
                    }
                    if (doAction) {
                        int count = 0;
                        int errorcount = 0;
                        for (final int serverGroupID : serverGroupList) {
                            final HashMap<String, String> actionResponse = this.queryLib.doCommand("servergroupdelclient sgid=" + serverGroupID + " cldbid=" + Integer.toString(clientDBIDArg));
                            if (actionResponse.get("id").equals("0")) {
                                ++count;
                            }
                            else {
                                ++errorcount;
                            }
                        }
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Removed " + Integer.toString(count) + " server groups of client database id " + Integer.toString(clientDBIDArg) + " successfully!" + ((errorcount > 0) ? (" " + Integer.toString(errorcount) + " server groups could not removed!") : ""));
                    }
                }
                else {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !removeservergroups <client database id or unique id>");
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleRemoveChannelGroups(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (arguments.length() >= 1) {
                    final ServerInfoCache_Interface serverInfoCache = this.modClass.getServerInfoCache();
                    if (serverInfoCache == null) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while getting server info, command aborted...");
                    }
                    else {
                        final Vector<Integer> channelList = new Vector<Integer>();
                        int defChannelGroupID = -1;
                        int clientDBIDArg = -1;
                        boolean doAction = false;
                        try {
                            defChannelGroupID = serverInfoCache.getServerDefaultChannelGroup();
                            if (defChannelGroupID < 0) {
                                throw new NumberFormatException("Got invalid default channel group id!");
                            }
                            try {
                                clientDBIDArg = Integer.parseInt(arguments);
                            }
                            catch (NumberFormatException e2) {
                                clientDBIDArg = this.modClass.getClientDBID(arguments);
                                if (clientDBIDArg < 0) {
                                    throw new NumberFormatException("Got invalid client database id or unique id!");
                                }
                            }
                            final HashMap<String, String> cgcListResponse = this.queryLib.doCommand("channelgroupclientlist cldbid=" + Integer.toString(clientDBIDArg));
                            if (cgcListResponse.get("id").equals("0")) {
                                final Vector<HashMap<String, String>> channelGroupClientList = this.queryLib.parseRawData(cgcListResponse.get("response"));
                                for (final HashMap<String, String> hashMap : channelGroupClientList) {
                                    if (Integer.parseInt(hashMap.get("cgid")) != defChannelGroupID) {
                                        channelList.addElement(Integer.parseInt(hashMap.get("cid")));
                                    }
                                }
                                doAction = true;
                            }
                            else {
                                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while getting channel group list for the client database id " + Integer.toString(clientDBIDArg) + ", reason from TS3 server: " + cgcListResponse.get("msg"));
                            }
                        }
                        catch (NumberFormatException nfe) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while parsing numbers, command aborted...");
                            this.modClass.addLogEntry(nfe, false);
                        }
                        if (doAction) {
                            int count = 0;
                            int errorcount = 0;
                            for (final int channelID : channelList) {
                                final HashMap<String, String> actionResponse = this.queryLib.doCommand("setclientchannelgroup cgid=" + Integer.toString(defChannelGroupID) + " cid=" + channelID + " cldbid=" + Integer.toString(clientDBIDArg));
                                if (actionResponse.get("id").equals("0")) {
                                    ++count;
                                }
                                else {
                                    ++errorcount;
                                }
                            }
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Channel group for client database id " + Integer.toString(clientDBIDArg) + " successfully set to default channel group for " + Integer.toString(count) + " channels!" + ((errorcount > 0) ? (" " + Integer.toString(errorcount) + " channels could not set to default channel group!") : ""));
                        }
                    }
                }
                else {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !removechannelgroups <client database id or unique id>");
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleMsgChannelGroup(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (arguments.length() >= 3) {
                    final int pos = arguments.indexOf(" ");
                    if (pos == -1) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !msgchannelgroup <channelgroup id> <message>");
                    }
                    else {
                        final Vector<Integer> clientListSend = new Vector<Integer>();
                        final String sendMessage = "Message from " + eventInfo.get("invokername") + ": " + arguments.substring(pos + 1);
                        boolean doAction = false;
                        try {
                            final Vector<Integer> groupList = new Vector<Integer>();
                            final String temp = arguments.substring(0, pos);
                            if (temp != null && temp.length() > 0) {
                                final StringTokenizer st = new StringTokenizer(temp, ",", false);
                                while (st.hasMoreTokens()) {
                                    groupList.addElement(Integer.parseInt(st.nextToken().trim()));
                                }
                            }
                            final Vector<HashMap<String, String>> clientListGroups = this.queryLib.getList(1, "-groups");
                            for (final HashMap<String, String> hashMap : clientListGroups) {
                                if (groupList.indexOf(Integer.parseInt(hashMap.get("client_channel_group_id"))) >= 0 && Integer.parseInt(hashMap.get("client_type")) == 0 && clientListSend.indexOf(Integer.parseInt(hashMap.get("clid"))) == -1) {
                                    clientListSend.addElement(Integer.parseInt(hashMap.get("clid")));
                                }
                            }
                            doAction = true;
                        }
                        catch (NumberFormatException nfe) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while parsing numbers, command aborted...");
                        }
                        if (doAction) {
                            int count = 0;
                            int errorcount = 0;
                            for (final int clientid : clientListSend) {
                                try {
                                    this.queryLib.sendTextMessage(clientid, 1, sendMessage);
                                    ++count;
                                }
                                catch (Exception e2) {
                                    ++errorcount;
                                }
                            }
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Message sent to " + Integer.toString(count) + " clients!" + ((errorcount > 0) ? (" Error while sending message to " + Integer.toString(errorcount) + " clients!") : ""));
                        }
                    }
                }
                else {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !msgchannelgroup <channelgroup id> <message>");
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleMsgServerGroup(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (arguments.length() >= 3) {
                    final int pos = arguments.indexOf(" ");
                    if (pos == -1) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !msgservergroup <servergroup id> <message>");
                    }
                    else {
                        final Vector<Integer> clientListSend = new Vector<Integer>();
                        final String sendMessage = "Message from " + eventInfo.get("invokername") + ": " + arguments.substring(pos + 1);
                        boolean doAction = false;
                        try {
                            final Vector<Integer> groupList = new Vector<Integer>();
                            final String temp = arguments.substring(0, pos);
                            if (temp != null && temp.length() > 0) {
                                final StringTokenizer st = new StringTokenizer(temp, ",", false);
                                while (st.hasMoreTokens()) {
                                    groupList.addElement(Integer.parseInt(st.nextToken().trim()));
                                }
                            }
                            final Vector<HashMap<String, String>> clientListGroups = this.queryLib.getList(1, "-groups");
                            for (final HashMap<String, String> hashMap : clientListGroups) {
                                if (this.modClass.isGroupListed(hashMap.get("client_servergroups"), groupList) && Integer.parseInt(hashMap.get("client_type")) == 0 && clientListSend.indexOf(Integer.parseInt(hashMap.get("clid"))) == -1) {
                                    clientListSend.addElement(Integer.parseInt(hashMap.get("clid")));
                                }
                            }
                            doAction = true;
                        }
                        catch (NumberFormatException nfe) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while parsing numbers, command aborted...");
                        }
                        if (doAction) {
                            int count = 0;
                            int errorcount = 0;
                            for (final int clientid : clientListSend) {
                                try {
                                    this.queryLib.sendTextMessage(clientid, 1, sendMessage);
                                    ++count;
                                }
                                catch (Exception e2) {
                                    ++errorcount;
                                }
                            }
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Message sent to " + Integer.toString(count) + " clients!" + ((errorcount > 0) ? (" Error while sending message to " + Integer.toString(errorcount) + " clients!") : ""));
                        }
                    }
                }
                else {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !msgservergroup <servergroup id> <message>");
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleSetChannelName(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (arguments.length() < 3) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !setchannelname <channel id> <new channel name>");
                    return;
                }
                final int pos = arguments.indexOf(" ");
                if (pos == -1) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !setchannelname <channel id> <new channel name>");
                    return;
                }
                int channelID;
                try {
                    channelID = Integer.parseInt(arguments.substring(0, pos));
                }
                catch (Exception e2) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! The channel id has to be a number!");
                    return;
                }
                final String newChannelName = arguments.substring(pos + 1).trim();
                final HashMap<String, String> actionResponse = this.queryLib.doCommand("channeledit cid=" + Integer.toString(channelID) + " channel_name=" + this.queryLib.encodeTS3String(newChannelName));
                if (actionResponse.get("id").equals("0")) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Channel id " + Integer.toString(channelID) + " was successfully renamed to: " + newChannelName);
                }
                else {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while rename channel id " + Integer.toString(channelID) + "!");
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleGetChannelID(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                final Vector<HashMap<String, String>> channelList = this.modClass.getChannelList();
                if (channelList == null) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while getting channel list. Command aborted!");
                    return;
                }
                final StringBuffer sbChannelList = new StringBuffer();
                int count = 0;
                if (arguments.length() == 0) {
                    for (final HashMap<String, String> channel : channelList) {
                        sbChannelList.append("ID: " + channel.get("cid") + " - [b]" + channel.get("channel_name") + "[/b]\n");
                        ++count;
                        if (sbChannelList.length() > 920) {
                            break;
                        }
                    }
                    sbChannelList.insert(0, "Displaying " + Integer.toString(count) + " of " + Integer.toString(channelList.size()) + " channels:\n");
                }
                else {
                    int channelID = -1;
                    try {
                        channelID = Integer.parseInt(arguments);
                    }
                    catch (NumberFormatException ex) {}
                    for (final HashMap<String, String> channel2 : channelList) {
                        if (channelID >= 0) {
                            if (Integer.parseInt(channel2.get("cid")) == channelID) {
                                sbChannelList.append("Channel ID: " + channel2.get("cid") + " - [b]" + channel2.get("channel_name") + "[/b]");
                                ++count;
                                break;
                            }
                            continue;
                        }
                        else {
                            if (channel2.get("channel_name").toLowerCase().indexOf(arguments.toLowerCase()) == -1) {
                                continue;
                            }
                            sbChannelList.append("ID: " + channel2.get("cid") + " - [b]" + channel2.get("channel_name") + "[/b]\n");
                            ++count;
                            if (sbChannelList.length() > 890 - arguments.length()) {
                                break;
                            }
                            continue;
                        }
                    }
                    if (channelID >= 0) {
                        if (count == 0) {
                            sbChannelList.append("No channel found with channel ID " + Integer.toString(channelID) + "!");
                        }
                    }
                    else {
                        if (count == 0) {
                            sbChannelList.append("No channels found with this name!");
                        }
                        sbChannelList.insert(0, "Displaying " + Integer.toString(count) + " of " + Integer.toString(channelList.size()) + " channels with the search string \"" + arguments + "\":\n");
                    }
                }
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, sbChannelList.toString());
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleExec(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        try {
            if (msg.toLowerCase().startsWith("!execwait")) {
                this.handleExecWait(msg, eventInfo, isFullAdmin);
            }
            else {
                final String arguments = this.getArguments(msg);
                if (isFullAdmin) {
                    if (this.manager.isCommandExecAllowed()) {
                        if (arguments.length() > 0) {
                            try {
                                Runtime.getRuntime().exec(arguments);
                                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Command executed!");
                            }
                            catch (Exception e) {
                                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while executing command: " + e.toString());
                            }
                        }
                        else {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !exec <system command>");
                        }
                    }
                    else {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Command disabled!");
                    }
                }
                else {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master! You have to be full bot admin to use this command.");
                }
            }
        }
        catch (Exception e2) {
            this.modClass.addLogEntry(e2, false);
        }
    }
    
    void handleExecWait(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin) {
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin) {
                if (this.manager.isCommandExecAllowed()) {
                    if (arguments.length() > 0) {
                        try {
                            final Process proc = Runtime.getRuntime().exec(arguments);
                            final BufferedInputStream bisOut = new BufferedInputStream(proc.getInputStream());
                            final BufferedInputStream bisErr = new BufferedInputStream(proc.getErrorStream());
                            final int returnValue = proc.waitFor();
                            final StringBuffer output = new StringBuffer();
                            while (bisOut.available() > 0) {
                                output.append((char)bisOut.read());
                            }
                            while (bisErr.available() > 0) {
                                output.append((char)bisErr.read());
                            }
                            if (output.length() > 1004) {
                                output.setLength(1000);
                                output.append("\n[...]");
                            }
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Command executed with a return value of " + Integer.toString(returnValue) + "!\n");
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Output:\n" + output.toString());
                        }
                        catch (Exception e) {
                            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while executing command: " + e.toString());
                        }
                    }
                    else {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !execwait <system command>");
                    }
                }
                else {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Command disabled!");
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master! You have to be full bot admin to use this command.");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotJoinChannel(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (arguments.length() == 0) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !botjoinchannel <channel id>");
                    return;
                }
                try {
                    final int newChannelID = Integer.parseInt(arguments);
                    if (newChannelID == this.queryLib.getCurrentQueryClientChannelID()) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Bot is already in the given channel!");
                        return;
                    }
                    try {
                        this.queryLib.moveClient(this.queryLib.getCurrentQueryClientID(), newChannelID, null);
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Bot was moved into the given channel!");
                    }
                    catch (Exception e) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while moving bot into the given channel!");
                        this.modClass.addLogEntry(e, false);
                    }
                    return;
                }
                catch (Exception e2) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while reading given channel id!");
                    return;
                }
            }
            this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
        }
        catch (Exception e2) {
            this.modClass.addLogEntry(e2, false);
        }
    }
    
    void handleBotRename(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        String arguments = this.getArguments(msg);
        try {
            if (isFullAdmin || isAdmin) {
                if (arguments.length() == 0) {
                    if (this.config.getValue("bot_server_query_name") == null) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "No default name set in bot config! Add a new name to command: !botrename <new name>");
                        return;
                    }
                    if (this.queryLib.getCurrentQueryClientName() != null && this.config.getValue("bot_server_query_name").equals(this.queryLib.getCurrentQueryClientName())) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Bot already has the default name! Add a new name to command: !botrename <new name>");
                        return;
                    }
                    arguments = this.config.getValue("bot_server_query_name");
                }
                if (arguments.length() < 3) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while renaming bot, new name needs at least 3 characters!");
                    return;
                }
                if (this.queryLib.getCurrentQueryClientName() != null && arguments.equals(this.queryLib.getCurrentQueryClientName())) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Bot already has the name: " + arguments);
                    return;
                }
                try {
                    this.queryLib.setDisplayName(arguments);
                    if (this.config.getValue("bot_server_query_name") != null && this.config.getValue("bot_server_query_name").equals(arguments)) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Bot name changed back to default name: " + arguments);
                    }
                    else {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Bot name temporary changed to: " + arguments);
                    }
                }
                catch (TS3ServerQueryException sqe) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while renaming bot to: " + arguments + "\n" + sqe.getMessage());
                }
                catch (Exception e) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Error while renaming bot to: " + arguments + "\nError: " + e.toString());
                }
            }
            else {
                this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "You are not my master!");
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    void handleBotHelp(final String msg, final HashMap<String, String> eventInfo, final boolean isFullAdmin, final boolean isAdmin) {
        this.modClass.addLogEntry((byte)1, "Got command from " + eventInfo.get("invokername") + ": " + msg, false);
        final String arguments = this.getArguments(msg);
        try {
            if (arguments.length() == 0) {
                final StringBuffer helpText = new StringBuffer("List of commands:\n!botinfo");
                final StringBuffer helpTextAdmin = new StringBuffer();
                final StringBuffer helpTextFullAdmin = new StringBuffer();
                helpText.append(this.modClass.getCommandList(eventInfo, isFullAdmin, isAdmin));
                if (isFullAdmin || isAdmin) {
                    helpTextAdmin.append("You can also use the following admin commands:\n!botcfghelp [config key]\n!botcfgget <config key>\n!botcfgset <config key> = <config value>\n!botcfgcheck\n!botcfgreload\n!botcfgsave\n!botfunctionlist\n!botfunctionactivate <prefix>\n!botfunctiondisable <prefix>\n!botjoinchannel <channel id>\n!botreload\n!botrename <new name>\n!botversioncheck\n!clientsearch <nickname>\n!getChannelID [channel id or channel name]\n!listinactivechannels [channel id or channel name]\n!listinactiveclients <minimum days inactive>\n!msgchannelgroup <channelgroup id> <message>\n!msgservergroup <servergroup id> <message>\n!removeservergroups <client database id>\n!removechannelgroups <client database id>\n!searchip <ip address>\n!setchannelgroup <client database id> <channel group id> <channel list separated with comma>\n!setchannelname <channel id> <new channel name>");
                }
                if (isFullAdmin) {
                    helpTextFullAdmin.append("You can also use the following full admin commands:\n!botinstancestart <name>\n!botinstancestop <name>\n!botinstancelist\n!botinstancelistreload\n!botreloadall\n!botquit");
                }
                if (isFullAdmin && this.manager.isCommandExecAllowed()) {
                    helpTextFullAdmin.append("\n!exec <system command>\n!execwait <system command>");
                }
                final String lastMessage = "\n\nTo get a help about the commands, just do !bothelp <command>";
                if (isFullAdmin) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, helpText.toString());
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, helpTextAdmin.toString());
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, String.valueOf(helpTextFullAdmin.toString()) + lastMessage);
                }
                else if (isAdmin) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, helpText.toString());
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, String.valueOf(helpTextAdmin.toString()) + lastMessage);
                }
                else {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, String.valueOf(helpText.toString()) + lastMessage);
                }
            }
            else {
                String args = arguments.toLowerCase();
                if (args.length() < 3) {
                    this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Wrong usage! Right: !bothelp <command>");
                }
                else {
                    if (args.charAt(0) == '!') {
                        args = args.substring(1);
                    }
                    String helpText2 = null;
                    if (args.equals("botinfo") || args.equals("info")) {
                        helpText2 = "Shows you the running bot version and uptime. It also displays the current bot admin permission level.";
                    }
                    else if (args.equals("botquit") || args.equals("quit") || args.equals("exit")) {
                        helpText2 = "Disconnect and quits all bot instances.";
                    }
                    else if (args.equals("botreload") || args.equals("reconnect")) {
                        helpText2 = "Disconnects the current bot instance, reload bot configuration and start bot instance again. Bot configuration will be checked first!";
                    }
                    else if (args.equals("botreloadall")) {
                        helpText2 = "Disconnects all bot instances, reload bot configuration and start all bot instances again. This command do not check bot configuration first!";
                    }
                    else if (args.equals("botversioncheck") || args.equals("botversion") || args.equals("version")) {
                        helpText2 = "Displays current installed, latest final and latest development (if exists) versions of this bot.";
                    }
                    else if (args.equals("botcfghelp")) {
                        helpText2 = "Returns informations about a config key. If no key argument given, a list of config keys will be returned.\nExample: !botcfghelp bot_channel_id";
                    }
                    else if (args.equals("botcfgget")) {
                        helpText2 = "Returns the value of a current config key.\nExample: !botcfgget bot_channel_id";
                    }
                    else if (args.equals("botcfgset")) {
                        helpText2 = "Set a new value for a config key. Notice: This changes will not used by the bot without saving and reloading!\nExample: !botcfgset bot_channel_id = -1";
                    }
                    else if (args.equals("botcfgcheck")) {
                        helpText2 = "Check if current config (for example after !botcfgset) is valid.";
                    }
                    else if (args.equals("botcfgreload")) {
                        helpText2 = "Reloads the bot configuration. You can use a function name as argument to reload only that configuration of that function.";
                    }
                    else if (args.equals("botcfgsave")) {
                        helpText2 = "Saves current bot configuration.";
                    }
                    else if (args.equals("botfunctionlist") || args.equals("functionlist")) {
                        helpText2 = "Get a list of currently loaded functions.";
                    }
                    else if (args.equals("botfunctionactivate") || args.equals("functionon")) {
                        helpText2 = "Activate the given function.\nUsage: !botfunctionactivate <function prefix>";
                    }
                    else if (args.equals("botfunctiondisable") || args.equals("functionoff")) {
                        helpText2 = "Disable the given function.\nUsage: !botfunctiondisable <function prefix>";
                    }
                    else if (args.equals("clientsearch") || args.equals("clients") || args.equals("clientlist")) {
                        helpText2 = "Shows some database informations of a client. Search using the client name (* as a wildcard possible). You can also search using the complete unique id. The client database list cache needs to be enabled in the main bot configuration!\nExample: !clientsearch *foo*bar*";
                    }
                    else if (args.equals("searchip")) {
                        helpText2 = "Shows some database informations of a client found using ip address. Use * as a wildcard. The client database list cache needs to be enabled in the main bot configuration!\nExample: !searchip 127.0.*";
                    }
                    else if (args.equals("listinactiveclients") || args.equals("inactiveclients")) {
                        helpText2 = "List all clients which are inactive since X days. The client database list cache needs to be enabled in the main bot configuration!\nUsage: !listinactiveclients <minimum days inactive>";
                    }
                    else if (args.equals("listinactivechannels") || args.equals("emptychannels")) {
                        helpText2 = "List of empty channels sorted by empty since time. Optionally you can add a channel name or channel id to this command to filter the list.\nUsage: !listinactivechannels [channel id or part of the channel name]";
                    }
                    else if (args.equals("botinstancestart")) {
                        helpText2 = "Starts a bot instance with the given name.\nUsage: !botinstancestart <name>";
                    }
                    else if (args.equals("botinstancestop")) {
                        helpText2 = "Stops a bot instance with the given name. If no name given, the current instance will be stopped.\nUsage: !botinstancestop [name]";
                    }
                    else if (args.equals("botinstancelist")) {
                        helpText2 = "Shows a list of all bot instances with the current status.";
                    }
                    else if (args.equals("botinstancelistreload")) {
                        helpText2 = "Reloads the instance list from bot configuration.";
                    }
                    else if (args.equals("setchannelname") || args.equals("renamechannel")) {
                        helpText2 = "Set a new channel name for the given channel id.\nUsage: !setchannelname <channel id> <new channel name>";
                    }
                    else if (args.equals("getchannelid") || args.equals("channellist")) {
                        helpText2 = "Search for channel name to see the channel id or vice versa. The full channel name is not needed, just enter a part of the channel name.\nUsage: !getchannelid [channel id or part of the channel name]";
                    }
                    else if (args.equals("setchannelgroup")) {
                        helpText2 = "Sets channel group to client to all specified channels! Separate all channels with a comma at the end of this command!\nUsage: !setchannelgroup <client database id or unique id> <channel group id> <channel list separated with comma>";
                    }
                    else if (args.equals("removeservergroups")) {
                        helpText2 = "Removes all server groups of a client!\nUsage: !removeservergroups <client database id or unique id>";
                    }
                    else if (args.equals("removechannelgroups")) {
                        helpText2 = "Sets all non-default channel groups of a client to the default channel group in all channels!\nUsage: !removechannelgroups <client database id or unique id>";
                    }
                    else if (args.equals("msgchannelgroup") || args.equals("msgchannelgroups")) {
                        helpText2 = "Sends a private message to all online clients with this specified channel groups at the moment. Multiple comma separated channel groups without spaces are possible.\nUsage: !msgchannelgroup <channelgroup id> <message>\nExample: !msgchannelgroup 8,5 Hello guys!";
                    }
                    else if (args.equals("msgservergroup") || args.equals("msgservergroups")) {
                        helpText2 = "Sends a private message to all online clients that are member of the specified server groups. Multiple comma separated server groups without spaces are possible.\nUsage: !msgservergroup <servergroup id> <message>\nExample: !msgservergroup 6,7 Hello guys!";
                    }
                    else if (args.equals("botjoinchannel") || args.equals("joinchannel")) {
                        helpText2 = "Switch the bot into another channel.\nUsage: !botjoinchannel <channel id>";
                    }
                    else if (args.equals("botrename")) {
                        helpText2 = "Without argument bot renames back to default client name from bot config. Specify a client name as argument to set a new temporary client name for the bot.\nUsage: !botrename [new name]";
                    }
                    else if (args.equals("exec")) {
                        helpText2 = "Executes the specified system command. This command don't return the text output, just fire and forget! For security reasons this command needs to be enabled at the JTS3ServerMod_InstanceManager.cfg file.\nUsage: !exec <system command>";
                    }
                    else if (args.equals("execwait")) {
                        helpText2 = "Executes the specified system command and waits for process end. Program output and return code will be send as answer. For security reasons this command needs to be enabled at the JTS3ServerMod_InstanceManager.cfg file.\nUsage: !execwait <system command>";
                    }
                    if (helpText2 == null) {
                        helpText2 = this.modClass.getCommandHelp(args);
                    }
                    if (helpText2 == null) {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "No such command: [b]!" + args + "[/b]!");
                    }
                    else {
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, "Help of the command [b]!" + args + "[/b]:\n");
                        this.queryLib.sendTextMessage(Integer.parseInt(eventInfo.get("invokerid")), 1, helpText2);
                    }
                }
            }
        }
        catch (Exception e) {
            this.modClass.addLogEntry(e, false);
        }
    }
    
    private String getArguments(final String command) {
        final int pos = command.indexOf(" ");
        if (pos == -1) {
            return "";
        }
        return command.substring(pos).trim();
    }
}
