package de.stefan1200.jts3servermod.interfaces;

import java.util.*;

public interface ClientDatabaseCache_Interface
{
    String getLastIP(int p0);
    
    int getLastOnline(int p0);
    
    int getCreatedAt(int p0);
    
    String getNickname(int p0);
    
    String getUniqueID(int p0);
    
    String getDescription(int p0);
    
    int getDatabaseID(String p0);
    
    boolean isUpdateRunning();
    
    Vector<Integer> searchInactiveClients(int p0);
    
    Vector<Integer> searchIPAddress(String p0);
    
    Vector<Integer> searchClientNickname(String p0);
}
