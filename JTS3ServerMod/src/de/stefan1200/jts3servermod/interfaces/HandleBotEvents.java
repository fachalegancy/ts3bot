package de.stefan1200.jts3servermod.interfaces;

import de.stefan1200.jts3serverquery.*;

public interface HandleBotEvents
{
    void initClass(JTS3ServerMod_Interface p0, JTS3ServerQuery p1, String p2);
    
    void handleOnBotConnect();
    
    void handleAfterCacheUpdate();
    
    void activate();
    
    void disable();
    
    void unload();
    
    boolean multipleInstances();
}
