package de.stefan1200.jts3servermod.interfaces;

import java.util.*;

public interface HandleTS3Events
{
    String[] botChatCommandList(HashMap<String, String> p0, boolean p1, boolean p2);
    
    String botChatCommandHelp(String p0);
    
    boolean handleChatCommands(String p0, HashMap<String, String> p1, boolean p2, boolean p3);
    
    void handleClientEvents(String p0, HashMap<String, String> p1);
}
