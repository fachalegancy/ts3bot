package de.stefan1200.jts3servermod.interfaces;

public interface ServerInfoCache_Interface
{
    String getServerName();
    
    String getServerVersion();
    
    String getServerPlatform();
    
    long getServerUptime();
    
    long getServerUptimeTimestamp();
    
    long getServerCreatedAt();
    
    long getServerMonthBytesDownloaded();
    
    long getServerMonthBytesUploaded();
    
    long getServerTotalBytesDownloaded();
    
    long getServerTotalBytesUploaded();
    
    int getServerMaxClients();
    
    int getServerReservedSlots();
    
    int getServerChannelCount();
    
    int getServerClientCount();
    
    long getServerClientConnectionsCount();
    
    long getServerMinClientVersion();
    
    int getServerDefaultServerGroup();
    
    int getServerDefaultChannelAdminGroup();
    
    int getServerDefaultChannelGroup();
    
    long getServerMaxDownloadTotalBandwidth();
    
    long getServerMaxUploadTotalBandwidth();
    
    long getServerDownloadQuota();
    
    long getServerUploadQuota();
}
