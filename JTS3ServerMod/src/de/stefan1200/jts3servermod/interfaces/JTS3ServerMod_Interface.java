package de.stefan1200.jts3servermod.interfaces;

import de.stefan1200.util.*;
import java.util.*;

public interface JTS3ServerMod_Interface
{
    public static final String VERSION = "5.4.2 (18.01.2015)";
    public static final long VERSION_BUILD = 5402L;
    public static final byte ERROR_LEVEL_DEBUG = 0;
    public static final byte ERROR_LEVEL_INFO = 1;
    public static final byte ERROR_LEVEL_WARNING = 2;
    public static final byte ERROR_LEVEL_ERROR = 3;
    public static final byte ERROR_LEVEL_CRITICAL = 4;
    public static final int LIST_AWAY = 0;
    public static final int LIST_GROUPS = 1;
    public static final int LIST_INFO = 2;
    public static final int LIST_TIMES = 3;
    public static final int LIST_UID = 4;
    public static final int LIST_VOICE = 5;
    public static final int LIST_COUNTRY = 6;
    public static final int LIST_IP = 7;
    
    String getMessageEncoding();
    
    String getChannelName(int p0);
    
    void unloadFunction(Object p0);
    
    boolean loadMessages(String p0, String p1, String[] p2);
    
    ServerInfoCache_Interface getServerInfoCache();
    
    Vector<HashMap<String, String>> getClientList();
    
    Vector<HashMap<String, String>> getChannelList();
    
    MySQLConnect getMySQLConnection();
    
    int getInstanceID();
    
    int getDefaultChannelID();
    
    ClientDatabaseCache_Interface getClientCache();
    
    int getCheckInterval();
    
    String getStringFromTimestamp(long p0);
    
    void addTS3ChannelEvent(Object p0);
    
    void addTS3ServerEvent(Object p0);
    
    void addBotTimer(TimerTask p0, long p1, long p2);
    
    boolean isGlobalMessageVarsEnabled();
    
    String replaceGlobalMessageVars(String p0);
    
    boolean sendMessageToClient(String p0, String p1, int p2, String p3);
    
    short getMaxMessageLength(String p0);
    
    boolean isMessageLengthValid(String p0, String p1);
    
    long getIdleTime(HashMap<String, String> p0, int p1);
    
    boolean isIDListed(int p0, Vector<Integer> p1);
    
    String getServerGroupName(int p0);
    
    int getServerGroupType(int p0);
    
    int getListedGroup(String p0, Vector<Integer> p1);
    
    boolean isGroupListed(String p0, Vector<Integer> p1);
    
    String getVersionString(String p0);
    
    String getFileSizeString(long p0, boolean p1);
    
    String getDifferenceTime(long p0, long p1);
    
    void addLogEntry(String p0, byte p1, String p2, boolean p3);
    
    void addLogEntry(String p0, Throwable p1, boolean p2);
    
    int getClientDBID(String p0);
}
