package de.stefan1200.jts3servermod.interfaces;

import de.stefan1200.util.*;
import de.stefan1200.jts3servermod.*;
import java.util.*;

public interface LoadConfiguration
{
    void initConfig(ArrangedPropertiesWriter p0);
    
    boolean loadConfig(ArrangedPropertiesWriter p0, boolean p1) throws BotConfigurationException, NumberFormatException;
    
    void setListModes(BitSet p0);
}
