package de.stefan1200.jts3servermod;

import java.util.*;

public class ChannelEmptyComparator implements Comparator<HashMap<String, String>>
{
    public int compare(final HashMap<String, String> o1, final HashMap<String, String> o2) {
        final int i1 = Integer.parseInt(o1.get("seconds_empty"));
        final int i2 = Integer.parseInt(o2.get("seconds_empty"));
        if (i1 < i2) {
            return 1;
        }
        if (i1 > i2) {
            return -1;
        }
        return 0;
    }
}
