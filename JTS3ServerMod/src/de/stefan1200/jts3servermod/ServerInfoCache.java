package de.stefan1200.jts3servermod;

import de.stefan1200.jts3servermod.interfaces.*;
import java.util.*;

public class ServerInfoCache implements ServerInfoCache_Interface
{
    private String serverName;
    private String serverVersion;
    private String serverPlatform;
    private long serverUptimeSince;
    private long serverCreatedAt;
    private long serverDownloadQuota;
    private long serverUploadQuota;
    private long serverMaxDownloadTotalBandwidth;
    private long serverMaxUploadTotalBandwidth;
    private long serverMonthBytesDownloaded;
    private long serverMonthBytesUploaded;
    private long serverTotalBytesDownloaded;
    private long serverTotalBytesUploaded;
    private long serverClientConnectionsCount;
    private long serverMinClientVersion;
    private int serverMaxClients;
    private int serverReservedSlots;
    private int serverChannelCount;
    private int serverClientCount;
    private int serverDefaultServerGroup;
    private int serverDefaultChannelAdminGroup;
    private int serverDefaultChannelGroup;
    
    public ServerInfoCache() {
        this.serverName = null;
        this.serverVersion = null;
        this.serverPlatform = null;
        this.serverUptimeSince = -1L;
        this.serverCreatedAt = -1L;
        this.serverDownloadQuota = -1L;
        this.serverUploadQuota = -1L;
        this.serverMaxDownloadTotalBandwidth = -1L;
        this.serverMaxUploadTotalBandwidth = -1L;
        this.serverMonthBytesDownloaded = -1L;
        this.serverMonthBytesUploaded = -1L;
        this.serverTotalBytesDownloaded = -1L;
        this.serverTotalBytesUploaded = -1L;
        this.serverClientConnectionsCount = -1L;
        this.serverMinClientVersion = -1L;
        this.serverMaxClients = -1;
        this.serverReservedSlots = -1;
        this.serverChannelCount = -1;
        this.serverClientCount = -1;
        this.serverDefaultServerGroup = -1;
        this.serverDefaultChannelAdminGroup = -1;
        this.serverDefaultChannelGroup = -1;
    }
    
    void updateServerInfo(final HashMap<String, String> serverinfo) {
        if (serverinfo == null) {
            return;
        }
        this.serverName = serverinfo.get("virtualserver_name");
        if (this.serverVersion == null) {
            this.serverVersion = serverinfo.get("virtualserver_version");
        }
        if (this.serverPlatform == null) {
            this.serverPlatform = serverinfo.get("virtualserver_platform");
        }
        try {
            if (this.serverUptimeSince == -1L) {
                this.serverUptimeSince = System.currentTimeMillis() - Long.parseLong(serverinfo.get("virtualserver_uptime")) * 1000L;
            }
            if (this.serverCreatedAt == -1L) {
                this.serverCreatedAt = Long.parseLong(serverinfo.get("virtualserver_created")) * 1000L;
            }
            this.serverMaxClients = Integer.parseInt(serverinfo.get("virtualserver_maxclients"));
            this.serverReservedSlots = Integer.parseInt(serverinfo.get("virtualserver_reserved_slots"));
            this.serverChannelCount = Integer.parseInt(serverinfo.get("virtualserver_channelsonline"));
            this.serverClientCount = Integer.parseInt(serverinfo.get("virtualserver_clientsonline")) - Integer.parseInt(serverinfo.get("virtualserver_queryclientsonline"));
            this.serverClientConnectionsCount = Long.parseLong(serverinfo.get("virtualserver_client_connections"));
            this.serverDefaultServerGroup = Integer.parseInt(serverinfo.get("virtualserver_default_server_group"));
            this.serverDefaultChannelAdminGroup = Integer.parseInt(serverinfo.get("virtualserver_default_channel_admin_group"));
            this.serverDefaultChannelGroup = Integer.parseInt(serverinfo.get("virtualserver_default_channel_group"));
            this.serverMinClientVersion = Long.parseLong(serverinfo.get("virtualserver_min_client_version"));
            this.serverMonthBytesDownloaded = Long.parseLong(serverinfo.get("virtualserver_month_bytes_downloaded"));
            this.serverMonthBytesUploaded = Long.parseLong(serverinfo.get("virtualserver_month_bytes_uploaded"));
            this.serverTotalBytesDownloaded = Long.parseLong(serverinfo.get("virtualserver_total_bytes_downloaded"));
            this.serverTotalBytesUploaded = Long.parseLong(serverinfo.get("virtualserver_total_bytes_uploaded"));
        }
        catch (Exception ex) {}
        try {
            this.serverDownloadQuota = Long.parseLong(serverinfo.get("virtualserver_download_quota"));
        }
        catch (Exception e) {
            this.serverDownloadQuota = -1L;
        }
        try {
            this.serverUploadQuota = Long.parseLong(serverinfo.get("virtualserver_upload_quota"));
        }
        catch (Exception e) {
            this.serverUploadQuota = -1L;
        }
        try {
            this.serverMaxDownloadTotalBandwidth = Long.parseLong(serverinfo.get("virtualserver_max_download_total_bandwidth"));
        }
        catch (Exception e) {
            this.serverMaxDownloadTotalBandwidth = -1L;
        }
        try {
            this.serverMaxUploadTotalBandwidth = Long.parseLong(serverinfo.get("virtualserver_max_upload_total_bandwidth"));
        }
        catch (Exception e) {
            this.serverMaxUploadTotalBandwidth = -1L;
        }
    }
    
    void updateClientCount(final Vector<HashMap<String, String>> clientList) {
        int clientCountTemp = 0;
        for (final HashMap<String, String> clientInfo : clientList) {
            if (clientInfo.get("client_type").equals("0")) {
                ++clientCountTemp;
            }
        }
        this.serverClientCount = clientCountTemp;
    }
    
    public String getServerName() {
        return this.serverName;
    }
    
    public String getServerVersion() {
        return this.serverVersion;
    }
    
    public String getServerPlatform() {
        return this.serverPlatform;
    }
    
    public long getServerUptime() {
        return System.currentTimeMillis() - this.serverUptimeSince;
    }
    
    public long getServerUptimeTimestamp() {
        return this.serverUptimeSince;
    }
    
    public long getServerCreatedAt() {
        return this.serverCreatedAt;
    }
    
    public long getServerDownloadQuota() {
        return this.serverDownloadQuota;
    }
    
    public long getServerUploadQuota() {
        return this.serverUploadQuota;
    }
    
    public long getServerMonthBytesDownloaded() {
        return this.serverMonthBytesDownloaded;
    }
    
    public long getServerMonthBytesUploaded() {
        return this.serverMonthBytesUploaded;
    }
    
    public long getServerTotalBytesDownloaded() {
        return this.serverTotalBytesDownloaded;
    }
    
    public long getServerTotalBytesUploaded() {
        return this.serverTotalBytesUploaded;
    }
    
    public int getServerMaxClients() {
        return this.serverMaxClients;
    }
    
    public int getServerReservedSlots() {
        return this.serverReservedSlots;
    }
    
    public int getServerChannelCount() {
        return this.serverChannelCount;
    }
    
    public int getServerClientCount() {
        return this.serverClientCount;
    }
    
    public long getServerClientConnectionsCount() {
        return this.serverClientConnectionsCount;
    }
    
    public long getServerMinClientVersion() {
        return this.serverMinClientVersion;
    }
    
    public int getServerDefaultServerGroup() {
        return this.serverDefaultServerGroup;
    }
    
    public int getServerDefaultChannelAdminGroup() {
        return this.serverDefaultChannelAdminGroup;
    }
    
    public int getServerDefaultChannelGroup() {
        return this.serverDefaultChannelGroup;
    }
    
    public long getServerMaxDownloadTotalBandwidth() {
        return this.serverMaxDownloadTotalBandwidth;
    }
    
    public long getServerMaxUploadTotalBandwidth() {
        return this.serverMaxUploadTotalBandwidth;
    }
}
