package de.stefan1200.jts3servermod;

import java.util.*;
import de.stefan1200.jts3serverquery.*;

public class FunctionExceptionLog
{
    Vector<TS3ServerQueryException> exceptionLog;
    Vector<Integer> exceptionTargetID;
    
    public FunctionExceptionLog() {
        this.exceptionLog = new Vector<TS3ServerQueryException>();
        this.exceptionTargetID = new Vector<Integer>();
    }
    
    public void addException(final TS3ServerQueryException exception) {
        this.addException(exception, -1);
    }
    
    public void addException(final TS3ServerQueryException exception, final int targetID) {
        this.exceptionLog.addElement(exception);
        this.exceptionTargetID.addElement(targetID);
    }
    
    public boolean existsException(final TS3ServerQueryException exception) {
        return this.existsException(exception, -1);
    }
    
    public boolean existsException(final TS3ServerQueryException exception, final int targetID) {
        for (int i = 0; i < this.exceptionLog.size(); ++i) {
            if (exception.getErrorID() == this.exceptionLog.elementAt(i).getErrorID() && exception.getFailedPermissionID() == this.exceptionLog.elementAt(i).getFailedPermissionID() && this.exceptionTargetID.elementAt(i) == targetID) {
                return true;
            }
        }
        return false;
    }
    
    public void clearException(final int targetID) {
        int i = -1;
        while ((i = this.exceptionTargetID.indexOf(targetID)) >= 0) {
            this.exceptionLog.removeElementAt(i);
            this.exceptionTargetID.removeElementAt(i);
        }
    }
    
    public void clearAllExceptions() {
        this.exceptionLog.clear();
        this.exceptionTargetID.clear();
    }
}
