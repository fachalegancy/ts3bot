package de.stefan1200.jts3servermod;

public class BotConfigurationException extends Exception
{
    public BotConfigurationException(final String description) {
        super(description);
    }
}
