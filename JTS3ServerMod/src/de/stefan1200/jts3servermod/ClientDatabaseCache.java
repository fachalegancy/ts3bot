package de.stefan1200.jts3servermod;

import de.stefan1200.jts3servermod.interfaces.*;
import de.stefan1200.jts3serverquery.*;
import java.util.*;

public class ClientDatabaseCache implements ClientDatabaseCache_Interface
{
    private final String REQUEST_COUNT = "50";
    private final short REQUEST_DELAY = 5000;
    private JTS3ServerMod modClass;
    private JTS3ServerQuery queryLib;
    private Vector<String> uniqueID;
    private Vector<String> nickname;
    private Vector<String> description;
    private Vector<String> lastIP;
    private Vector<Integer> createdAt;
    private Vector<Integer> lastOnline;
    private Vector<Integer> databaseID;
    private Vector<HashMap<String, String>> tempCache;
    private int currentPosition;
    private boolean updateIsRunning;
    private boolean disconnectIfReady;
    
    public ClientDatabaseCache(final JTS3ServerQuery queryLib, final JTS3ServerMod modClass) {
        this.uniqueID = new Vector<String>();
        this.nickname = new Vector<String>();
        this.description = new Vector<String>();
        this.lastIP = new Vector<String>();
        this.createdAt = new Vector<Integer>();
        this.lastOnline = new Vector<Integer>();
        this.databaseID = new Vector<Integer>();
        this.tempCache = new Vector<HashMap<String, String>>();
        this.currentPosition = 0;
        this.updateIsRunning = false;
        this.disconnectIfReady = false;
        this.queryLib = queryLib;
        this.modClass = modClass;
        this.updateCache();
    }
    
    public ClientDatabaseCache(final JTS3ServerQuery queryLib, final JTS3ServerMod modClass, final boolean disconnectIfReady) {
        this.uniqueID = new Vector<String>();
        this.nickname = new Vector<String>();
        this.description = new Vector<String>();
        this.lastIP = new Vector<String>();
        this.createdAt = new Vector<Integer>();
        this.lastOnline = new Vector<Integer>();
        this.databaseID = new Vector<Integer>();
        this.tempCache = new Vector<HashMap<String, String>>();
        this.currentPosition = 0;
        this.updateIsRunning = false;
        this.disconnectIfReady = false;
        this.queryLib = queryLib;
        this.modClass = modClass;
        this.disconnectIfReady = disconnectIfReady;
        this.updateCache();
    }
    
    private void updateCache() {
        this.currentPosition = 0;
        new Thread(new Runnable() {
            public void run() {
                ClientDatabaseCache.this.modClass.addLogEntry((byte)1, "Creating client database cache...", true);
                ClientDatabaseCache.access$1(ClientDatabaseCache.this, true);
                while (ClientDatabaseCache.this.updateIsRunning) {
                    Vector<HashMap<String, String>> clientDBList = null;
                    Label_0100: {
                        try {
                            clientDBList = ClientDatabaseCache.this.queryLib.getList(5, "start=" + Integer.toString(ClientDatabaseCache.this.currentPosition) + ",duration=" + "50");
                            if (clientDBList != null && clientDBList.size() != 0) {
                                if (ClientDatabaseCache.this.updateIsRunning) {
                                    break Label_0100;
                                }
                            }
                        }
                        catch (Exception e) {}
                        break;
                    }
                    for (final HashMap<String, String> clientInfo : clientDBList) {
                        try {
                            if (ClientDatabaseCache.this.databaseID.size() <= ClientDatabaseCache.this.currentPosition) {
                                ClientDatabaseCache.this.createdAt.addElement(Integer.parseInt(clientInfo.get("client_created")));
                                ClientDatabaseCache.this.lastOnline.addElement(Integer.parseInt(clientInfo.get("client_lastconnected")));
                                ClientDatabaseCache.this.databaseID.addElement(Integer.parseInt(clientInfo.get("cldbid")));
                                ClientDatabaseCache.this.nickname.addElement(clientInfo.get("client_nickname"));
                                ClientDatabaseCache.this.description.addElement(clientInfo.get("client_description"));
                                ClientDatabaseCache.this.lastIP.addElement(clientInfo.get("client_lastip"));
                                ClientDatabaseCache.this.uniqueID.addElement(clientInfo.get("client_unique_identifier"));
                            }
                            else {
                                ClientDatabaseCache.this.createdAt.setElementAt(Integer.parseInt(clientInfo.get("client_created")), ClientDatabaseCache.this.currentPosition);
                                ClientDatabaseCache.this.lastOnline.setElementAt(Integer.parseInt(clientInfo.get("client_lastconnected")), ClientDatabaseCache.this.currentPosition);
                                ClientDatabaseCache.this.databaseID.setElementAt(Integer.parseInt(clientInfo.get("cldbid")), ClientDatabaseCache.this.currentPosition);
                                ClientDatabaseCache.this.nickname.setElementAt(clientInfo.get("client_nickname"), ClientDatabaseCache.this.currentPosition);
                                ClientDatabaseCache.this.description.setElementAt(clientInfo.get("client_description"), ClientDatabaseCache.this.currentPosition);
                                ClientDatabaseCache.this.lastIP.setElementAt(clientInfo.get("client_lastip"), ClientDatabaseCache.this.currentPosition);
                                ClientDatabaseCache.this.uniqueID.setElementAt(clientInfo.get("client_unique_identifier"), ClientDatabaseCache.this.currentPosition);
                            }
                        }
                        catch (NumberFormatException nfe) {
                            ClientDatabaseCache.this.modClass.addLogEntry((byte)2, "Got invalid information for client \"" + clientInfo.get("client_nickname") + "\", skipping client!", false);
                            continue;
                        }
                        final ClientDatabaseCache this$0 = ClientDatabaseCache.this;
                        ClientDatabaseCache.access$12(this$0, this$0.currentPosition + 1);
                    }
                    try {
                        Thread.sleep(5000L);
                    }
                    catch (Exception e) {
                        break;
                    }
                }
                ClientDatabaseCache.this.updateFromTempCache();
                ClientDatabaseCache.access$1(ClientDatabaseCache.this, false);
                ClientDatabaseCache.this.modClass.addLogEntry((byte)1, "Client database cache created, " + Integer.toString(ClientDatabaseCache.this.databaseID.size()) + " clients in cache.", true);
                if (ClientDatabaseCache.this.disconnectIfReady) {
                    ClientDatabaseCache.this.queryLib.closeTS3Connection();
                }
            }
        }).start();
    }
    
    private void updateFromTempCache() {
        if (!this.updateIsRunning) {
            return;
        }
        while (this.tempCache.size() > 0) {
            this.internalUpdateSingleClient(this.tempCache.elementAt(0));
            this.tempCache.removeElementAt(0);
            if (!this.updateIsRunning) {
                break;
            }
        }
    }
    
    void updateSingleClient(final HashMap<String, String> clientInfo) {
        if (this.updateIsRunning) {
            this.tempCache.addElement(clientInfo);
            return;
        }
        this.internalUpdateSingleClient(clientInfo);
    }
    
    private void internalUpdateSingleClient(final HashMap<String, String> clientInfo) {
        try {
            if (Integer.parseInt(clientInfo.get("client_type")) != 0) {
                return;
            }
            final int searchDBID = Integer.parseInt(clientInfo.get("client_database_id"));
            final int index = this.databaseID.indexOf(searchDBID);
            if (index >= 0) {
                this.nickname.setElementAt(clientInfo.get("client_nickname"), index);
                this.lastOnline.setElementAt((int)(System.currentTimeMillis() / 1000L), index);
                if (clientInfo.get("client_description") != null) {
                    this.description.setElementAt(clientInfo.get("client_description"), index);
                }
                if (this.uniqueID.elementAt(index).length() == 0 && clientInfo.get("client_unique_identifier") != null) {
                    this.uniqueID.setElementAt(clientInfo.get("client_unique_identifier"), index);
                }
                if (clientInfo.get("connection_client_ip") != null) {
                    this.lastIP.setElementAt(clientInfo.get("connection_client_ip"), index);
                }
            }
            else {
                this.nickname.addElement(clientInfo.get("client_nickname"));
                this.description.addElement((clientInfo.get("client_description") == null) ? "" : clientInfo.get("client_description"));
                this.uniqueID.addElement((clientInfo.get("client_unique_identifier") == null) ? "" : clientInfo.get("client_unique_identifier"));
                this.createdAt.addElement((int)(System.currentTimeMillis() / 1000L));
                this.lastOnline.addElement((int)(System.currentTimeMillis() / 1000L));
                this.databaseID.addElement(Integer.parseInt(clientInfo.get("client_database_id")));
                this.lastIP.addElement((clientInfo.get("connection_client_ip") == null) ? "" : clientInfo.get("connection_client_ip"));
            }
        }
        catch (NumberFormatException e) {
            this.modClass.addLogEntry((byte)2, "Got invalid information for client \"" + clientInfo.get("client_nickname") + "\"!", false);
        }
    }
    
    void stopUpdating() {
        this.updateIsRunning = false;
    }
    
    public String getLastIP(final int clientDBID) {
        final int pos = this.databaseID.indexOf(clientDBID);
        if (pos != -1) {
            return this.lastIP.elementAt(pos);
        }
        return null;
    }
    
    public int getLastOnline(final int clientDBID) {
        final int pos = this.databaseID.indexOf(clientDBID);
        if (pos != -1) {
            return this.lastOnline.elementAt(pos);
        }
        return -1;
    }
    
    public int getCreatedAt(final int clientDBID) {
        final int pos = this.databaseID.indexOf(clientDBID);
        if (pos != -1) {
            return this.createdAt.elementAt(pos);
        }
        return -1;
    }
    
    public String getNickname(final int clientDBID) {
        final int pos = this.databaseID.indexOf(clientDBID);
        if (pos != -1) {
            return this.nickname.elementAt(pos);
        }
        return null;
    }
    
    public String getUniqueID(final int clientDBID) {
        final int pos = this.databaseID.indexOf(clientDBID);
        if (pos != -1) {
            return this.uniqueID.elementAt(pos);
        }
        return null;
    }
    
    public String getDescription(final int clientDBID) {
        final int pos = this.databaseID.indexOf(clientDBID);
        if (pos != -1) {
            return this.description.elementAt(pos);
        }
        return null;
    }
    
    public int getDatabaseID(final String clientUniqueID) {
        final int pos = this.uniqueID.indexOf(clientUniqueID);
        if (pos != -1) {
            return this.databaseID.elementAt(pos);
        }
        return -1;
    }
    
    public boolean isUpdateRunning() {
        return this.updateIsRunning;
    }
    
    public Vector<Integer> searchInactiveClients(final int daysInactive) {
        if (this.updateIsRunning) {
            return null;
        }
        if (daysInactive < 10) {
            return null;
        }
        final long daysInactiveSeconds = System.currentTimeMillis() / 1000L - daysInactive * 86400;
        final Vector<Integer> result = new Vector<Integer>();
        for (int i = 0; i < this.lastOnline.size(); ++i) {
            if (this.lastOnline.elementAt(i) < daysInactiveSeconds) {
                result.addElement(this.databaseID.elementAt(i));
            }
        }
        return result;
    }
    
    public Vector<Integer> searchIPAddress(final String search) {
        if (this.updateIsRunning) {
            return null;
        }
        if (search == null) {
            return null;
        }
        final Vector<Integer> result = new Vector<Integer>();
        try {
            final String ipaddressTemp = search.replace("*", "");
            if (ipaddressTemp.length() < 3) {
                return null;
            }
            final boolean startsWith = search.startsWith("*");
            final boolean endsWith = search.endsWith("*");
            final StringTokenizer st = new StringTokenizer(search, "*", false);
            final Vector<String> parts = new Vector<String>();
            while (st.hasMoreTokens()) {
                final String tmp = st.nextToken();
                if (tmp.length() > 0) {
                    parts.addElement(tmp.toLowerCase());
                }
            }
            int pos = -1;
            for (int i = 0; i < this.lastIP.size(); ++i) {
                final String tmp = this.lastIP.elementAt(i).toLowerCase();
                pos = 0;
                for (int x = 0; x < parts.size(); ++x) {
                    final String tmpPart = parts.elementAt(x);
                    if (parts.size() == 1) {
                        if (tmp.equalsIgnoreCase(tmpPart)) {
                            result.addElement(this.databaseID.elementAt(i));
                            break;
                        }
                        if (!startsWith && !endsWith) {
                            break;
                        }
                    }
                    if (x == 0 && !startsWith) {
                        if (!tmp.startsWith(tmpPart)) {
                            break;
                        }
                        pos = tmpPart.length();
                        if (x == parts.size() - 1) {
                            result.addElement(this.databaseID.elementAt(i));
                        }
                    }
                    else if (x == parts.size() - 1 && !endsWith) {
                        if (tmp.endsWith(tmpPart)) {
                            result.addElement(this.databaseID.elementAt(i));
                            break;
                        }
                        break;
                    }
                    else {
                        pos = tmp.indexOf(tmpPart, pos);
                        if (pos == -1) {
                            break;
                        }
                        pos += tmpPart.length();
                        if (x == parts.size() - 1) {
                            result.addElement(this.databaseID.elementAt(i));
                        }
                    }
                }
            }
        }
        catch (Exception ex) {}
        return result;
    }
    
    public Vector<Integer> searchClientNickname(final String search) {
        if (this.updateIsRunning) {
            return null;
        }
        if (search == null) {
            return null;
        }
        final Vector<Integer> result = new Vector<Integer>();
        final int tempID = this.getDatabaseID(search);
        if (tempID != -1) {
            result.addElement(tempID);
            return result;
        }
        try {
            final String clientnameTemp = search.replace("*", "");
            if (clientnameTemp.length() < 3) {
                return null;
            }
            final boolean startsWith = search.startsWith("*");
            final boolean endsWith = search.endsWith("*");
            final StringTokenizer st = new StringTokenizer(search, "*", false);
            final Vector<String> parts = new Vector<String>();
            while (st.hasMoreTokens()) {
                final String tmp = st.nextToken();
                if (tmp.length() > 0) {
                    parts.addElement(tmp.toLowerCase());
                }
            }
            int pos = -1;
            for (int i = 0; i < this.nickname.size(); ++i) {
                final String tmp = this.nickname.elementAt(i).toLowerCase();
                pos = 0;
                for (int x = 0; x < parts.size(); ++x) {
                    final String tmpPart = parts.elementAt(x);
                    if (parts.size() == 1) {
                        if (tmp.equalsIgnoreCase(tmpPart)) {
                            result.addElement(this.databaseID.elementAt(i));
                            break;
                        }
                        if (!startsWith && !endsWith) {
                            break;
                        }
                    }
                    if (x == 0 && !startsWith) {
                        if (!tmp.startsWith(tmpPart)) {
                            break;
                        }
                        pos = tmpPart.length();
                        if (x == parts.size() - 1) {
                            result.addElement(this.databaseID.elementAt(i));
                        }
                    }
                    else if (x == parts.size() - 1 && !endsWith) {
                        if (tmp.endsWith(tmpPart)) {
                            result.addElement(this.databaseID.elementAt(i));
                            break;
                        }
                        break;
                    }
                    else {
                        pos = tmp.indexOf(tmpPart, pos);
                        if (pos == -1) {
                            break;
                        }
                        pos += tmpPart.length();
                        if (x == parts.size() - 1) {
                            result.addElement(this.databaseID.elementAt(i));
                        }
                    }
                }
            }
        }
        catch (Exception ex) {}
        return result;
    }
    
    static /* synthetic */ void access$1(final ClientDatabaseCache clientDatabaseCache, final boolean updateIsRunning) {
        clientDatabaseCache.updateIsRunning = updateIsRunning;
    }
    
    static /* synthetic */ void access$12(final ClientDatabaseCache clientDatabaseCache, final int currentPosition) {
        clientDatabaseCache.currentPosition = currentPosition;
    }
}
