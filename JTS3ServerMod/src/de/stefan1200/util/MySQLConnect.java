package de.stefan1200.util;

import java.sql.*;

public class MySQLConnect
{
    private Connection dbConnection;
    private String url;
    private String username;
    private String password;
    
    public MySQLConnect(String mysqlHost, int mysqlPort, String mysqlDB, String mysqlUser, String mysqlPW) throws ClassNotFoundException {
        this.dbConnection = null;
        this.url = null;
        this.username = null;
        this.password = null;
        if (mysqlPort < 0 || mysqlPort > 65535) {
            mysqlPort = 3306;
        }
        if (mysqlHost == null) {
            mysqlHost = "localhost";
        }
        if (mysqlDB == null) {
            mysqlDB = "";
        }
        if (mysqlUser == null) {
            mysqlUser = "root";
        }
        if (mysqlPW == null) {
            mysqlPW = "";
        }
        this.url = "jdbc:mysql://" + mysqlHost + ":" + Integer.toString(mysqlPort) + "/" + mysqlDB;
        this.username = mysqlUser;
        this.password = mysqlPW;
        Class.forName("com.mysql.jdbc.Driver");
    }
    
    public void close() {
        try {
            if (!this.dbConnection.isClosed()) {
                this.dbConnection.close();
            }
        }
        catch (Exception ex) {}
    }
    
    public void connect() throws SQLException {
        if (this.dbConnection == null || this.dbConnection.isClosed()) {
            this.dbConnection = DriverManager.getConnection(this.url, this.username, this.password);
        }
    }
    
    public Connection getConnection() {
        return this.dbConnection;
    }
    
    public PreparedStatement getPreparedStatement(final String sql) throws SQLException {
        return this.dbConnection.prepareStatement(sql);
    }
    
    public Statement getStatement() throws SQLException {
        return this.dbConnection.createStatement();
    }
}
