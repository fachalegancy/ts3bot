package de.stefan1200.util;

import java.text.*;
import java.io.*;
import java.util.*;

public class ArrangedPropertiesWriter
{
    private final String SEPARATOR = "***";
    private final String LINE_SEPARATOR;
    private HashMap<String, String> hmHelp;
    private HashMap<String, String> hmValue;
    private HashMap<String, Boolean> hmSave;
    private Vector<String> vKeys;
    private SimpleDateFormat sdf;
    
    public ArrangedPropertiesWriter() {
        this.LINE_SEPARATOR = System.getProperty("line.separator");
        this.hmHelp = new HashMap<String, String>();
        this.hmValue = new HashMap<String, String>();
        this.hmSave = new HashMap<String, Boolean>();
        this.vKeys = new Vector<String>();
        this.sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }
    
    public boolean addKey(final String key, final String helpText) {
        return this.addKey(key, helpText, null, true);
    }
    
    public boolean addKey(final String key, final String helpText, final String defaultValue) {
        return this.addKey(key, helpText, defaultValue, true);
    }
    
    public boolean addKey(final String key, final String helpText, final boolean saveToFile) {
        return this.addKey(key, helpText, null, saveToFile);
    }
    
    public boolean addKey(final String key, final String helpText, final String defaultValue, final boolean saveToFile) {
        if (!key.equals("***") && key.length() > 0 && this.vKeys.indexOf(key) == -1) {
            this.vKeys.addElement(key);
            this.hmValue.put(key, defaultValue);
            this.hmHelp.put(key, helpText);
            this.hmSave.put(key, saveToFile);
            return true;
        }
        return false;
    }
    
    public boolean insertKey(final String key, final int pos, final String helpText) {
        if (!key.equals("***") && key.length() > 0 && this.vKeys.indexOf(key) == -1) {
            this.vKeys.insertElementAt(key, pos);
            this.hmHelp.put(key, helpText);
            return true;
        }
        return false;
    }
    
    public boolean canSaveToFile(final String key) {
        return this.hmSave.get(key);
    }
    
    public Vector<String> getKeys() {
        final Vector<String> retKeys = new Vector<String>();
        retKeys.addAll(this.vKeys);
        while (retKeys.removeElement("***")) {}
        return retKeys;
    }
    
    public String getValue(final String key) {
        return this.hmValue.get(key);
    }
    
    public String getValue(final String key, final String defValue) {
        if (this.hmValue.get(key) == null) {
            return defValue;
        }
        return this.hmValue.get(key);
    }
    
    public boolean setValue(final String key, final String value) {
        if (!key.equals("***") && key.length() > 0 && this.vKeys.indexOf(key) != -1) {
            this.hmValue.put(key, value);
            return true;
        }
        return false;
    }
    
    public boolean setValue(final String key, final long value) {
        return this.setValue(key, Long.toString(value));
    }
    
    public boolean setValue(final String key, final double value) {
        return this.setValue(key, Double.toString(value));
    }
    
    public boolean setValue(final String key, final boolean value) {
        return this.setValue(key, Boolean.toString(value));
    }
    
    public void removeAllValues() {
        this.hmValue.clear();
    }
    
    public void addSeparator() {
        this.vKeys.addElement("***");
    }
    
    public void insertSeparator(final int pos) {
        this.vKeys.insertElementAt("***", pos);
    }
    
    public void removeAllSeparators() {
        while (this.vKeys.removeElement("***")) {}
    }
    
    public int getKeyCount() {
        return this.vKeys.size();
    }
    
    public String getHelpText(final String key) {
        return this.hmHelp.get(key);
    }
    
    public boolean removeKey(final String key) {
        if (!key.equals("***") && key.length() > 0 && this.vKeys.indexOf(key) != -1) {
            this.vKeys.removeElement(key);
            this.hmHelp.remove(key);
            this.hmValue.remove(key);
            this.hmSave.remove(key);
            return true;
        }
        return false;
    }
    
    public boolean loadValues(final File file) {
        if (file == null || !file.isFile()) {
            return false;
        }
        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream(file));
            for (final String key : this.vKeys) {
                final String temp = prop.getProperty(key);
                if (temp != null) {
                    this.hmValue.put(key, temp);
                }
            }
        }
        catch (Exception e) {
            prop = null;
            return false;
        }
        prop = null;
        return true;
    }
    
    public boolean loadValues(final String filename) {
        return filename != null && this.loadValues(new File(filename));
    }
    
    public boolean save(final String filename, final String header) {
        if (filename == null) {
            return false;
        }
        PrintStream ps;
        try {
            ps = new PrintStream(filename, "ISO-8859-1");
        }
        catch (Exception e) {
            return false;
        }
        if (header != null && header.length() > 0) {
            ps.println(this.convertString(header));
        }
        ps.println("# File created at " + this.sdf.format(new Date(System.currentTimeMillis())));
        ps.println();
        for (final String key : this.vKeys) {
            if (key.equals("***")) {
                ps.println();
            }
            else {
                if (!this.hmSave.get(key)) {
                    continue;
                }
                if (this.hmHelp.get(key) != null) {
                    ps.println(this.convertString(this.hmHelp.get(key)));
                }
                ps.print(key);
                ps.print(" = ");
                ps.println((this.hmValue.get(key) == null) ? "" : this.hmValue.get(key));
            }
        }
        ps.close();
        return true;
    }
    
    private String convertString(final String text) {
        String retValue = "# " + text;
        retValue = retValue.replace("\\", "$[mkbackslashsave]");
        retValue = retValue.replace("\n", String.valueOf(this.LINE_SEPARATOR) + "# ");
        retValue = retValue.replace("$[mkbackslashsave]", "\\");
        return retValue;
    }
}
